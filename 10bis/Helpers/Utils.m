//
//  Utils.m
//  UZ
//
//  Created by Vadim Pavlov on 16.10.13.
//  Copyright (c) 2013 Haski Team. All rights reserved.
//

#import "Utils.h"
#import "Downloader.h"

inline id ValueOrNil (NSDictionary *dict, NSString *key)
{
    if ([dict isKindOfClass: [NSDictionary class]])
    {
        if (dict[key] != [NSNull null])
        {
            return dict[key];
        }
    }
    return nil;
}

static dispatch_block_t _blockHandler;

@implementation Utils
+ (BOOL)isIOS7
{
    return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7;
}
+ (UIColor*)appSecondaryColor
{
    NSDictionary *color = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppSecondaryColor"];
    return RGBA([color[@"r"] integerValue], [color[@"g"] integerValue], [color[@"b"] integerValue], 1);
}

+ (UIColor*)appTextColor
{
    NSDictionary *color = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppTextColor"];
    return RGBA([color[@"r"] integerValue], [color[@"g"] integerValue], [color[@"b"] integerValue], 1);
}

+ (UIColor *)appPrimaryColor
{
    NSDictionary *color = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"AppPrimaryColor"];
    return RGBA([color[@"r"] integerValue], [color[@"g"] integerValue], [color[@"b"] integerValue], 1);
}

+ (void)callToNumber:(NSString *)phoneNumber
{
    NSString *newNum = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    newNum = [newNum stringByReplacingOccurrencesOfString:@"(" withString:@""];
        newNum = [newNum stringByReplacingOccurrencesOfString:@")" withString:@""];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",newNum]]];
    
}

+ (BOOL)isEmailValid:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"%@ MATCHES %@", email, emailRegex];
    return [emailTest evaluateWithObject: email];
}
+ (UIImage*)iconForSocialNetwork:(NSString*)network
{
    NSString *imageName = nil;
    if ([network isEqualToString: @"facebook"])
        imageName = @"icon_fb";
    else if ([network isEqualToString: @"twitter"])
        imageName = @"icon_tw";
    else if ([network isEqualToString: @"linkedin"])
        imageName = @"icon_li";
    else if ([network isEqualToString: @"googleplus"])
        imageName = @"icon_g";
    else if ([network isEqualToString: @"google"])
        imageName = @"icon_gl";
    
    return [UIImage imageNamed: imageName];
}
+ (UIColor*)colorForSocialNetwork:(NSString*)network
{
    UIColor *imageColor = nil;
    if ([network isEqualToString: @"facebook"])
        imageColor = RGB(58, 89, 152);
    else if ([network isEqualToString: @"twitter"])
        imageColor = RGB(1, 172, 238);
    else if ([network isEqualToString: @"linkedin"])
        imageColor = RGB(1, 123, 182);
    else if ([network isEqualToString: @"googleplus"])
        imageColor = RGB(221, 75, 57);
    else if ([network isEqualToString: @"google"])
        imageColor = RGB(221, 31, 17);
    return imageColor;
}
+ (UIButton*)menuButton
{
    UIImage *menuImage = [UIImage imageNamed: @"icon_menu"];
    UIButton *menuButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [menuButton setImage: menuImage forState: UIControlStateNormal];
    if ([Utils isIOS7])
        [menuButton sizeToFit];
    else
        menuButton.frame = CGRectMake(0, 0, 30, 25);
    return menuButton;
}
+ (void)showErrorAlert:(NSError*)error
{
    [self showErrorMessage: error.localizedDescription];
}
+ (void)showSuccessMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Success" message:message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    [alert show];
}
+ (void)showErrorMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message:message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    [alert show];
}
+ (void)showNetworkErrorMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Network error" message:message delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    [alert show];
}
+ (void)loadImage:(NSString*)photoUrl completion:(void (^)(UIImage* image))completion
{
	if (photoUrl == nil)
	{
		completion(nil);
		return;
	}
    // check if saved in temp
    NSString *imageName = [photoUrl stringByReplacingOccurrencesOfString: @"/" withString: @""];
    imageName = [imageName stringByReplacingOccurrencesOfString: @":" withString: @""];
    imageName = [imageName stringByReplacingOccurrencesOfString: @"." withString: @""];
    imageName = [imageName stringByReplacingOccurrencesOfString: @"?" withString: @""];
    
    UIImage *image = [self imageFromTemp:imageName];
    if (image)
    {
        completion(image);
        return;
    }
    
    if (!photoUrl) {
        return;
    }
    // download
    [Downloader imageDownloadWithRequest: [photoUrl URLRequest] completion: ^(UIImage *downloadedImage, NSError *error)
     {
         if (error)
         {
             // TODO: error handling
             //[self alertError: error.localizedDescription];
             completion(nil);
         }
         
         completion(downloadedImage);
         
         // save to temp in background
         [self saveToTemp:downloadedImage withName: imageName];
     }];
}
+ (void)saveToTemp:(UIImage*)image withName:(NSString*)imageName
{
    dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(bgQueue, ^{
        NSString *tempPath = [NSString stringWithFormat: @"%@%@", NSTemporaryDirectory(), imageName];
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
        [imageData writeToFile: tempPath atomically: YES];
    });
    
}

+ (UIImage*)imageFromTemp:(NSString*)imageName
{
    NSString *tempPath = [NSString stringWithFormat: @"%@%@", NSTemporaryDirectory(), imageName];
    if ([[NSFileManager defaultManager] fileExistsAtPath: tempPath])
    {
        return [UIImage imageWithContentsOfFile: tempPath];
    }
    
    return nil;
}
+ (NSString*)screenTitleForDelivery:(DeliveryMethod)deliveryType
{
    return @"Delivery";
    NSString *title = nil;
    switch (deliveryType) {
        case DeliveryMethodNone:
            break;
        
        case DeliveryMethodDelivery:
            title = @"Delivery";
            break;
        case DeliveryMethodPickup:
            title = @"Pickup";
            break;
        case DeliveryMethodSitting:
            title = @"Eat Outside";
            break;
    }
    if ([DataManager quickOrder])
        title = @"Start Order Now";
    
    return title;
}

+ (void)addBoldAttributeToString:(NSMutableAttributedString*)attrString forText:(NSString*)text font:(CGFloat)size
{
    NSDictionary *boldAttribute = @{NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: size]};
    if(text)
    {
    NSRange strRange = [attrString.string rangeOfString: text];
    [attrString addAttributes: boldAttribute range: strRange];
    }
}

+ (void)handleScreenTouch:(dispatch_block_t)handler
{
    _blockHandler = handler;
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    UIView *screenOverlay = [[UIView alloc] initWithFrame: window.bounds];
    screenOverlay.backgroundColor = [UIColor clearColor];
    [window addSubview: screenOverlay];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(handleScreenTap:)];
    [screenOverlay addGestureRecognizer: pan];
    pan.cancelsTouchesInView = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(handleScreenTap:)];
    [screenOverlay addGestureRecognizer: tap];
    tap.cancelsTouchesInView = YES;
}

+ (void)handleScreenTap:(UITapGestureRecognizer*)sender
{
    [sender.view removeFromSuperview];
//    [sender.view removeGestureRecognizer:sender];
    if(_blockHandler)
    {
    _blockHandler();
    _blockHandler = nil;
    }
}
+ (NSArray*)requestBoundariesForLocation:(CLLocationCoordinate2D)location
{
    CLLocationDegrees degreeInPoints = 0.017f;

    CLLocationDegrees northPointLat = location.latitude + 2 * degreeInPoints;
    CLLocationDegrees southPointLat = location.latitude - 2 * degreeInPoints;
    CLLocationDegrees eastPointLng = location.longitude + 2 * degreeInPoints;
    CLLocationDegrees westPointLng = location.longitude - 2 * degreeInPoints;
    return @[@(northPointLat), @(southPointLat), @(eastPointLng), @(westPointLng)];
}
+ (CLLocationCoordinate2D)defaultLocation
{
    return CLLocationCoordinate2DMake(40.7770784, -73.9824074);
}
@end

@implementation AppRedButton
- (void)awakeFromNib
{
    [self setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [self setTitleColor: [UIColor whiteColor] forState: UIControlStateHighlighted];
    if (self.titleLabel.text.length)
    {
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString: self.titleLabel.text attributes: @{NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: 19]}];
        [self setAttributedTitle: attributedString forState: UIControlStateNormal];
    }
    [self setBackgroundImage: [UIImage imageNamed: @"btn_fb_clicked"] forState: UIControlStateNormal];
    [self setBackgroundImage: [UIImage imageNamed: @"btn_fb_normal"] forState: UIControlStateHighlighted];
    CGRect rect = self.bounds;
    rect.size = [UIImage imageNamed: @"btn_fb_clicked"].size;
    self.bounds = rect;
}
@end

@implementation AppTextField

- (void)awakeFromNib
{
    _whiteBackground = [UIImage imageNamed: @"empty_field"];
    _redBackground = [UIImage imageNamed: @"empty_field"];
    NSString *placeholder = self.placeholder;
    if (placeholder == nil)
    {
        placeholder = @"";
    }
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder  attributes: @{NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: 22], NSForegroundColorAttributeName : [Utils appPrimaryColor]}]; //RGB(186, 142, 141)}];
    [self setAttributedPlaceholder: attributedString];
    self.textColor = [Utils appPrimaryColor];
    self.font = [UIFont fontWithName: @"ArialMT" size: 22];
    self.borderStyle = UITextBorderStyleNone;
    self.background = _redBackground;
    
    if ([Utils isIOS7])
    {
        self.tintColor = [Utils appPrimaryColor];
    }else
    {
        [[self valueForKey:@"textInputTraits"] setValue: [Utils appPrimaryColor] forKey:@"insertionPointColor"];
    }
}

- (void)setPlaceholder:(NSString *)placeholder
{
    
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:placeholder  attributes: @{NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: 22], NSForegroundColorAttributeName : [Utils appPrimaryColor]}]; //RGB(186, 142, 141)}];
    [self setAttributedPlaceholder: attributedString];

}
- (void)setFilled:(BOOL)filled
{
    _filled = filled;
    self.background = filled ? _whiteBackground : _redBackground;
   // self.textColor = filled ? [Utils appPrimaryColor] : RGB(50, 50, 50);
}


@end