//
//  CustomAlert.h
//  Snapcious
//
//  Created by Vadim Pavlov on 21.06.13.
//  Copyright (c) 2013 Assist Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlert : UIView
+ (void)showCustomAlert:(NSString*)title msg:(NSString*)message button:(NSString*)btn alignment:(NSTextAlignment)alignment completion:(dispatch_block_t)handler;
+ (void)showCustomAlertWithURL:(NSString*)url button:(NSString*)btn;

@end
