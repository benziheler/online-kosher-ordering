//
//  AppStyleButton.m
//  10bis
//
//  Created by Anton Vilimets on 5/16/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "AppStyleButton.h"

@implementation AppStyleButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup
{
    self.layer.cornerRadius = 3.0;
    self.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:19];
    [self setBackgroundColor:[Utils appPrimaryColor]];
    [self setTitleColor:[Utils appTextColor] forState:UIControlStateNormal];
}

@end
