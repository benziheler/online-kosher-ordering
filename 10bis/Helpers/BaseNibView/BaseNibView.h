//
//  BaseNibView.h
//  FBTestApp
//
//  Created by Anton Vilimets on 8/27/13.
//  Copyright (c) 2013 Anton Vilimets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNibView : UIView

+ (UIView *)viewFromNib;

@end
