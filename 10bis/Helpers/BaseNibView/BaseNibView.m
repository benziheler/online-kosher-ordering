//
//  BaseNibView.m
//  FBTestApp
//
//  Created by Anton Vilimets on 8/27/13.
//  Copyright (c) 2013 Anton Vilimets. All rights reserved.
//

#import "BaseNibView.h"

@implementation BaseNibView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (UIView *)viewFromNib
{
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass(self.class) owner: self options: nil];
    return xib[0];
}

- (id) awakeAfterUsingCoder:(NSCoder*)aDecoder {
    BOOL isNibNotLoaded = ([[self subviews] count] == 0);
    if (isNibNotLoaded) {
        UIView* galeryView = (id) [[self class] viewFromNib];
        galeryView.frame = self.frame;
        galeryView.autoresizingMask = self.autoresizingMask;
        galeryView.alpha = self.alpha;
        return galeryView;
    }
    return self;
}

@end
