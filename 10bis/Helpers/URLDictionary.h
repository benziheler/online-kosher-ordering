//
//  SPDictionary.h
//  Speclet
//
//  Created by Vadim Pavlov on 30.10.11.
//  Copyright (c) 2011 HaskiTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLDictionary : NSObject
{
    NSMutableDictionary *dictionary_;
}

+ (id)dictionaryWithCapacity:(NSInteger)itemsNum;
- (id)initWithCapacity:(NSInteger)itemsNum;
- (void)setObject:(id)anObject forKey:(id)aKey;
- (void)removeObjectForKey:(id)aKey;
- (NSUInteger)count;
- (id)objectForKey:(id)aKey;
- (NSEnumerator *)keyEnumerator;

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;

- (NSString*)getParametrs;
- (NSData *)postParametrs;
- (NSDictionary*)dictionary;
+ (NSString *)encodeToPercentEscapeString:(NSString *)string;

@end
