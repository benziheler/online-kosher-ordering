//
//  AppStyleButton.h
//  10bis
//
//  Created by Anton Vilimets on 5/16/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppStyleButton : UIButton

@end
