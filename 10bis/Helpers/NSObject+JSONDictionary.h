//
//  NSObject+JSONDictionary.h
//  10bis
//
//  Created by Vadim Pavlov on 12.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (JSONDictionary)
- (NSDictionary*)jsonDictionary;
@end
