//
//  ConnectionManager.m
//  Onset
//
//  Created by Noam Ma-Yafit on 02/09/10.
//  Copyright 2010 OnO Apps. All rights reserved.
//

#import <objc/message.h>
#import "CommManager.h"
#import "ASIFormDataRequest.h"

#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject; \

static CommManager *sharedConnectionManager = nil;

@implementation CommManager

@synthesize delegate;
@synthesize _reachability;
@synthesize _isOfflineMode;

#pragma mark Singleton Methods
+ (id)sharedManager {
	DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if(sharedConnectionManager == nil)  {
			sharedConnectionManager = [super allocWithZone:zone];
			return sharedConnectionManager;
		}
	}
	return nil;
}

#pragma mark -
#pragma mark NetworkStatus
-(void)startNotifyingOfInternetChanges
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name: kReachabilityChangedNotification object:nil];
    self._reachability = [Reachability reachabilityForInternetConnection];
    [self._reachability startNotifier];
}

-(void)reachabilityChanged:(NSNotification *)notif
{
    Reachability* curReach = [notif object];
	NetworkStatus internetStatus = [curReach currentReachabilityStatus];
	switch (internetStatus)
	{
		case NotReachable:
		{
            break;
		}
		default:
			break;
	}
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityChangedNotification object:nil];
}

+(BOOL)checkReachability
{
	Reachability* internetReachable = [Reachability reachabilityForInternetConnection];
	NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
	switch (internetStatus)
	
	{
		case NotReachable:
		{
			return NO;
			break;
		}
		default:
			return YES;
			break;
	}
	return YES;
}

-(BOOL)isOnlineMode{
    return (!_isOfflineMode && [CommManager checkReachability]);
}

#pragma mark - ASIHTTP 
//Connection to server
- (void)grabURLInBackground:(NSString *)urlString withDelegate:(id)dlg useQueue:(BOOL)useQueue withData:(NSDictionary *)data finishSelector:(NSString *)finishSel failSelector:(NSString *)failSel queueFinishSel:(SEL)queueFinishSel userInfo:(NSDictionary *)userInfo useJSON:(BOOL)shouldUseJSON isResponseIsInJSON:(BOOL)isResponseIsInJSON requestMethod:(eRequestMethod)requestMethod useHTTPS:(BOOL)shouldUseHTTPS continueInBackground:(BOOL)shouldContinueInBackground headers:(NSDictionary *)headers numberOfRetriesOnFail:(int)numberOfRetries completionBlock:(void (^)(id response))completionBlock failBlock:(void (^)(ASIHTTPRequest *result))failBlock timeoutInSec:(int)timeoutInSec
{
    if (!_queue && useQueue) {
        _queue = [[ASINetworkQueue alloc] init];
    }
	NSURL *url = [NSURL URLWithString:urlString];
    //[ASIHTTPRequest setDefaultUserAgentString:@"Online Ordering 1.0"];

	ASIFormDataRequest *_request = [ASIFormDataRequest requestWithURL:url];
    [_request setDelegate:self];
    

    
    NSString *method;
    switch (requestMethod) {
        case kPOST:
            method = @"POST";
            break;
        case kGET:
            method = @"GET";
            break;
        case kDELETE:
            method = @"DELETE";
            break;
        case kPUT:
            method = @"PUT";
            break;
        default:
            method = @"POST";
            break;
    }
    [_request setRequestMethod:method];
    [_request setValidatesSecureCertificate:!shouldUseHTTPS];
	[_request setTimeOutSeconds:timeoutInSec];
    [_request setShouldContinueWhenAppEntersBackground:shouldContinueInBackground];
    [_request setNumberOfTimesToRetryOnTimeout:numberOfRetries];


    if (shouldUseJSON && data){
        
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"jsonString %@",jsonString);
        [_request appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
    }else{
        NSArray *allKeys = [data allKeys];
        
        for (NSString *obj in allKeys)
        {
            [_request setPostValue:[data objectForKey:obj] forKey:obj];
        }
    }
    
    if (headers) {
        for (NSString *key in headers.allKeys)
        {
            [_request addRequestHeader:key value:[headers valueForKey:key]];
        }
    }
    else
    {
        [_request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
    }
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    
    [_request setUserInfo:info];
    
    if (finishSel) {
        [info setValue:finishSel forKey:@"finishSelector"];
    }
    
    if (failSel) {
        [info setValue:failSel forKey:@"failSelector"];
    }
    
    if (completionBlock) {
        [info setValue:[completionBlock copy] forKey:@"completionBlock"];
    }
    
    if (failBlock) {
        [info setValue:[failBlock copy] forKey:@"failBlock"];
    }
    
    if (dlg) {
        [info setValue:dlg forKey:@"delegate"];
    } else {
        [_request setDelegate:self];
    }
    
    NSNumber *responseType = [NSNumber numberWithBool:isResponseIsInJSON];
    [info setValue:responseType forKey:@"isWithJsonResponse"];
    
    if (userInfo) {
        [info setObject:userInfo forKey:@"userInfo"];
    }
	
    if (info) {
        [_request setUserInfo:info];
    }
    
    if (useQueue) {
        [_queue addOperation:_request]; //queue is an NSOperationQueue
        [_queue go];
        
    } else {
        [_request startAsynchronous];
    }
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    id response;
    if ([[[request userInfo] objectForKey:@"isWithJsonResponse"] boolValue] == YES) {

        NSError *error;
        response = [NSJSONSerialization JSONObjectWithData:[request responseData] options:(NSJSONReadingMutableContainers | NSJSONReadingAllowFragments) error:&error];
      //  NSLog(@"error %@", error.localizedDescription);
    }
    else
    {
        response = [request responseString];
    }
    
//    DebugLog(@"response: %@", response);
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[request userInfo]];
    [userInfo setValue:response forKey:@"responseData"];
    
    if ([request.userInfo objectForKey:@"completionBlock"]) {
        void (^completionBlock)(id response) = [request.userInfo objectForKey:@"completionBlock"];
        if (completionBlock) {
            completionBlock(response);
        }
    }
    else if ([request.userInfo objectForKey:@"delegate"]) {
        id dlg = [request.userInfo objectForKey:@"delegate"];
        SEL finishSel = NSSelectorFromString([request.userInfo objectForKey:@"finishSelector"]);
        
        if (dlg && finishSel && [dlg respondsToSelector:finishSel]) {
            objc_msgSend(dlg, finishSel, userInfo);
        }
    }
}
- (void)requestFailed:(ASIHTTPRequest *)request
{    
    if ([[request error] code] != 4) { //user cancel
        if ([self.delegate respondsToSelector:@selector(requestFailed:)]) {
            [self.delegate requestFailed:request];
        }
        
        if ([request.userInfo objectForKey:@"failBlock"]) {
            void (^failBlock)(ASIHTTPRequest *request) = [request.userInfo objectForKey:@"failBlock"];
            if (failBlock) {
                failBlock(request);
            }
        }
        else if ([request.userInfo objectForKey:@"delegate"]) {
            id dlg = [request.userInfo objectForKey:@"delegate"];
            SEL failSel = NSSelectorFromString([request.userInfo objectForKey:@"failSelector"]);
            if (failSel) {
                if (dlg && failSel && [dlg respondsToSelector:failSel]) {
                    objc_msgSend(dlg, failSel, [NSDictionary dictionaryWithDictionary:request.userInfo]);
                }
            }
        }  
    }
}

- (void)stopConnection
{
	for (ASIHTTPRequest *operation in [_queue operations]) {
		[operation setDelegate:nil];
		[operation cancel];
	}
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}
@end
