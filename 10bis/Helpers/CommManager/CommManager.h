//
//  ConnectionManager.h
//  Onset
//
//  Created by Noam Ma-Yafit on 02/09/10.
//  Copyright 2010 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "Reachability.h"

@protocol CommManagerProtocol 
@required
- (void)requestFinished:(ASIHTTPRequest *)request;
- (void)requestFailed:(ASIHTTPRequest *)request;
@end

typedef enum {
    kPOST,
    kGET,
    kDELETE,
    kPUT
} eRequestMethod;

@interface CommManager : NSObject {
	//a delegate so we get updates
	id				 delegate;
	ASINetworkQueue	*_queue;
    Reachability    *_reachability;
    BOOL _isOfflineMode;
}

@property (nonatomic, retain) id		delegate;
@property (nonatomic, retain) Reachability    *_reachability;
@property (nonatomic) BOOL _isOfflineMode;
+ (id) sharedManager;

//ASIHTTP
- (void)grabURLInBackground:(NSString *)urlString withDelegate:(id)dlg useQueue:(BOOL)useQueue withData:(NSDictionary *)data finishSelector:(NSString *)finishSel failSelector:(NSString *)failSel queueFinishSel:(SEL)queueFinishSel userInfo:(NSDictionary *)userInfo useJSON:(BOOL)shouldUseJSON isResponseIsInJSON:(BOOL)isResponseIsInJSON requestMethod:(eRequestMethod)requestMethod useHTTPS:(BOOL)shouldUseHTTPS continueInBackground:(BOOL)shouldContinueInBackground headers:(NSDictionary *)headers numberOfRetriesOnFail:(int)numberOfRetries completionBlock:(void (^)(id response))completionBlock failBlock:(void (^)(ASIHTTPRequest *result))failBlock timeoutInSec:(int)timeoutInSec;

//- (void)grabURLInBackground:(NSString *)urlString withDelegate:(id)dlg useQueue:(BOOL)useQueue withData:(NSDictionary *)data finishSelector:(NSString *)finishSel failSelector:(NSString *)failSel queueFinishSel:(SEL)queueFinishSel userInfo:(NSDictionary *)userInfo useJSON:(BOOL)shouldUseJSON isResponseIsInJSON:(BOOL)isResponseIsInJSON requestMethod:(NSString *)requestMethod useHTTPS:(BOOL)shouldUseHTTPS continueInBackground:(BOOL)shouldContinueInBackground;

- (void)requestFinished:(ASIHTTPRequest *)request;
- (void)requestFailed:(ASIHTTPRequest *)request;
- (void)stopConnection;
+ (BOOL)checkReachability;
- (BOOL)isOnlineMode;
-(void)startNotifyingOfInternetChanges;

@end
