
#import "Downloader.h"

@interface NSURLConnectionBlocksDelegate : NSObject <NSURLConnectionDataDelegate> {
	DownloadDataCompletionBlock completion;
	NSMutableData *data;
}

- (id)initWithBlock:(DownloadDataCompletionBlock)block;

@end

@implementation Downloader

- (id)init {
	[NSException raise:@"BKDownloadException" format:@"Do not alloc BKDownload!"];
	return nil;
}

+ (void)dataDownloadWithRequest:(NSURLRequest*)request completion:(DownloadDataCompletionBlock)completion {
	NSURLConnectionBlocksDelegate *delegate = [[[NSURLConnectionBlocksDelegate alloc] initWithBlock:completion] autorelease];
	
	[NSURLConnection connectionWithRequest:request delegate:delegate];
}

+ (void)imageDownloadWithRequest:(NSURLRequest*)request completion:(DownloadImageCompletionBlock)completion {
	[[self class] dataDownloadWithRequest:request completion:^(NSData *result, NSError *error) {
		completion(result == nil? nil: [UIImage imageWithData:result], error);
	}];
}

+ (void)stringDownloadWithRequest:(NSURLRequest*)request completion:(DownloadStringCompletionBlock)completion {
	[[self class] dataDownloadWithRequest:request completion:^(NSData *result, NSError *error) {
		completion(result == nil? nil: [[[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding] autorelease], error);
	}];
}

@end

@implementation NSURLConnectionBlocksDelegate

- (id)initWithBlock:(DownloadDataCompletionBlock)block {
	if ((self = [super init])) {
		completion = Block_copy(block);
		data = nil;
	}
	
	return [self retain];
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	data = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	completion(nil, error);
	[self release];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)newdata {
	[data appendData:newdata];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	completion(data, nil);
	[self release];
}

- (void)dealloc {
	Block_release(completion);
	[data release];
	
	[super dealloc];
}

- (void)connection: (NSURLConnection *)connection willSendRequestForAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge
{
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
}


@end

@implementation NSString (BKDownload)

- (NSURLRequest*)URLRequest {
	return [NSURLRequest requestWithURL:[NSURL URLWithString:self]];
}

@end
