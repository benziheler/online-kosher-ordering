//
//  CustomAlert.m
//  Snapcious
//
//  Created by Vadim Pavlov on 21.06.13.
//  Copyright (c) 2013 Assist Software Solutions. All rights reserved.
//

#import "CustomAlert.h"
#import <AVFoundation/AVFoundation.h>
#import "AppStyleButton.h"
#define kMaxWidth 280.0f
#define kMaxHeight 300.0f
#define kTitleOneLineHeight 33.0f
#define kMaxTitleHeight 50.0f
#define kBuffer 5.0f
#define kMaxMessageHeight (kMaxHeight - titleLabel.frame.size.height - 3*kBuffer - kButtonHeight)
#define kLabelAutosizeExtra 10.0f
#define kTextViewAutosizeExtra 30.0f

#define kOneButtonMaxWidth kMaxWidth/2
#define kButtonHeight 30.0f
#define kAnimateInDuration 0.4
#define kAnimateOutDuration 0.3
#define kOverlayMaxAlpha 0.4

@interface CustomAlert () <UIWebViewDelegate>
@property (nonatomic, copy) void (^okBlock)(BOOL dontShowAgain);
@property (nonatomic, copy) dispatch_block_t completion;
@end
@implementation CustomAlert
{
    UIWindow *_window;
    UIView *_overlay;
    UIButton *_checkButton;
    UIActivityIndicatorView *_indicator;
}

+ (void)showCustomAlert:(NSString*)title msg:(NSString*)message button:(NSString*)btn alignment:(NSTextAlignment)alignment completion:(dispatch_block_t)handler
{
    CustomAlert *alert = [[CustomAlert alloc] initWithTitle: title msg: message button: btn alignment: alignment link: nil];
    alert.completion = handler;
    [alert showAlertAnimated: YES];
}
+ (void)showCustomAlertWithURL:(NSString*)url button:(NSString*)btn
{
    CustomAlert *alert = [[CustomAlert alloc] initWithTitle: nil msg: nil button: btn alignment: NSTextAlignmentCenter link: url];
    [alert showAlertAnimated: YES];
}

- (id)initWithTitle:(NSString*)title msg:(NSString*)message button:(NSString*)btn alignment:(NSTextAlignment)alignment link:(NSString*)link
{
//    UIView *winParent = [[UIApplication sharedApplication].windows objectAtIndex:0];
//    _window = [winParent.subviews objectAtIndex:0];
    id <UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    _window = delegate.window;
            
    self = [super initWithFrame: _window.bounds];
    if (self)
    {
        UIView *whiteBackgroundView = [[UIView alloc] initWithFrame: CGRectZero];
        whiteBackgroundView.backgroundColor = [UIColor whiteColor];
        whiteBackgroundView.layer.cornerRadius = 3.0f;
        
        CGFloat offset = 20.0f;
        CGRect bgRect = CGRectZero;
        bgRect.origin.x = offset;
        bgRect.size.width = CGRectGetWidth(_window.bounds) - offset *2;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [Utils appPrimaryColor];
        titleLabel.text = title;
        titleLabel.numberOfLines = 0;
        titleLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.textAlignment = alignment;
        messageLabel.textColor = RGB(50, 50, 50);
        messageLabel.text = message;
        messageLabel.numberOfLines = 0;
        messageLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
        AppStyleButton *button = [AppStyleButton buttonWithType: UIButtonTypeCustom];
        CGRect rect = button.bounds;
        rect.size = [UIImage imageNamed: @"btn_fb_clicked"].size;
        button.bounds = rect;

        [button setTitle: btn forState: UIControlStateNormal];
        [button awakeFromNib];
        [button addTarget: self action: @selector(buttonTapped:) forControlEvents: UIControlEventTouchUpInside];
        button.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        [self addSubview: whiteBackgroundView];
        [whiteBackgroundView addSubview: titleLabel];
        [whiteBackgroundView addSubview: messageLabel];
        [whiteBackgroundView addSubview: button];

        UIWebView *webview = nil;
        if (link)
        {
            NSURL *url = [NSURL URLWithString: link];
            NSURLRequest *request = [NSURLRequest requestWithURL: url];
            webview = [[UIWebView alloc] init];
            webview.delegate = self;
            [webview loadRequest: request];
            [whiteBackgroundView addSubview: webview];
        }
        
        CGFloat textOffset = 20.0f;
        CGFloat textWidth = bgRect.size.width - textOffset *2;
        CGRect titleRect = CGRectZero;
        CGRect msgRect = CGRectZero;
        CGRect btnRect = button.frame;
        CGRect webRect = CGRectZero;
        
        titleRect.size = [titleLabel sizeThatFits: CGSizeMake(textWidth, CGFLOAT_MAX)];
        msgRect.size = [messageLabel sizeThatFits: CGSizeMake(textWidth, CGFLOAT_MAX)];
        
        titleRect.origin.x = msgRect.origin.x = textOffset;
        titleRect.size.width = msgRect.size.width = textWidth;
        
        titleRect.origin.y = offset;
        msgRect.origin.y = CGRectGetMaxY(titleRect) + 10.0f;


        CGFloat alertHeight = CGRectGetMaxY(msgRect) + offset + CGRectGetHeight(btnRect) + 20.0f;
        bgRect.size.height = MAX(alertHeight, link ? 400.0f : 200.0f);
        bgRect.origin.y = roundf((CGRectGetHeight(_window.bounds) - CGRectGetHeight(bgRect))/2);
        
        btnRect.size.width = 240.0f;
        btnRect.origin.y = CGRectGetHeight(bgRect) - CGRectGetHeight(btnRect) - 20.0f;
        btnRect.origin.x = roundf((CGRectGetWidth(bgRect) - CGRectGetWidth(btnRect))/2);
        
        whiteBackgroundView.frame = bgRect;
        titleLabel.frame = titleRect;
        messageLabel.frame = msgRect;
        button.frame = btnRect;
        
        webRect.origin.x = titleRect.origin.x;
        webRect.origin.y = titleRect.origin.y;
        webRect.size.width = titleRect.size.width;
        webRect.size.height = CGRectGetMinY(btnRect) - 5.0f - webRect.origin.y;
        webview.frame = webRect;
        
        if (webview)
        {
            _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
            [whiteBackgroundView addSubview: _indicator];
            _indicator.center = webview.center;
            [_indicator startAnimating];
            _indicator.hidesWhenStopped = YES;
        }
    }
    
    return self;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_indicator stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_indicator stopAnimating];
}
#pragma mark - Animate Alert
- (void)buttonTapped:(id)sender
{
    if (self.okBlock) {
        self.okBlock(_checkButton.selected);
    }
    if (self.completion)
        self.completion();
    [self dismissAlertAnimated:YES];
}
- (void)checkButtonTapped:(UIButton*)sender
{
    sender.selected = !sender.selected;
}

- (void)showAlertAnimated:(BOOL)animated {
    [self showOverlayAnimated:animated];
    [_window addSubview:self];
    
    if (animated) {
        self.alpha = 0;
        [UIView animateWithDuration:0.1 animations:^{self.alpha = 1.0;}];
        
        self.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
        
        CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        bounceAnimation.values = [NSArray arrayWithObjects:
                                  [NSNumber numberWithFloat:0.5],
                                  [NSNumber numberWithFloat:1.2],
                                  [NSNumber numberWithFloat:0.9],
                                  [NSNumber numberWithFloat:1.0], nil];
        bounceAnimation.duration = kAnimateInDuration;
        bounceAnimation.removedOnCompletion = NO;
        [self.layer addAnimation:bounceAnimation forKey:@"bounce"];
        
        self.layer.transform = CATransform3DIdentity;
    }
}

- (void)dismissAlertAnimated:(BOOL)animated {
    [self dismissOverlayAnimated:animated];
    
    [UIView animateWithDuration:kAnimateOutDuration animations:^{self.alpha = 0.0;}];
    
    self.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
    
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    bounceAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:1.0],
                              [NSNumber numberWithFloat:1.1],
                              [NSNumber numberWithFloat:0.5],
                              [NSNumber numberWithFloat:0.0], nil];
    bounceAnimation.duration = kAnimateOutDuration;
    bounceAnimation.removedOnCompletion = NO;
    bounceAnimation.delegate = self;
    [self.layer addAnimation:bounceAnimation forKey:@"bounce"];
    
    self.layer.transform = CATransform3DIdentity;
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {
    if (flag)
        [self removeFromSuperview];
}

#pragma mark - Animate Overlay

- (void)showOverlayAnimated:(BOOL)animated {
    _overlay = [[UIView alloc] initWithFrame:_window.bounds];
    _overlay.backgroundColor = [UIColor blackColor];
    _overlay.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [_window addSubview:_overlay];
    
    if (animated) {
        _overlay.alpha = 0;
        [UIView animateWithDuration:0.1 animations:^{
            _overlay.alpha = kOverlayMaxAlpha;
        }];
    } else {
        _overlay.alpha = kOverlayMaxAlpha;
    }
    
}

- (void)dismissOverlayAnimated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.1 animations:^{
            _overlay.alpha = 0.0;
        } completion:^(BOOL finished) {
            [_overlay removeFromSuperview];
        }];
    } else {
        [_overlay removeFromSuperview];
    }
}
@end
