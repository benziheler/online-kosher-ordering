//
//  Utils.h
//  UZ
//
//  Created by Vadim Pavlov on 16.10.13.
//  Copyright (c) 2013 Haski Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"

#define kTapNotification @"TapNotification"

extern id ValueOrNil (NSDictionary *dict, NSString *key);

#define RGB(r,g,b)      [UIColor colorWithRed:((r)/255.0) green:((g)/255.0) blue:((b)/255.0) alpha:1.0]
#define RGBA(r,g,b,a)   [UIColor colorWithRed:((r)/255.0) green:((g)/255.0) blue:((b)/255.0) alpha:(a)]

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)

#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width

@interface Utils : NSObject
+ (BOOL)isIOS7;

+ (UIColor*)appSecondaryColor;
+ (UIColor *)appPrimaryColor;
+ (UIColor*)appTextColor;

+ (void)callToNumber:(NSString *)number;

+ (void)showSuccessMessage:(NSString*)message;
+ (void)showErrorAlert:(NSError*)error;
+ (void)showErrorMessage:(NSString*)message;
+ (void)showNetworkErrorMessage:(NSString*)message;

+ (void)loadImage:(NSString*)photoUrl completion:(void (^)(UIImage* image))completion;
+ (BOOL)isEmailValid:(NSString*)email;

+ (UIImage*)iconForSocialNetwork:(NSString*)network;
+ (UIColor*)colorForSocialNetwork:(NSString*)network;
+ (UIButton*)menuButton;

+ (NSString*)screenTitleForDelivery:(DeliveryMethod)deliveryType;
+ (void)addBoldAttributeToString:(NSMutableAttributedString*)attrString forText:(NSString*)text font:(CGFloat)size;

+ (void)handleScreenTouch:(dispatch_block_t)handler;
+ (NSArray*)requestBoundariesForLocation:(CLLocationCoordinate2D)location;
+ (CLLocationCoordinate2D)defaultLocation;
@end



@interface AppRedButton : UIButton

@end

@interface AppTextField : UITextField
@property (nonatomic, readonly) UIImage *whiteBackground;
@property (nonatomic, readonly) UIImage *redBackground;
@property (nonatomic, assign) BOOL filled;
@end