//
//  Policy.m
//  10bis
//
//  Created by Vadim Pavlov on 11.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Policy.h"

@interface Policy ()

@end

@implementation Policy
{
    __weak IBOutlet UIWebView *_webView;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *url = [NSString stringWithFormat:@"%@/Info/PrivacyPolicies?isMobileUser=true", [DataManager baseUrl]];
    [_webView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];

}

@end
