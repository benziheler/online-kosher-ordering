//
//  MenuVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "MenuVC.h"
#import "DataManager.h"
#import "User.h"
#import <GigyaSDK/Gigya.h>
#import "SlidingVC.h"
#import "WelcomeVC.h"
//#import "AboutVC.h"
#import "WebVC.h"
#import "TouVC.h"
#import "Policy.h"
#import "FilterVC.h"

@interface MenuVC () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation MenuVC
{
    __weak IBOutlet UITableView *_tableView;
    IBOutlet UIView *_userView;
    __weak IBOutlet UIImageView *_userPhotoView;
    __weak IBOutlet UILabel *_userLabel;
    
    NSArray *_cells;
    NSInteger _selectedTab;
    __weak IBOutlet UIView *_shadowView;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.backgroundColor = [Utils appSecondaryColor];
	// Do any additional setup after loading the view.
    _userView.backgroundColor = [Utils appPrimaryColor];
    _userLabel.textColor = [Utils appTextColor];
    _selectedTab = -1;
    _userPhotoView.layer.cornerRadius = 4.0f;
    _shadowView.layer.shadowOffset = CGSizeMake(-5, 0);
    _shadowView.layer.shadowOpacity = 0.2f;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [self updateData];
}
- (void)updateData
{
    User *user = [DataManager currentUser];
    _userPhotoView.image = [UIImage imageNamed: @"icon-avatar"];
//    NSString *serverTitle = [NSString stringWithFormat: @"API: %@", [DataManager debugMode] ? @"10.10.200.177" : @"www.10bis.co.il"];
  //  NSString *aboutTitle = [NSString stringWithFormat:@"About %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
    NSMutableArray *cells = [NSMutableArray arrayWithObjects:
                             @{@"title": @"Last orders", @"image": [UIImage imageNamed: @"icon-doch"]},
                              @{@"title": @"Credit card" , @"image": [UIImage imageNamed: @"icon-credit_cards"]},
                             @{@"title": @"About Us", @"image": [UIImage imageNamed: @"icn_about_us"]},
                             @{@"title": @"Contact Support", @"image": [UIImage imageNamed: @"icn_contact"]},
                              @{@"title": @"Own a Restaurant?", @"image": [UIImage imageNamed: @"icn_own_rest"]},
                            @{@"title": @"Terms of Use", @"image": [UIImage imageNamed: @"icn_tou"]},
                            @{@"title": @"Privacy Policy", @"image": [UIImage imageNamed: @"icn_pp"]},
                             @{@"title": @"Rate Us", @"image": [UIImage imageNamed: @"icon-rate"]},
                                                          @{@"title": @"Settings", @"image": [UIImage imageNamed: @"icon-settings"]},
                             @{@"title": @"Log out", @"image": [UIImage imageNamed: @"icon-disconnect"]},
//                             @{@"title": serverTitle, @"image" : [NSNull null]},
                             nil];

    if (user)
    {
        _userLabel.text = [NSString stringWithFormat: @"%@ %@", user.firstName, user.lastName];
        [Utils loadImage: user.photoURL completion:^(UIImage *image) {
            _userPhotoView.image = image;
        }];
    }
    else
    {
        _userLabel.text = @"User name";
        [cells replaceObjectAtIndex: cells.count-1 withObject: @{@"title": @"Log in", @"image": [UIImage imageNamed: @"icon-disconnect"]}];
        [cells removeObjectAtIndex:cells.count-2];
        [cells removeObjectsInRange: NSMakeRange(0, 2)];
        
    }
    _cells = cells;
    [_tableView reloadData];

}
- (IBAction)clickOnUserHeader:(id)sender
{
    UINavigationController *naviVC = (UINavigationController*)self.slidingViewController.topViewController;
    [naviVC popToRootViewControllerAnimated: NO];
    WelcomeVC *welcomeVC = (WelcomeVC*)naviVC.topViewController;

    if ([DataManager currentUser])
    {
        WebVC *webVC = [self webVC: [DataManager profileURL] title: @"Personal Information"];
        [welcomeVC.navigationController pushViewController: webVC animated: YES];
    }
    else
    {
        [welcomeVC performSegueWithIdentifier: @"existUserLogin" sender: self];
    }
    [self.slidingViewController resetTopView];

}
- (WebVC*)webVC:(NSString*)webURL title:(NSString*)title
{
    _selectedTab = -1;
    WebVC *webVC = [self.storyboard instantiateViewControllerWithIdentifier: @"WebVC"];
    webVC.urlToLoad = webURL;
    webVC.title = title;
    webVC.showNavigationBar = YES;
    return webVC;
}
#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cells.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [Utils appPrimaryColor];
        cell.textLabel.font = [UIFont fontWithName: @"ArialMT" size: 18];
        cell.textLabel.backgroundColor = [Utils isIOS7] ? [UIColor clearColor] : [UIColor whiteColor];
        UIView *separator = [[UIView alloc] initWithFrame: CGRectMake(0, 59, DEVICE_WIDTH - 50, 1)];
        separator.backgroundColor = [Utils appPrimaryColor];
        [cell.contentView addSubview: separator];
    }

    NSDictionary *tab = _cells[indexPath.row];
    cell.textLabel.text = tab[@"title"];
    
    UIImage *image = tab[@"image"];
    if ([image isKindOfClass: [UIImage class]])
    {
        cell.imageView.image = image;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    else
    {
        cell.imageView.image = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    NSDictionary *cell = _cells[indexPath.row];
    NSString *cellTitle = cell[@"title"];
    NSInteger cellIndex = [_cells indexOfObject: cell];
    
    User *user = [DataManager currentUser];
    if (user == nil)
    {
        cellIndex += 2;
    }


    if (cellIndex == _selectedTab)
    {
        [self.slidingViewController resetTopView];
        return;
    }
    
    _selectedTab = cellIndex;
    UINavigationController *naviVC = (UINavigationController*)self.slidingViewController.topViewController;
    [naviVC popToRootViewControllerAnimated: NO];
    WelcomeVC *welcomeVC = (WelcomeVC*)naviVC.topViewController;
    
    switch (cellIndex) {
        case 0:
        {
            WebVC *webVC = [self webVC: [DataManager reportURL] title: cellTitle];
            [welcomeVC.navigationController pushViewController: webVC animated: YES];
        }
            break;

        case 1:
        {
           
            WebVC *webVC = [self webVC: [DataManager creditCardsURL] title: cellTitle];
            [welcomeVC.navigationController pushViewController: webVC animated: YES];

        }
            break;
        case 2:
            _selectedTab = -1;
            [welcomeVC performSegueWithIdentifier: @"AboutSegue" sender: self];
            break;
        case 3:
        {
            WebVC *webVC = [self webVC: [DataManager contactURL] title: cellTitle];
            [welcomeVC.navigationController pushViewController: webVC animated: YES];
            break;
        }
        case 4:
        {
            WebVC *webVC = [self webVC: [DataManager ownRestaurantUrl] title: cellTitle];
            [welcomeVC.navigationController pushViewController: webVC animated: YES];
        }
            break;

        case 5:
        {
            WebVC *webVC = [self webVC: [DataManager touURL] title: cellTitle];
            [welcomeVC.navigationController pushViewController: webVC animated: YES];
          /*  // not used now
            _selectedTab = -1;
            BOOL debug = [DataManager debugMode];
            [DataManager setDebugMode: !debug];
            [self updateData];
            return;*/
        }
            break;
            case 6:
        {
            WebVC *webVC = [self webVC: [DataManager policyURL] title: cellTitle];
            [welcomeVC.navigationController pushViewController:webVC animated: YES];
        }
            break;
            case 7:
        {
            NSString *appId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ItunesAppId"];
            NSString *iOS7AppStoreURLFormat = [NSString stringWithFormat: @"itms-apps://itunes.apple.com/app/id%@", appId];
            NSString *iOSAppStoreURLFormat = [NSString stringWithFormat:  @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", appId];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: [Utils isIOS7] ? iOS7AppStoreURLFormat : iOSAppStoreURLFormat]];
        }
            break;
            case 8:
        {
            if(user)
            {
                //show settings
                _selectedTab = -1;
                FilterVC *filterVC = [[FilterVC alloc] initWithNibName:nil bundle:nil];
                filterVC.title = cellTitle;
                [welcomeVC.navigationController pushViewController: filterVC animated: YES];
            }
            else
            {
                _selectedTab = -1;
                [welcomeVC performSegueWithIdentifier: @"existUserLogin" sender: self];
            }
            
        }
            break;
            case 9:
        {
            [DataManager userLogout];
            [Gigya logout];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLogOutKey object:nil];
        }
            break;
    }
    
    [self.slidingViewController resetTopView];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0f;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame: CGRectZero];
}

@end
