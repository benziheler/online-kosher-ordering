//
//  WebVC.m
//  10bis
//
//  Created by Vadim Pavlov on 04.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "WebVC.h"
#import "Dish.h"
@interface WebVC () <UIWebViewDelegate>

@end

@implementation WebVC
{
    __weak IBOutlet UIWebView *_webView;
    __weak IBOutlet UIActivityIndicatorView *_indicator;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if (_showNavigationBar == NO)
        [self.navigationController setNavigationBarHidden: YES animated: YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    if (_showNavigationBar == NO)
        [self.navigationController setNavigationBarHidden: NO animated: YES];
    [super viewWillDisappear: animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [Utils appPrimaryColor];
    [self configurateNavigationBar];
    if (_showNavigationBar)
    {
        _webView.frame = self.view.bounds;
    }
    if (_urlToLoad)
    {
        [self startLoading];
    }
}
- (void)configurateNavigationBar
{
    // screen title
    UILabel *titleLabel = [[UILabel alloc]initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 30)];
    NSDictionary *attrs = [[UINavigationBar appearance] titleTextAttributes];
    titleLabel.font = attrs[NSFontAttributeName];
    titleLabel.textColor = attrs[NSForegroundColorAttributeName];
    titleLabel.backgroundColor = [UIColor clearColor];
    
    titleLabel.text = self.title;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *titleWrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 40)];
    [titleWrapView addSubview: titleLabel];
    titleWrapView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleWrapView;
}
- (void)setUrlToLoad:(NSString *)urlToLoad
{
    _urlToLoad = urlToLoad;
    if (self.isViewLoaded)
    {
        [self startLoading];
    }
}
- (void)startLoading
{
    NSURLRequest* request = [NSURLRequest requestWithURL: [NSURL URLWithString: _urlToLoad]];
    _webView.delegate = self;
    [_webView loadRequest: request];

}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = request.URL.absoluteString;
    NSString *backPrefix = @"tenbis://back";
    NSString *homePrefix = @"tenbis://home";
    
    if ([requestString isEqualToString: backPrefix])
    {
        [self.navigationController popViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString isEqualToString: homePrefix])
    {
        [DataManager loginOnInit];
        [self.navigationController popToRootViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString hasPrefix: backPrefix])
    {
        // TODO: extract encryptedUserId and login
        [self.navigationController popViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString hasPrefix: homePrefix])
    {
        // TODO: extract encryptedUserId and login
        [self.navigationController popToRootViewControllerAnimated: YES];
        return NO;
    }
    
    return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [_indicator stopAnimating];
}
@end
