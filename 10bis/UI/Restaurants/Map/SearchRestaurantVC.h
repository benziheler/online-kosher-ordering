//
//  SearchRestaurantVC.h
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
#import "DataManager.h"
@class Address;
@interface SearchRestaurantVC : BaseVC
@property (nonatomic, strong) NSArray *restaurants;

@property (nonatomic, assign) CLLocationCoordinate2D selectedLocation;
@property (nonatomic, strong) Address *address;
@property (nonatomic, strong) NSString *addressString;

@end
