//
//  RestaurantAnnotation.h
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@class Restaurant, RestaurantCluster;
@interface RestaurantAnnotation : NSObject <MKAnnotation, NSCopying>
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) RestaurantCluster *cluster;
@property (nonatomic, assign) NSInteger clusterSelectedIndex;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
//@property (nonatomic, readonly, copy) NSString *title;
//@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, assign) BOOL isShowed;
+ (id)annotationWithRestaurant:(Restaurant*)restaurant;
- (id)initWithRestaurant:(Restaurant*)restaurant;
- (id)initWithCluster:(RestaurantCluster*)restaurantsCluster;
@end
