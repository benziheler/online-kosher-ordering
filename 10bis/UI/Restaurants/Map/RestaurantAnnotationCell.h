//
//  RestaurantAnnotationCell.h
//  10bis
//
//  Created by Anton Vilimets on 9/3/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarsView.h"

@class Restaurant;

@interface RestaurantAnnotationCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *rateBgView;
@property (weak, nonatomic) IBOutlet UILabel *phoneOnlyLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIView *phoneOnlyView;
//@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet  UIImageView *bgUnavailable;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *unavailableLabel;
@property (weak, nonatomic) IBOutlet StarsView *starView;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *ribbonView;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UIButton *kosherButton;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UILabel *distLabel;

@property (weak, nonatomic) IBOutlet UIImageView *distIcon;
- (void)updateToRestaurant:(Restaurant*)restaurant;
- (IBAction)kosherDetailsPressed:(id)sender;
- (IBAction)showCusieneList:(UIButton*)sender;
- (IBAction)showAlert:(id)sender;
@property (nonatomic, copy) void (^showKosherDetails)(Restaurant *restaurant);


@end
