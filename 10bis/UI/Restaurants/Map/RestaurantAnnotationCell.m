//
//  RestaurantAnnotationCell.m
//  10bis
//
//  Created by Anton Vilimets on 9/3/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "RestaurantAnnotationCell.h"
#import "Restaurant.h"
#import "CustomAlert.h"

@implementation RestaurantAnnotationCell
{
    CGRect _nameRect;
    CGRect _addressRect;
    CGRect _cusineRect;
    Restaurant *_restaurant;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
        _nameLabel.textColor = [Utils appTextColor];
        _addressLabel.textColor = [Utils appTextColor];
        _leftLabel.textColor = [Utils appTextColor];
            _rightLabel.textColor = [Utils appTextColor];
            _distLabel.textColor = [Utils appTextColor];
        _orderLabel.textColor = [Utils appTextColor];
        _ratingLabel.textColor = [Utils appTextColor];
        _discountLabel.textColor = [Utils appTextColor];
        [_categoryButton setTitleColor:[Utils appPrimaryColor] forState:UIControlStateNormal];
        _phoneOnlyLabel.textColor = [Utils appTextColor];
        [_callButton setTitleColor:[Utils appTextColor] forState:UIControlStateNormal];

        if([[DataManager appDomain] isEqualToString:@"kosherordering"])
        {
            _kosherButton.hidden = NO;
        }
        else
        {
            _kosherButton.hidden = YES;
            _callButton.layer.borderColor = [UIColor whiteColor].CGColor;
            _callButton.layer.borderWidth = 1.1;
            _callButton.layer.cornerRadius = 2.5f;
            
        }
    
    [_categoryButton setValue: @(UIBaselineAdjustmentAlignCenters) forKeyPath: @"titleLabel.baselineAdjustment"];
    
    [_logoView setValue: @(2.0f) forKeyPath: @"layer.cornerRadius"];
    [_logoView setValue: @(YES) forKeyPath: @"layer.masksToBounds"];
        if([[DataManager appDomain] isEqualToString:@"kosherordering"])
        {
            _discountLabel.textColor = [Utils appPrimaryColor];
        }
    [_discountLabel setValue: [NSValue valueWithCGAffineTransform: CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(30))] forKey: @"transform"];
    _nameRect = _nameLabel.frame;
    _addressRect = _addressLabel.frame;
    _cusineRect = _categoryButton.frame;
    
    UIImage *bgImage = [_categoryButton backgroundImageForState: UIControlStateNormal];
    UIImage *strBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight:bgImage.size.height/2];

        [_categoryButton setBackgroundImage: strBgImage forState: UIControlStateNormal];
    
}

- (void)updateToRestaurant:(Restaurant*)restaurant
{
      _distLabel.text = restaurant.distanceString;

    _restaurant = restaurant;
    
    _nameLabel.text = restaurant.name;
    
    NSMutableArray *address = [NSMutableArray arrayWithCapacity: 2];
    //    if (restaurant.cityName.length)
    //        [address addObject: restaurant.cityName];
    if (restaurant.street.length)
        [address addObject: restaurant.street];
    _addressLabel.text = [address componentsJoinedByString: @", "];
    
    if(restaurant.numberOfReviews > 0)
    {
        _rateBgView.hidden = NO;
        _starView.hidden = NO;
        _ratingLabel.hidden = NO;
        NSString *reviewsText = [NSString stringWithFormat: @"%ld reviews", (long)restaurant.numberOfReviews];
        NSMutableAttributedString *attrReviewsText = [[NSMutableAttributedString alloc] initWithString: reviewsText];
        [Utils addBoldAttributeToString: attrReviewsText forText: [NSString stringWithFormat: @"%ld", (long)restaurant.numberOfReviews] font: 10];
        _ratingLabel.attributedText = attrReviewsText;
        [_starView setStars: restaurant.reviewsRank];
        
    }
    else
    {
        _starView.hidden = YES;
        _ratingLabel.hidden = YES;
        _rateBgView.hidden = YES;
        
    }
    
    
    // category
    [_categoryButton setTitle: restaurant.cuisineList forState: UIControlStateNormal];
    _categoryButton.hidden = restaurant.cuisineList.length == 0;
    
    _logoView.image = nil;
    
    [Utils loadImage: restaurant.logoURL completion:^(UIImage *image) {
            _logoView.image = image;
    }];
    
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    DeliveryMethod method = [DataManager deliveryMethod];
    switch (method) {
        case DeliveryMethodDelivery:
        {
            BOOL isDeliveryActive = restaurant.isActiveForDelivery;
            _rightIcon.hidden = YES;
            _rightLabel.hidden = YES;
            _distLabel.hidden = YES;
            _distIcon.hidden = YES;
            _orderLabel.hidden = NO;
            _unavailableLabel.text = @"Delivery Unavailable";
            _unavailableLabel.hidden = isDeliveryActive;
            _bgUnavailable.hidden = isDeliveryActive;
            _leftIcon.hidden = NO;
            _leftLabel.hidden = NO;
            
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
        }
            break;
        case DeliveryMethodPickup:
            _leftIcon.hidden = NO;
            _leftLabel.hidden = NO;
            _rightIcon.hidden = NO;
            _rightLabel.hidden = NO;
            _distLabel.hidden = NO;
            _distIcon.hidden = NO;
            _orderLabel.hidden = YES;
            _unavailableLabel.text = @"Pickup Unavailable";
            BOOL isPickupActive = restaurant.isActiveForPickup;
            _unavailableLabel.hidden = isPickupActive;
            _bgUnavailable.hidden = isPickupActive;
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            break;
        default:
            break;
    }
    _categoryButton.frame = _cusineRect;
    
    // discount
    _discountLabel.hidden = _ribbonView.hidden = couponHidden;
    if (method == DeliveryMethodSitting || restaurant.couponHasRestrictions)
        couponText = [@"*" stringByAppendingString: couponText];
    
    _discountLabel.text = couponText;
    _orderLabel.attributedText = nil;
    // hours or delivery fee
    if (method == DeliveryMethodDelivery)
    {
        _leftLabel.text = restaurant.phone;
        NSString *minOrder = nil;
        NSString *boldMinOrder = nil;
        NSString *deliveryPrice = nil;
        NSString *boldDeliveryPrice = nil;
        if (restaurant.minimumPriceForOrder == 0)
        {
            minOrder = @"No minimum order";
            boldMinOrder = minOrder;
        }
        else
        {
            minOrder = [NSString stringWithFormat: @"Minimum order: $%ld" , (long)restaurant.minimumPriceForOrder];
            boldMinOrder = [NSString stringWithFormat: @"$%ld", (long)restaurant.minimumPriceForOrder];
        }
        
        if (restaurant.deliveryPriceForOrder == 0)
        {
            deliveryPrice = @"Free Delivery";
            boldDeliveryPrice = deliveryPrice;
        }
        else
        {
            deliveryPrice = [NSString stringWithFormat: @"Delivery %@", restaurant.deliveryPrice];
            boldDeliveryPrice = [NSString stringWithFormat: @"%@", restaurant.deliveryPrice];
        }
        NSString *fullString = [NSString stringWithFormat: @"%@ | %@", minOrder, deliveryPrice];
        NSMutableAttributedString *attrOrder = [[NSMutableAttributedString alloc] initWithString: fullString];
        
        [Utils addBoldAttributeToString: attrOrder forText: boldMinOrder font: _orderLabel.font.pointSize];
        [Utils addBoldAttributeToString: attrOrder forText: boldDeliveryPrice font: _orderLabel.font.pointSize];
        _orderLabel.attributedText = attrOrder;
        _leftIcon.image = [UIImage imageNamed:@"icn_phone_white"];

    }
    else if (method == DeliveryMethodPickup)
    {
        _rightLabel.text = restaurant.phone;
        _leftLabel.text = restaurant.pickupActivityHours;
        _leftIcon.image = [UIImage imageNamed:@"icn_hours_white"];
        _rightIcon.image = [UIImage imageNamed:@"icn_phone_white"];
    }
    
    if (couponHidden)
    {
        CGRect nameRect = _nameRect;
        //nameRect.origin.x = 5.0f;
        nameRect.size.width = CGRectGetMaxX(_nameRect) - nameRect.origin.x;
        CGRect addressRect = _addressRect;
        addressRect.origin.x = nameRect.origin.x;
        addressRect.size.width = nameRect.size.width;
        _nameLabel.frame = nameRect;
        _addressLabel.frame = addressRect;
    }
    else
    {
        _nameLabel.frame = _nameRect;
        _addressLabel.frame = _addressRect;
    }
    _phoneOnlyView.hidden = YES;
    
    if(restaurant.phoneOrdersOnlyOnPortals)
    {
        _phoneOnlyView.hidden = NO;
        _addressLabel.hidden = YES;
        _leftLabel.hidden = YES;
        _rightLabel.hidden = YES;
        _distLabel.hidden = YES;
        _leftIcon.hidden = YES;
        _rightIcon.hidden = YES;
        _distIcon.hidden = YES;
        _orderLabel.hidden = YES;
    }

}
#pragma mark - Actions
- (IBAction)showAlert:(id)sender
{
   
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [CustomAlert showCustomAlertWithURL: url button: @"Close"];
}
- (IBAction)showCusieneList:(UIButton*)sender
{
    CGRect buttonRect = sender.frame;
    //  CGFloat offset = CGRectGetWidth(self.frame) - CGRectGetMaxX(buttonRect);
    buttonRect.size.width = CGRectGetWidth(self.frame) -  buttonRect.origin.x * 2;
    NSString *title = [sender titleForState: UIControlStateNormal];
    [sender setTitle: @"  " forState: UIControlStateNormal];
    [UIView animateWithDuration: 0.2f animations:^{
        if (sender.selected)
        {
            sender.frame = _cusineRect;
            [sender setTitle: title forState: UIControlStateNormal];
        }
        else
        {
            sender.frame = buttonRect;
            [sender setTitle: title forState: UIControlStateNormal];
            [Utils handleScreenTouch:^{
                [self showCusieneList: sender];
            }];
        }
        sender.selected = !sender.selected;
    }];
}

- (IBAction)kosherDetailsPressed:(id)sender
{
    if(_showKosherDetails)
    {
        _showKosherDetails(_restaurant);
    }
}

- (IBAction)callButtonPressed:(id)sender {
    [Utils callToNumber:_restaurant.phone];
    
}



@end
