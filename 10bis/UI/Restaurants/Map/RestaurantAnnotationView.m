//
//  RestaurantAnnotationView.m
//  10bis
//
//  Created by Vadim Pavlov on 29.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantAnnotationView.h"
#import "StarsView.h"
#import "RestaurantAnnotation.h"
#import "Restaurant.h"
#import "RestaurantCluster.h"
#import "NSArray+Sort.h"
#import "CustomAlert.h"
#import "RestaurantAnnotationCell.h"

@implementation RestaurantAnnotationView
{
    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UIPageControl *_pageControl;
    
    NSInteger _currentPage;
    CGFloat _oldOffset;
    BOOL _isAnimating;
    
}
+ (id)annotationView
{
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass(self.class) owner: self options: nil];
    return xib[0];
}
- (void)awakeFromNib
{
    [_collectionView registerNib:[UINib nibWithNibName:@"RestaurantAnnotationCell" bundle:nil] forCellWithReuseIdentifier:@"RestaurantAnnotationCell"];
    
    
  }
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
//    if (_collectionView.isDecelerating)
//    {
//        return nil;
//    }
//    return nil;
    return [super hitTest: point withEvent: event];
}
- (IBAction)callButtonPressed:(id)sender
{
    if(_annotation.restaurant)
    {
        [Utils callToNumber:_annotation.restaurant.phone];
    }
    else
    {
        Restaurant *restaurant = _annotation.cluster.restaurants[_currentPage];
        [Utils callToNumber:restaurant.phone];
    }
}


- (void)setAnnotation:(RestaurantAnnotation *)annotation
{
    
    _annotation = annotation;
    _currentPage = 0;
    _oldOffset = 0.0f;
    _collectionView.contentOffset = CGPointZero;
    

    
    if (annotation.restaurant)
    {
        _numberLabel.hidden = YES;
        _pageControl.numberOfPages = 0;
    }
    else if (annotation.cluster)
    {
        _numberLabel.hidden = NO;
        [self updateNumber];
        if([[DataManager appDomain] isEqualToString:@"kosherordering"])
        {
            [_pageControl setPageIndicatorTintColor:[UIColor whiteColor]];
            [_pageControl setCurrentPageIndicatorTintColor:[Utils appSecondaryColor]];
        }
    }
    
    [_collectionView reloadData];

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(_annotation.restaurant)
    {
        return 1;
    }
    else if (_annotation.cluster)
    {
        return _annotation.cluster.restaurants.count;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantAnnotationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RestaurantAnnotationCell" forIndexPath:indexPath];
    cell.showKosherDetails = _showKosherDetails;
    Restaurant *rest = nil;
    if(_annotation.restaurant)
    {
        rest = _annotation.restaurant;
    }
    else if (_annotation.cluster)
    {
        rest = _annotation.cluster.restaurants[indexPath.item];
    }
    [cell updateToRestaurant:rest];
    return cell;
    
}

- (void)updateNumber
{
    NSInteger numberOfRestaurans = _annotation.cluster.restaurants.count;
    _currentPage = _collectionView.contentOffset.x / 210;

    _numberLabel.text = [NSString stringWithFormat: @"%ld/%ld", _currentPage + 1, (long)numberOfRestaurans];
    NSInteger controlPage = _currentPage % 5;
    
    NSInteger bigPageOffset = (_currentPage / 5) * 5;
    _pageControl.numberOfPages = MIN(numberOfRestaurans - bigPageOffset, 5);
    _pageControl.currentPage = controlPage;
    _annotation.clusterSelectedIndex = _currentPage;
}



#pragma mark - Scroll View
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"Did End Decelerating");
    [self updateScrollView];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == NO)
        [self updateScrollView];
}
- (void)updateScrollView
{
    [self updateNumber];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
