//
//  RestaurantVC.h
//  10bis
//
//  Created by Vadim Pavlov on 01.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@class Restaurant;
@interface RestaurantVC : BaseVC
@property (nonatomic, strong) Restaurant *restaurant;

@end
