//
//  RestaurantAnnotation.m
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantAnnotation.h"
#import "Restaurant.h"
#import "RestaurantCluster.h"
@implementation RestaurantAnnotation

+ (id)annotationWithRestaurant:(Restaurant*)restaurant
{
    RestaurantAnnotation *annotation = [RestaurantAnnotation new];
    annotation.restaurant = restaurant;
    [annotation setCoordinate: CLLocationCoordinate2DMake(restaurant.latitude, restaurant.longitude)];
    [annotation setValue: restaurant.name forKey: @"title"];
    [annotation setValue: restaurant.activityHours forKey: @"subtitle"];
    return annotation;
}
- (id)initWithRestaurant:(Restaurant*)restaurant
{
    if (self = [super init])
    {
        _restaurant = restaurant;
        _coordinate = CLLocationCoordinate2DMake(restaurant.latitude, restaurant.longitude);
    }
    return self;
}
- (id)initWithCluster:(RestaurantCluster*)restaurantsCluster
{
    if (self = [super init])
    {
        _cluster = restaurantsCluster;
        _coordinate = restaurantsCluster.location.coordinate;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    RestaurantAnnotation *annotation = [RestaurantAnnotation new];
    annotation.restaurant = self.restaurant;
    annotation->_coordinate = self.coordinate;
    annotation->_cluster = self.cluster;
    return annotation;
}
@end
