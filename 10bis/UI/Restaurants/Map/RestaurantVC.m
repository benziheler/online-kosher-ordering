//
//  RestaurantVC.m
//  10bis
//
//  Created by Vadim Pavlov on 01.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantVC.h"

@interface RestaurantVC ()

@end

@implementation RestaurantVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
