//
//  RestaurantAnnotationView.h
//  10bis
//
//  Created by Vadim Pavlov on 29.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
@class RestaurantAnnotation;
@interface RestaurantAnnotationView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>
+ (id)annotationView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, copy) void (^showKosherDetails)(Restaurant *restaurant);

@property (nonatomic, strong) RestaurantAnnotation *annotation;
@end
