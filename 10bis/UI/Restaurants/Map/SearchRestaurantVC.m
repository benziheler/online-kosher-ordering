//
//  SearchRestaurantVC.m
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "SearchRestaurantVC.h"
#import <MapKit/MapKit.h>
#import "DataManager.h"
#import "Restaurant.h"
#import "RestaurantAnnotation.h"
#import "RestaurantMenuVC.h"
#import "RestaurantCell.h"
#import <CoreLocation/CoreLocation.h>
#import "UIView+Extensions.h"
#import "RestaurantVC.h"
#import "Address.h"
#import "StarsView.h"
#import "RestaurantAnnotationView.h"
#import "RestaurantCluster.h"
#import "DSActivityView.h"
#import "WebVC.h"
#import "FilterView.h"

@interface SearchRestaurantVC () <MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, copy) void (^showKosherDetails)(Restaurant *restaurant);


@end

@implementation SearchRestaurantVC
{
    NSDictionary *currFilterDict;
    __weak IBOutlet MKMapView *_mapView;
    __weak IBOutlet UIView *_listView;
    __weak IBOutlet UITableView *_tableView;
    
    __weak IBOutlet UIButton *_filterButton;
    __weak IBOutlet UILabel *_noResultLabel;
    __weak IBOutlet UIButton *_addressButton;
    __weak IBOutlet UIButton *_mapListButton;
    
    UITextField *_searchTF;
    UIView *_searchView;
    UISearchBar* _searchBar;
    
    CGRect _initialSearchRect;
    
    NSMutableArray *_restaurantsAnnotations;
    
    BOOL _userLocationFound;
    CLLocationCoordinate2D _location;
    CLGeocoder* _geocoder;
    
    __weak Restaurant *_selectedRestaurant;
    __weak RestaurantAnnotation *_showedAnnotation;
    __weak MKAnnotationView *_showedAnnotationView;
    
    UIBarButtonItem *_searchItem;
    UIBarButtonItem *_locationItem;
    
    BOOL _restaurantsFound;
    
    NSMutableArray *_filteredRestaurants;
    NSMutableArray *_restaurantsClusters;
    
    NSArray *_requestedBoundary;
    BOOL _isSearchingRestaurants;
    FilterView *filterView;
}
- (void)dealloc
{
    _mapView.delegate = nil;
    _mapView.showsUserLocation =  NO;
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _noResultLabel.textColor = [Utils appPrimaryColor];
	// Do any additional setup after loading the view.
    __weak SearchRestaurantVC *weakSelf = self;
    [self setShowKosherDetails:^(Restaurant *restaurant) {
        NSString *url = [NSString stringWithFormat:@"%@?resId=%@", [DataManager kosherDetailsUrl],restaurant.restaurantId.stringValue];

        [weakSelf showCustomAlertWithURL: url button: @"Close"];

    }];
    _geocoder = [CLGeocoder new];
    [_tableView registerNib: [UINib nibWithNibName: @"RestaurantCell" bundle: nil] forCellReuseIdentifier: @"RestaurantCell"];
    _restaurantsAnnotations = [NSMutableArray arrayWithCapacity: 100];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(tapOnMap:)];
    pan.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(tapOnMap:)];
    tap.delegate = self;
    
    [_mapView addGestureRecognizer: tap];
    [_mapView addGestureRecognizer: pan];
    [_addressButton setTitleColor:[Utils appPrimaryColor] forState:UIControlStateNormal];
    _addressButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _addressButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    CLLocationCoordinate2D defaultLocation = [Utils defaultLocation];
    _mapView.showsUserLocation = NO;
    if (_restaurants)
    {
        _restaurantsFound = YES;
        // already have list of restaurants loaded by address
        NSString *addressString = _addressString;
        if (addressString == nil)
            addressString = _address.addressString;
        
        [_addressButton setTitle: addressString forState: UIControlStateNormal];
        _mapView.userTrackingMode = MKUserTrackingModeNone;
        [self addMapAnnotations];
        if(_address )
        {
            [self zoomMapToCoords: CLLocationCoordinate2DMake(_address.latitude, _address.longitude)];
        }
        else if(CLLocationCoordinate2DIsValid(_selectedLocation))
        {
            [self zoomMapToCoords:_selectedLocation];
        }
        
        if([DataManager deliveryMethod] == DeliveryMethodPickup)
        {
            _requestedBoundary = [Utils requestBoundariesForLocation: CLLocationCoordinate2DMake(_address.latitude, _address.longitude)];
        }
        
    }
    else
    {
        // search restaurants by user current location
        _mapView.userTrackingMode = MKUserTrackingModeFollow;
        CLLocation *userLocation = [[DataManager sharedManager] currentLocation];
        if (userLocation)
        {
            [self proceedUserLocation: userLocation];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"An error occurred when determining your location. \n Please make sure that location services are enabled" delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
            [alert show];
            userLocation = [[CLLocation alloc] initWithLatitude: defaultLocation.latitude longitude: defaultLocation.longitude];
            [self proceedUserLocation: userLocation];
        }
    }
    
    // show list firstly for 'delivery' method
    DeliveryMethod method = [DataManager deliveryMethod];
    NSString *screenTitle = nil;
    if (method == DeliveryMethodDelivery)
    {
        screenTitle = @"Delivering Restaurants";
        _mapListButton.selected = NO;
        [self showMapList: _mapListButton];
    }
    else
    {
        screenTitle = @"Pickup Restaurants";
    }
    self.title = screenTitle;
    filterView = [FilterView filterView];
    [self.navigationController.view addSubview:filterView];
    filterView.frame = self.view.bounds;
    filterView.hidden = YES;
    NSDictionary *savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    if(savedDict)
    {
        NSString *cuisineFilter = savedDict[@"cuisine"];
        _filterButton.selected = (cuisineFilter != nil || [savedDict[kHasDiscountKey] boolValue] == YES);
        [filterView setupWithDict:savedDict];
    }
}

- (void)setRestaurants:(NSArray *)restaurants
{
    _restaurants = restaurants;
    NSDictionary *savedDict = currFilterDict;
    if(!currFilterDict)
    {
        savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    }
    if(savedDict)
    {
        NSString *cuisineFilter = savedDict[@"cuisine"];
        _filterButton.selected = (cuisineFilter != nil || [savedDict[kHasDiscountKey] boolValue]);
        
        [self filterRestaurantsWithDict:savedDict];
    }
}


- (void)geocodeAddress:(NSString*)address completion:(void (^)(BOOL found))handler
{
    __weak SearchRestaurantVC *weakSelf = self;
    [_geocoder geocodeAddressString: address completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            [weakSelf zoomMapToCoords: placemark.location.coordinate];
        }
        if (handler)
            handler(placemarks.count > 0);
    }];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [self configureNavigationBar];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
   // [self removeLoadingOverlay];
    [_searchView removeFromSuperview];
    _searchView = nil;
    _searchBar = nil;
    _searchTF = nil;
}
- (void)configureNavigationBar
{
    UIButton *searchButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [searchButton setImage: [UIImage imageNamed: @"icon_search"] forState: UIControlStateNormal];
    [searchButton addTarget: self action: @selector(search:) forControlEvents: UIControlEventTouchUpInside];
    [searchButton sizeToFit];
    _searchItem = [[UIBarButtonItem alloc] initWithCustomView: searchButton];
    
    UIButton *locationButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [locationButton setImage: [UIImage imageNamed: @"icn_you_r_here"] forState: UIControlStateNormal];
    [locationButton addTarget: self action: @selector(showMe) forControlEvents: UIControlEventTouchUpInside];
    [locationButton sizeToFit];
    _locationItem = [[UIBarButtonItem alloc] initWithCustomView: locationButton];
    
    [self.navigationItem setRightBarButtonItem: _mapListButton.selected ? _searchItem : _locationItem animated: YES];
    
    _initialSearchRect = CGRectMake(0, -64, DEVICE_WIDTH, 44);
    _searchView = [[UIView alloc] initWithFrame: _initialSearchRect];
    _searchView.backgroundColor = [UIColor clearColor];
    _searchBar = [[UISearchBar alloc] initWithFrame: _searchView.bounds];
    _searchBar.showsCancelButton = YES;
    _searchBar.backgroundImage = [UIImage alloc];
    _searchBar.backgroundColor = [Utils appPrimaryColor];
    _searchBar.tintColor = [Utils appPrimaryColor];
    _searchBar.delegate = self;
    [_searchView addSubview: _searchBar];
    
    if ([Utils isIOS7])
    {
        [searchButton sizeToFit];
    }
    else
    {
        searchButton.frame = CGRectMake(0, 0, 30, 25);
    }
    
    
    UIButton *cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
    cancelButton.backgroundColor = [Utils appPrimaryColor];
    [cancelButton setTitle: @"Cancel" forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor lightGrayColor] forState: UIControlStateHighlighted];
    
    [cancelButton addTarget: self action: @selector(cancelButtonClicked) forControlEvents: UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(250, 5, 65, 34);
    cancelButton.titleLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
    [_searchBar addSubview: cancelButton];
    _searchBar.tintColor = [Utils appPrimaryColor];
    [self.navigationController.navigationBar addSubview: _searchView];
    
    for (UIView *subView in _searchBar.subviews){
        if ([subView isKindOfClass: [UITextField class]])
            _searchTF =  (UITextField *)subView;
        else
            for (UIView *secondLevelSubView in subView.subviews)
            {
                if ([secondLevelSubView isKindOfClass:[UITextField class]])
                {
                    _searchTF = (UITextField *)secondLevelSubView;
                    break;
                }
            }
        if (_searchTF)
        {
            _searchTF.enablesReturnKeyAutomatically = NO;
            _searchTF.textColor = [Utils appPrimaryColor];
            _searchTF.background = [UIImage alloc];
            _searchTF.borderStyle = UITextBorderStyleNone;
            _searchTF.backgroundColor = RGB(228, 227, 223);
            _searchTF.layer.cornerRadius = 2.0f;
            
            if ([Utils isIOS7])
                _searchTF.tintColor = [Utils appPrimaryColor];
            else
                [[_searchTF valueForKey:@"textInputTraits"] setValue: [Utils appPrimaryColor] forKey:@"insertionPointColor"];
            break;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"restaurantMenuSegue"])
    {
        RestaurantMenuVC *menuVC = segue.destinationViewController;
        menuVC.restaurant = _selectedRestaurant;
        
        DeliveryMethod method = [DataManager deliveryMethod];
        if ((method == DeliveryMethodPickup || method == DeliveryMethodSitting)
            && _userLocationFound)
        {
            menuVC.curLocation = _location;
        }
    }
    else if ([segue.identifier isEqualToString: @"restaurantSegue"])
    {
        RestaurantVC *restaurantVC = segue.destinationViewController;
        restaurantVC.restaurant = _selectedRestaurant;
    }
}

- (void)proceedUserLocation:(CLLocation*)userLocation
{
    _location = userLocation.coordinate;
    [_tableView reloadData]; // will update distance calculation
    
    if (_userLocationFound == NO)
    {
        _userLocationFound = YES;
        if (_restaurantsFound == NO) // restaurants was not found by city/street
        {
            [self zoomMapToCoords: userLocation.coordinate];
            [self searchRestaurantsByLocation: _location];
        }
    }
    // update user address
    if ([_geocoder isGeocoding])
        return;
    if(!_address && _selectedLocation.latitude == 0 && _selectedLocation.longitude == 0)
    {
    __weak SearchRestaurantVC *weakSelf = self;
    [_geocoder reverseGeocodeLocation: userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        SearchRestaurantVC *strongSelf = weakSelf;
        if (strongSelf && placemarks.count)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString * addressString = [NSString stringWithFormat: @"%@, %@", placemark.locality, placemark.thoroughfare];
            [strongSelf->_addressButton setTitle: addressString forState: UIControlStateNormal];
            
        }
    }];
    }
}
#pragma mark - Restaurants Clusters
- (void)updateRestaurantsClustersFromRestaurants:(NSArray*)restaurants
{
    CLLocationDistance distance = 50.0f;
    _restaurantsClusters = [NSMutableArray arrayWithCapacity: restaurants.count];
    
    for (Restaurant *restaurant in restaurants)
    {
        CLLocation *restaurantLoc = [[CLLocation alloc] initWithLatitude: restaurant.latitude longitude: restaurant.longitude];
        BOOL joinedCluster = NO;
        
        for (RestaurantCluster *cluster in _restaurantsClusters)
        {
            CLLocationDistance clusterDistance = [cluster.location distanceFromLocation: restaurantLoc];
            if (clusterDistance < distance)
            {
                joinedCluster =  YES;
                [cluster.restaurants addObject: restaurant];
                break;
            }
        }
        
        if (joinedCluster == NO)
        {
            RestaurantCluster *cluster = [RestaurantCluster new];
            cluster.location = restaurantLoc;
            cluster.restaurants = [NSMutableArray arrayWithCapacity: 10];
            [cluster.restaurants addObject: restaurant];
            [_restaurantsClusters addObject: cluster];
        }
        
    }
}

#pragma mark - Actions
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    _tableView.scrollIndicatorInsets = _tableView.contentInset;
}
- (void)keyboardHidden
{
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (IBAction)changeAddress:(id)sender
{
    UINavigationController *naviVC = self.navigationController;
    UIViewController *addressesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"AddressesVC"];
    [naviVC setViewControllers:@[naviVC.viewControllers.firstObject, addressesVC] animated:YES];
}
- (IBAction)showMapList:(UIButton*)sender
{
    if (sender.selected)
    {
        _mapView.hidden = NO;
        _listView.hidden = YES;
        [self.navigationItem setRightBarButtonItem: _locationItem animated: YES];
        [_searchBar resignFirstResponder];
        [UIView animateWithDuration: 0.3f animations:^{
            _searchView.frame = _initialSearchRect;
        }];
        
    }
    else
    {
        _mapView.hidden = YES;
        _listView.hidden = NO;
        [self.navigationItem setRightBarButtonItem: _searchItem animated: YES];
    }
    sender.selected = !sender.selected;
}

- (void)search:(id)sender
{
    [UIView animateWithDuration: 0.3f animations:^{
        CGRect searchRect = _searchView.frame;
        searchRect.origin.y = 0;
        _searchView.frame = searchRect;
    } completion:^(BOOL finished) {
        [_searchTF becomeFirstResponder];
    }];
}

- (void)selectRestaurant:(Restaurant*)restaurant
{
    if(!restaurant.phoneOrdersOnlyOnPortals)
    {
    _selectedRestaurant = restaurant;
    
    NSString *segue = @"restaurantMenuSegue"; // make currently all methods open screen with menu
    /*
     DeliveryMethod method = [DataManager deliveryMethod];
     switch (method)
     {
     case DeliveryMethodPickup:
     case DeliveryMethodDelivery:
     segue = @"restaurantMenuSegue";
     break;
     case DeliveryMethodSitting:
     segue = @"restaurantSegue";
     break;
     default:
     NSLog(@"Select reustarant for undefined delivery method");
     break;
     }
     */
    if (segue)
        [self performSegueWithIdentifier: segue sender: self];
        
    }
    
}
#pragma mark - Search Delegate
- (void)cancelButtonClicked
{
    [self resetSearch];
    [_searchBar resignFirstResponder];
    [UIView animateWithDuration: 0.3f animations:^{
        _searchView.frame = _initialSearchRect;
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length == 0)
    {
        [self resetSearch];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        [self resetSearch];
    }
    else
    {
        NSMutableArray * resultArr = [NSMutableArray arrayWithCapacity: _restaurants.count];
        NSArray *allRestaurants = _restaurants;
        if(currFilterDict)
        {
            allRestaurants = [self filteredRestaurantsArrayWithDict:currFilterDict];
        }
        for (Restaurant *restaurant in allRestaurants)
        {
            if ([self restaurant: restaurant containsSearchString: searchText])
            {
                [resultArr addObject: restaurant];
            }
        }
        _filteredRestaurants = resultArr;
        [_tableView reloadData];
        [_tableView setContentOffset:CGPointZero];
        [self addMapAnnotations];
    }
}
- (BOOL)restaurant:(Restaurant*)restaurant containsSearchString:(NSString*)searchText
{
    NSMutableArray *searchInStrings = [NSMutableArray arrayWithCapacity: 4];
    if (restaurant.name)
        [searchInStrings addObject: restaurant.name];
    if (restaurant.cityName)
        [searchInStrings addObject: restaurant.cityName];
    if (restaurant.street)
        [searchInStrings addObject: restaurant.street];
    if (restaurant.cuisineList)
        [searchInStrings addObject: restaurant.cuisineList];
    
    for (NSString *candidateString in searchInStrings)
    {
        NSRange range = [candidateString rangeOfString: searchText options: (NSCaseInsensitiveSearch)];
        if (range.location != NSNotFound)
        {
            return YES;
        }
    }
    return NO;
}

- (void)resetSearch
{
    _searchTF.text = nil;
    _filteredRestaurants = nil;
    if(currFilterDict)
    {
        _filteredRestaurants = [self filteredRestaurantsArrayWithDict:currFilterDict];
    }

    [_tableView reloadData];
    [_tableView setContentOffset:CGPointZero animated:1];
    [self addMapAnnotations];
}

- (void)searchRestaurantsByLocation:(CLLocationCoordinate2D)location
{
    __weak SearchRestaurantVC *weakSelf = self;
    [DSBezelActivityView newActivityViewForView: self.view withLabel: @""];
    ;
    _isSearchingRestaurants = YES;
    [DataManager searchRestaurantsByLocation: location completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        SearchRestaurantVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            strongSelf->_isSearchingRestaurants = NO;
            strongSelf->_requestedBoundary = [Utils requestBoundariesForLocation: location];
            strongSelf->_restaurants = result;
            [strongSelf addMapAnnotations];
            [strongSelf->_tableView reloadData];
        }
    }];
}

- (void)searchRestaurantsByMapLocation:(CLLocationCoordinate2D)location
{
    __weak SearchRestaurantVC *weakSelf = self;
    [DSBezelActivityView newActivityViewForView: self.view withLabel: @""];
    ;
    _isSearchingRestaurants = YES;
    [DataManager searchRestaurantsByMap: location completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        SearchRestaurantVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            strongSelf->_isSearchingRestaurants = NO;
            strongSelf->_requestedBoundary = [Utils requestBoundariesForLocation: location];
            strongSelf->_restaurants = result;
            [strongSelf addMapAnnotations];
            [strongSelf->_tableView reloadData];
        }
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    if (restaurantsArray && restaurantsArray.count == 0)
        _noResultLabel.hidden = NO;
    else
        _noResultLabel.hidden = YES;
    
    return restaurantsArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RestaurantCell"];
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    
    cell.restaurant = restaurantsArray[indexPath.section];
    cell.showKosherDetails = self.showKosherDetails;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    Restaurant *restaurant = restaurantsArray[indexPath.section];
    return  (!restaurant.phoneOrdersOnlyOnPortals);


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    [self selectRestaurant: restaurantsArray[indexPath.section]];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2.0f;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame: CGRectZero];
}

- (IBAction)showFilterScreen:(id)sender {
    [_searchBar resignFirstResponder];
    if(currFilterDict)
    {
        [filterView setupWithDict:currFilterDict];
    }
    filterView.isSortingEnabled = _mapView.hidden;
    __weak SearchRestaurantVC *weakSelf = self;
    [filterView showWithCompletion:^(NSDictionary *result) {
        if(result || currFilterDict)
        {
            NSDictionary *dict = result ? result : currFilterDict;
            NSString *cuisineFilter = dict[@"cuisine"];
            
            _filterButton.selected = (cuisineFilter != nil || [dict[kHasDiscountKey] boolValue]);
        }
        else
        {
            _filterButton.selected = NO;
        }
        [weakSelf filterRestaurantsWithDict:result];
    }];
}

- (void)filterRestaurantsWithDict:(NSDictionary *)result
{
    if(result)
    {
        currFilterDict = result;
    }
    NSMutableArray *filtered = [self filteredRestaurantsArrayWithDict:currFilterDict];
    _filteredRestaurants = filtered;

    
  
    
    [_tableView reloadData];
    [_tableView setContentOffset:CGPointZero];
    
    [self addMapAnnotations];
    [self tapOnMap:nil];
}

- (NSMutableArray *)filteredRestaurantsArrayWithDict:(NSDictionary *)result
{
    NSMutableArray *resultArr = nil;
    if(result == nil)
    {
        NSDictionary *savedDict = nil;//[[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
        if(savedDict)
        {
            NSString *cuisineFilter = savedDict[@"cuisine"];
            _filterButton.selected = (cuisineFilter != nil || [savedDict[kHasDiscountKey] boolValue] == YES);
            
            [filterView setupWithDict:savedDict];
            [self filterRestaurantsWithDict:savedDict];
        }
        else
        {
            _filteredRestaurants = nil;
            [_tableView reloadData];
            [_tableView setContentOffset:CGPointZero];
            
            [_mapView removeAnnotation: _showedAnnotation];
            _showedAnnotation = nil;
            _showedAnnotationView = nil;
            [self addMapAnnotations];
            
        }
        resultArr = [_restaurants mutableCopy];
    }
    
    resultArr = [NSMutableArray arrayWithCapacity: _restaurants.count];
    if (result[@"cuisine"])
    {
        NSString *cuisineName = [result[@"cuisine"] objectForKey:@"CuisineTypeName"];
        NSArray *filtered = [_restaurants filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(cuisineList contains[cd] %@)", cuisineName]];
        resultArr = [NSMutableArray arrayWithArray:filtered];
    }
    else
    {
        [resultArr addObjectsFromArray:_restaurants];
    }
    NSString *key = @"sortByPickup";
    if([DataManager deliveryMethod] == DeliveryMethodDelivery)
    {
        key = @"sortByDelivery";
    }
    if(result[key])
    {
        BOOL ascending = NO;
        NSString *sortValue = [result[key] objectForKey:@"field"];
        if([sortValue isEqualToString:@"minimumPriceForOrder"] || [sortValue isEqualToString:@"deliveryPriceForOrder"] || [sortValue isEqualToString:@"name"] || [sortValue isEqualToString:@"distanceFromUserMeters"])
        {
            ascending = YES;
        }
        
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:sortValue  ascending:ascending];
        NSArray *sorted = [resultArr sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        resultArr = [NSMutableArray arrayWithArray:sorted];
    }
    
    if([result[kHasDiscountKey] boolValue])
    {
        NSArray *filtered = [resultArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(discountCouponPercent>0)"]];
        resultArr = [NSMutableArray arrayWithArray:filtered];
    }
    return resultArr;
}


#pragma mark - Map View
- (void)showMe
{
    CLLocation *userLocation = [[DataManager sharedManager] currentLocation];
    if (CLLocationCoordinate2DIsValid(_location))
        [self zoomMapToCoords: _location];
    else if (userLocation)
        [self zoomMapToCoords: userLocation.coordinate];
    
}
- (void)tapOnMap:(id)sender
{
    if (_showedAnnotation)
    {
        [_mapView removeAnnotation: _showedAnnotation];
        _showedAnnotation = nil;
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView: _mapView];
    UIView *view = [_mapView hitTest: point withEvent: nil];
    if ([view isKindOfClass: [MKAnnotationView class]])
        return NO;
    UIView *superView = view.superview;
    while (superView)
    {
        if ([superView isKindOfClass: [MKAnnotationView class]])
            return NO;
        superView = superView.superview;
        if (superView == _mapView)
            break;
    }
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)zoomMapToCoords:(CLLocationCoordinate2D)coords
{
    double miles = 0.43f;
    double scalingFactor = ABS(cos (2 * M_PI * coords.latitude /360.0));
    MKCoordinateSpan span;
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor*69.0);
    MKCoordinateRegion region;
    region.span = span;
    region.center = coords;
    [_mapView setRegion: region animated: YES];
}
- (void)addMapAnnotations
{
    [_mapView removeAnnotations: _restaurantsAnnotations];
    _mapView.showsUserLocation = YES;
    NSArray *restaurants = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    // group restaurants in clusters
    [self updateRestaurantsClustersFromRestaurants: restaurants];
    
    NSMutableArray* annotaions = [NSMutableArray array];
    for (RestaurantCluster *cluster in _restaurantsClusters)
    {
        RestaurantAnnotation *annotation = nil;
        if (cluster.restaurants.count == 1)
        {
            Restaurant *restaurant = [cluster.restaurants lastObject];
            annotation = [[RestaurantAnnotation alloc] initWithRestaurant: restaurant];
        }
        else
        {
            annotation = [[RestaurantAnnotation alloc] initWithCluster: cluster];
        }
        [_restaurantsAnnotations addObject: annotation];
        [annotaions addObject: annotation];
    }
    
    [_mapView addAnnotations: annotaions];
}


- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(RestaurantAnnotation *)annotation
{
    if ([annotation isKindOfClass: [MKUserLocation class]])
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"userLoc"];
        annotationView.canShowCallout = NO;
        
        NSArray *sublayers = annotationView.layer.sublayers;
        NSInteger layerIndex = [Utils isIOS7] ? 1 : 3;
        if (sublayers.count > layerIndex)
        {
            CALayer *userLayer = sublayers[layerIndex];
            userLayer.hidden = YES;
        }
        
        UILabel *hereLabel = [[UILabel alloc] initWithFrame: CGRectMake(-3, -45, 30, 50)];
        hereLabel.numberOfLines = 3;
        hereLabel.text = @"You are here";
        hereLabel.backgroundColor = [UIColor clearColor];
        hereLabel.textColor = [Utils appPrimaryColor];
        hereLabel.font = [UIFont fontWithName: @"ArialMT" size: 12];
        hereLabel.textAlignment = NSTextAlignmentCenter;
        [annotationView addSubview: hereLabel];
        
        annotationView.image = [UIImage imageNamed: @"icn_you_r_here_blue"];
        return annotationView;
    }
    
    MKAnnotationView *annotationView = nil;
    
    NSInteger numberOfRestaurans = annotation.cluster.restaurants.count;
    
    if (annotation.isShowed == NO)
    {
        // Pin Annotation
        annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier: @"RestaurantPinAnnotation"];
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"RestaurantPinAnnotation"];
            NSArray *xib = [[NSBundle mainBundle] loadNibNamed: @"RestaurantPinAnnotation" owner: self options: nil];
            UIView *view = xib[0];
            [annotationView addSubview: view];
            annotationView.frame = view.bounds;
        }
        annotationView.annotation = annotation;
        
        UIView *logoView = [annotationView viewWithTag: 1];
        UILabel *pinLabel = (UILabel*)[annotationView viewWithTag: 2];
        UIImageView *imgView = (UIImageView *)[annotationView viewWithTag: 3];
        imgView.image = [UIImage imageNamed:@"map_tag"];
        if([[DataManager appDomain] isEqualToString:@"kosherordering"])
        {
            logoView.hidden = YES;
            pinLabel.textColor = [Utils appSecondaryColor];
            if(numberOfRestaurans == 0)
            {
                imgView.image = [UIImage imageNamed:@"map_tag_with_logo"];
            }
        }
        else
        {
            logoView.hidden = numberOfRestaurans != 0;
        }
        pinLabel.hidden = numberOfRestaurans == 0;
        pinLabel.text = [NSString stringWithFormat: @"%ld", (long)numberOfRestaurans];
    }
    else
    {
        
        annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier: @"RestaurantAnnotation"];
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"RestaurantAnnotation"];
            UIView *view = [RestaurantAnnotationView annotationView];
            view.tag = 1;
            [annotationView addSubview: view];
            annotationView.frame = view.bounds;
            annotationView.centerOffset = CGPointMake(-88, -84);
            
            
        }
        _showedAnnotationView = annotationView;
        RestaurantAnnotationView *view = (RestaurantAnnotationView*)[annotationView viewWithTag: 1];
        annotationView.annotation = annotation;
        view.showKosherDetails = self.showKosherDetails;
        view.annotation = annotation;
        
    }
    return annotationView;
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapView deselectAnnotation: view.annotation animated: NO];
    
    
    if ([view.reuseIdentifier isEqualToString: @"RestaurantPinAnnotation"])
    {
        RestaurantAnnotation *pinAnnotation = (RestaurantAnnotation*)view.annotation;
        
        if (_showedAnnotation) // reset old opened restaurant
        {
            CGRect intersect = CGRectIntersection(view.frame, _showedAnnotationView.frame);
            if (CGRectIsNull(intersect) == NO)
            {
                return;
            }
            Restaurant *showedRestaurant = _showedAnnotation.restaurant;
            [mapView removeAnnotation: _showedAnnotation];
            _showedAnnotation = nil;
            _showedAnnotationView = nil;
            if (pinAnnotation.restaurant == showedRestaurant)
            {
                return;
            }
        }
        RestaurantAnnotation *annotation = [pinAnnotation copy];
        annotation.isShowed = YES;
        [mapView addAnnotation: annotation];
        _showedAnnotation = annotation;
        MKMapPoint point = MKMapPointForCoordinate(annotation.coordinate);
        CGFloat offset = [Utils isIOS7] ? 350.0f : 200.0f;
        MKMapRect rect = MKMapRectMake(point.x - offset, point.y, 0, 0);
        [mapView setVisibleMapRect: rect animated: YES];
    }
    else
    {
        RestaurantAnnotation *annotation = (RestaurantAnnotation*)view.annotation;
        if ([annotation isKindOfClass: [RestaurantAnnotation class]])
        {
            if (annotation.restaurant)
                [self selectRestaurant: annotation.restaurant];
            else
            {
                NSInteger index = annotation.clusterSelectedIndex;
                [self selectRestaurant: annotation.cluster.restaurants[index]];
            }
            
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodPickup || method == DeliveryMethodSitting)
    {
        [self proceedUserLocation: userLocation.location];
    }
    else
    {
        _userLocationFound = YES;
        _location = userLocation.coordinate;
    }
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (_requestedBoundary && _isSearchingRestaurants == NO)
    {
        MKCoordinateRegion region =  mapView.region;
        CLLocationDegrees visibleNorth = region.center.latitude + region.span.latitudeDelta/2.0f;
        CLLocationDegrees visibleSouth = region.center.latitude - region.span.latitudeDelta/2.0f;
        CLLocationDegrees visibleEast = region.center.longitude + region.span.longitudeDelta/2.0f;
        CLLocationDegrees visibleWest = region.center.longitude - region.span.longitudeDelta/2.0f;
        
        CLLocationDegrees requestedNorth = [_requestedBoundary[0] doubleValue];
        CLLocationDegrees requestedSouth = [_requestedBoundary[1] doubleValue];
        CLLocationDegrees requestedEast = [_requestedBoundary[2] doubleValue];
        CLLocationDegrees requestedWest = [_requestedBoundary[3] doubleValue];
        
        if ((visibleNorth - visibleSouth) > (requestedNorth - requestedSouth) ||
            (visibleEast - visibleWest) > (requestedEast - requestedWest))
        {
            NSLog(@"return");
            return;
        }
        
        if (visibleNorth > requestedNorth ||
            visibleSouth < requestedSouth ||
            visibleEast > requestedEast ||
            visibleWest < requestedWest)
        {
            NSLog(@"search");
            [self searchRestaurantsByMapLocation: region.center];
        }
    }
}
@end
