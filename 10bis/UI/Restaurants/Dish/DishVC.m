//
//  DishVC.m
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "DishVC.h"
#import "Restaurant.h"
#import "Dish.h"
#import "MenuHeader.h"
#import "DataManager.h"
#import "McUser.h"

#import "Choice.h"
#import "SubChoice.h"
#import "ChoiceCell.h"
#import "Order.h"
#import "User.h"
#import "DishConfirmation.h"

static NSTimeInterval const kAnimationTime = 0.3f;

static NSInteger const kHeaderTextFontSize = 20;

@interface DishVC () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end

@implementation DishVC
{
    UIBarButtonItem *_cartItem;
    NSString *_cartSum;
    NSMutableAttributedString *_headerText;

    __weak IBOutlet NSLayoutConstraint *heightConstraint;
    __weak IBOutlet UILabel *_priceLabel;
    __weak IBOutlet UILabel *_dishDescLabel;
    __weak IBOutlet UIButton *_userButton;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet AppRedButton *_addButton;
    __weak IBOutlet UIButton *_plusButton;
    __weak IBOutlet UIButton *_minusButton;
    
    __weak IBOutlet UILabel *_quantityLabel;
    NSMutableArray *_quantityes;
    NSArray *_users;
  //  McUser *_selectedUser;
    
    NSMutableSet *_openedChoices;
    
    NSString *_dishNotes;
    NSMutableArray *_selectedCells;
    NSMutableDictionary *_selectedSubchoices;
    UITapGestureRecognizer *_doneTap;
    __weak UITextField *_notesTF;
    NSMutableDictionary *_headerViews;
    
    __weak IBOutlet UIView *_addDishView;
    __weak IBOutlet UIView *_editDishView;
    
    BOOL _transitingToNextSection;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];
    _priceLabel.textColor = [Utils appTextColor];
    _quantityLabel.textColor = [Utils appTextColor];

    // screen title
    UILabel *titleLabel = [[UILabel alloc]initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 30)];
    NSDictionary *attrs = [[UINavigationBar appearance] titleTextAttributes];
    titleLabel.font = attrs[NSFontAttributeName];
    titleLabel.textColor = attrs[NSForegroundColorAttributeName];
    titleLabel.backgroundColor = [UIColor clearColor];

    NSString *screenTitle = nil;

    if (_restaurant.name)
        screenTitle = [NSString stringWithFormat: @"%@", _restaurant.name];
    
    titleLabel.text = screenTitle;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *titleWrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 40)];
    [titleWrapView addSubview: titleLabel];
    titleWrapView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleWrapView;

    UIImage *bgImage = [_userButton backgroundImageForState: UIControlStateNormal];
    UIImage *strBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight:bgImage.size.height/2];
    [_userButton setBackgroundImage: strBgImage forState: UIControlStateNormal];
    
    
    _doneTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(done)];
    [self.view addGestureRecognizer: _doneTap];
    _doneTap.cancelsTouchesInView = NO;
    
    _userButton.titleLabel.adjustsFontSizeToFitWidth = YES;

    
    [_tableView registerNib: [UINib nibWithNibName: @"ChoiceCell" bundle: nil] forCellReuseIdentifier: @"ChoiceCell"];
    [_tableView registerNib: [UINib nibWithNibName: @"MenuHeader" bundle: nil] forHeaderFooterViewReuseIdentifier: @"MenuHeader"];

    
    // update UI
    _quantityLabel.backgroundColor = [Utils appPrimaryColor];
    _quantityLabel.textColor = [Utils appTextColor];
    _openedChoices = [NSMutableSet setWithCapacity: _dish.choices.count];
    NSMutableAttributedString *restStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", _dish.dishName, _dish.dishDesc] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15],  NSWritingDirectionAttributeName :@[@(NSWritingDirectionLeftToRight | NSTextWritingDirectionOverride)]}];
    [Utils addBoldAttributeToString:restStr forText:_dish.dishName font:15];
    _headerText = restStr;
    _dishDescLabel.attributedText = restStr;
    [_dishDescLabel sizeToFit];
    _dishDescLabel.height = 54;
    NSInteger minQuantity = _dish.minimalQuantity;
    _quantityLabel.text = [NSString stringWithFormat: @"%ld", (long)minQuantity];
    [self updatePriceLabel];
    
    NSInteger maxQuantity = 15;
    if(minQuantity > maxQuantity) maxQuantity = minQuantity;
    _quantityes = [NSMutableArray arrayWithCapacity: maxQuantity - minQuantity];
    for (NSInteger i = minQuantity; i <= maxQuantity; i++)
    {
        [_quantityes addObject: [NSString stringWithFormat: @"%ld", (long)i]];
    }
    
    
    if (_confirmation)
        [self updateScreenWithConfirmation];
}

- (void)setDish:(Dish *)dish
{
    _dish = [dish copy];
    
    _selectedSubchoices = [NSMutableDictionary dictionaryWithCapacity: _dish.choices.count];
    _selectedCells = [NSMutableArray arrayWithCapacity: 20];
    NSMutableArray *choices = [NSMutableArray arrayWithArray: _dish.choices];
    
    // make default selections
    if (_confirmation == nil)
    {
        for (Choice *choice in choices)
        {
            if (choice.type == ChoiceTypeBeverages ||
                choice.type == ChoiceTypeComboBox)
            {
                NSInteger section = [choices indexOfObject: choice];
                NSIndexPath *ip = [NSIndexPath indexPathForRow:0  inSection: section];
                [_selectedCells addObject: ip];
                
                if (choice.subsChosen.count)
                {
                    SubChoice *subchoice = choice.subsChosen[0];
                    [_selectedSubchoices setObject: @[subchoice.name] forKey: @(section)];
                }
                
            }
        }
    }
    
    // add dish notes
    Choice *notesChoice = [Choice new];
    notesChoice.type = ChoiceTypeNotes;
    notesChoice.textBeforeChoosing = @"Comments:";
    SubChoice *notesSubchoice = [SubChoice new];
    notesChoice.subsChosen = @[notesSubchoice];
    [choices addObject: notesChoice];
    _dish.choices = choices;

}
- (void)setConfirmation:(DishConfirmation *)confirmation
{
    _confirmation = confirmation;
    if (self.isViewLoaded)
    {
        [self updateScreenWithConfirmation];
    }
}
- (void)updateScreenWithConfirmation
{
    // buttons on bottom
    _addDishView.hidden = YES;
    _editDishView.hidden = NO;
    
    _dishNotes = _confirmation.dishNotes;
    NSInteger section = _dish.choices.count-1;
    if (_dishNotes)
        [_selectedSubchoices setObject:@[_dishNotes] forKey: @(section)];


    // choices
    for (Choice *selectedChoice in _confirmation.choices)
    {
        for (Choice *choice in _dish.choices)
        {
            NSInteger section = [_dish.choices indexOfObject: choice];
            
            if ([selectedChoice.ID isEqualToNumber: choice.ID])
            {
                
                // subchoices
                for (SubChoice *selectedSubChoice in selectedChoice.subsChosen)
                {
                    for (SubChoice *subChoice in choice.subsChosen)
                    {
                        if ([selectedSubChoice.ID isEqualToNumber: subChoice.ID])
                        {
                            [_selectedSubchoices setObject: @[subChoice.name] forKey: @(section)];
                            NSInteger row = [choice.subsChosen indexOfObject: subChoice];
                            NSIndexPath *ip = [NSIndexPath indexPathForRow: row  inSection: section];
                            [_selectedCells addObject: ip];
                            break;
                        }
                    }
                }
                
                break;
            }
        }

    }
    
    // quantity
    NSString *quantity = [NSString stringWithFormat: @"%ld", (long)_confirmation.quantity];
    _quantityLabel.text = quantity;
    [self updatePriceLabel];
    [_tableView reloadData];
}
#pragma mark - Actions
- (void)cartClicked:(id)sender
{
    // TODO: use _cartSum
//    [self performSegueWithIdentifier: @"cartSegue" sender: self];
}



- (void)done
{
    _tableView.userInteractionEnabled = YES;
    _userButton.selected = NO;
    [_notesTF resignFirstResponder];
}
- (Order*)createOrder
{
    Order *order = [Order new];
    order.ID = _dish.dishId;
    order.dishOwnerId = _dish.categoryId;
    order.quantity = [_quantityLabel.text integerValue];
   // order.assignedUserId = _selectedUser.userId;
    order.dishNotes = _dishNotes ? _dishNotes : @"";
    order.choices = [self selectedChoicesWithSubchoices];
    order.orderIndex = @(_dish.orderIndex);
    return order;
}
- (IBAction)addItem:(id)sender
{
    Order *order = [self createOrder];
    [self showLoadingOverlay];
    
    __weak DishVC *weakSelf = self;
    [DataManager addDishToCart: order restaurant: _restaurant.restaurantId completion:^(BOOL result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        DishVC *strongSelf = weakSelf;
        if (error)
            [Utils showErrorMessage: error];
        if (result && strongSelf)
        {
            //[strongSelf.navigationController popViewControllerAnimated: NO];
            strongSelf->_didAddDish();
        }

    }];
}
- (IBAction)updateItem:(id)sender
{
    Order *order = [self createOrder];
    [self showLoadingOverlay];
    
    __weak DishVC *weakSelf = self;
    [DataManager updateDish: order restaurant: _restaurant.restaurantId completion:^(BOOL result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        DishVC *strongSelf = weakSelf;
        if (error)
            [Utils showErrorMessage: error];
        if (result && strongSelf)
        {
            [strongSelf.navigationController popViewControllerAnimated: YES];
            strongSelf->_didUpdateDish();
        }
    }];
}
- (IBAction)dishTextButtonPressed:(UIButton *)sender
{
    _dishDescLabel.contentMode = UIViewContentModeTop;
    CGSize descrSize = [_headerText boundingRectWithSize: CGSizeMake(_dishDescLabel.width, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    if(descrSize.height < 54) return;
    CGRect frame = _topContentView.frame;
    CGRect tableFrame = _tableView.frame;
    if(descrSize.height > 32 && sender.selected == NO)
    {
        frame.size.height += descrSize.height - 45;
        sender.selected = YES;
        NSLog(@"show full");
    }
    else if(sender.selected == YES)
    {
        _dishDescLabel.numberOfLines = 0;
        frame.size.height = 98;
        sender.selected = NO;
    }
    
    heightConstraint.constant = frame.size.height;
    [_topContentView setNeedsUpdateConstraints];
    [_tableView setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:0.35f animations:^{
        [_topContentView layoutIfNeeded];
        [_tableView layoutIfNeeded];
    }];

}
- (IBAction)removeItem:(id)sender
{
    __weak DishVC *weakSelf = self;
    [self showLoadingOverlay];

    [DataManager removeDishFromCart: _restaurant.restaurantId index: _confirmation.orderIndex completion:^(BOOL result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        
        DishVC *strongSelf = weakSelf;
        if (error)
            [Utils showErrorMessage: error];
        if (result && strongSelf)
        {
            [strongSelf.navigationController popViewControllerAnimated: YES];
            strongSelf->_didUpdateDish();
        }

    }];
    
}
- (NSArray*)selectedChoicesWithSubchoices
{
    NSMutableArray *selectedChoices = [NSMutableArray arrayWithCapacity: _selectedCells.count];
    for (NSIndexPath *ip in _selectedCells)
    {
        Choice *selectedChoice = _dish.choices[ip.section];
        if ([selectedChoices containsObject: selectedChoice.ID] == NO)
        {
            [selectedChoices addObject: selectedChoice.ID];
        }
    }
    NSMutableArray *result = [NSMutableArray arrayWithCapacity: selectedChoices.count];
    for (NSNumber *selectedChoiceID in selectedChoices)
    {
        Choice *choice = [Choice new];
        choice.ID = selectedChoiceID;
        NSMutableArray *subsChosen = [NSMutableArray arrayWithCapacity: choice.subsChosen.count];
        for (NSIndexPath *ip in _selectedCells)
        {
            Choice *selectedChoice = _dish.choices[ip.section];
            SubChoice *selectedSubchoice = selectedChoice.subsChosen[ip.row];
            if ([selectedChoice.ID isEqualToNumber: selectedChoiceID])
            {
                SubChoice *subChoice = [SubChoice new];
                subChoice.ID = selectedSubchoice.ID;
                subChoice.price = selectedSubchoice.price;
                subChoice.name = selectedSubchoice.name;
                [subsChosen addObject: subChoice];
            }
        }
        choice.subsChosen = subsChosen;
        [result addObject: choice];
    }
    return result;
}
- (void)updatePriceLabel
{
    float price = [_dish.dishPrice floatValue];
    for (NSIndexPath *ip in _selectedCells)
    {
        Choice *choice = _dish.choices[ip.section];
        SubChoice *subChoice = choice.subsChosen[ip.row];
        price += [subChoice.price integerValue];
    }
    CGFloat quantity = [_quantityLabel.text floatValue];
    float priceF = floorf((CGFloat)(price * quantity) * 100 + 0.5) / 100;

    _priceLabel.text = [NSString stringWithFormat: @"$%.2f", priceF];
}

#pragma mark - Notes Text Field
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _notesTF = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_dishNotes)
    {
        NSInteger section = _dish.choices.count-1;
        [_selectedSubchoices setObject:@[_dishNotes] forKey: @(section)];
        [self updateHeaderViewForSection: section header: nil];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange: range withString: string];
    _dishNotes = result;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Table View
- (NSArray*)subChoicesForSection:(NSInteger)section
{
    Choice *choice = _dish.choices[section];
    return choice.subsChosen;   }
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    _headerViews = [NSMutableDictionary dictionaryWithCapacity: _dish.choices.count];
    return _dish.choices.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BOOL isSectionOpened = [_openedChoices containsObject: @(section)];
    NSInteger subsCount = [self subChoicesForSection: section].count;
    
    return isSectionOpened ? subsCount : 0;
}


- (void)updateHeaderViewForSection:(NSInteger)section header:(MenuHeader*)header
{
    BOOL updateTable = NO;
    if (header == nil)
    {
        header = (MenuHeader*)[_tableView headerViewForSection: section];
        updateTable = YES;
        if ([_openedChoices anyObject])
        {
            header = (MenuHeader*)[_tableView headerViewForSection: section];
        }

    }
    
    Choice *choice = _dish.choices[section];
    NSAttributedString *headerText = [self headerTextForChoice: choice inSection: section];
    header.headerLabel.attributedText = headerText;
    header.headerHeight = [MenuHeader heightForText: headerText];
    if (updateTable)
    {
        [_tableView beginUpdates];
        [_tableView endUpdates];
    }
}
- (NSAttributedString*)headerTextForChoice:(Choice*)choice inSection:(NSInteger)section
{
    NSString *headerText = choice.textBeforeChoosing;
    NSArray *subchoices = [_selectedSubchoices objectForKey: @(section)];
    NSString *subchoicesText = nil;
    if (subchoices)
    {
        subchoicesText = [subchoices componentsJoinedByString: @", "];
        headerText = [headerText stringByAppendingFormat: @"\n%@", subchoicesText];
    }
    NSMutableAttributedString *attrHeaderText = [[NSMutableAttributedString alloc] initWithString: headerText];
    NSRange headerRange = [headerText rangeOfString: choice.textBeforeChoosing];
    [attrHeaderText addAttribute: NSFontAttributeName value: [UIFont fontWithName: @"ArialMT" size: kHeaderTextFontSize] range: headerRange];
    if (subchoicesText)
    {
        NSRange subsRange = [headerText rangeOfString: subchoicesText options:NSBackwardsSearch];
        [attrHeaderText addAttribute: NSFontAttributeName value: [UIFont fontWithName: @"Arial" size: 15] range: subsRange];
    }
    return attrHeaderText;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    MenuHeader *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier: @"MenuHeader"];
    header.backgroundView = [[UIView alloc] initWithFrame: CGRectZero];
    header.backgroundView.backgroundColor = [UIColor clearColor];
    header.imgView.backgroundColor = [Utils appSecondaryColor];
//    if (_openedChoices.count)
//        section = [[_openedChoices anyObject] integerValue];
    header.isOpen = [_openedChoices containsObject: @(section)];
    header.section = section;
    [_headerViews setObject: header forKey: @(section)];
    [self updateHeaderViewForSection: section header: header];
    
    header.didClickOnHeader = ^(MenuHeader *clickedHeader){
//        if (_openedChoices.count)
//        {
//            NSInteger openedSection = [[_openedChoices anyObject] integerValue];
//            if (openedSection !=clickedHeader.section)
//                return;
//        }
//        else
            if (_transitingToNextSection)
            return;
        [self collapseSection: clickedHeader.section];
    };
    return header;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    if (_openedChoices.count)
//    {
//        section = [[_openedChoices anyObject] integerValue];
//    }
    Choice *choice = _dish.choices[section];
    NSAttributedString *text = [self headerTextForChoice: choice inSection: section];
    CGFloat headerHeight = [MenuHeader heightForText: text];
    return headerHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
//    if ([_openedChoices count])
//    {
//        section = [[_openedChoices anyObject] integerValue];
//    }
    NSArray *subs = [self subChoicesForSection: section];
    SubChoice *subChoice = subs[indexPath.row];
    return [ChoiceCell heightForSubChoice: subChoice];
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger openedSection = [[_openedChoices anyObject] integerValue];
    NSIndexPath *ip = [NSIndexPath indexPathForRow: indexPath.row inSection: openedSection];

    ChoiceCell *cell = (ChoiceCell*)[tableView dequeueReusableCellWithIdentifier: @"ChoiceCell"];
    cell.textField.delegate = self;
    NSArray *subs = [self subChoicesForSection: openedSection];

    cell.choice = _dish.choices[openedSection];
    cell.subChoice = subs[indexPath.row];
    
    NSInteger cellsCount = [self subChoicesForSection: openedSection].count;
    cell.bgImage.hidden = indexPath.row != cellsCount-1;
    
    NSArray *selectedSubs = [_selectedSubchoices objectForKey: @(openedSection)];
    cell.iconImageView.alpha = selectedSubs.count == cell.choice.maxPicksAllowed ? 0.5f : 1.0f;

    if (cell.choice.type == ChoiceTypeRadioButtons)
        cell.iconImageView.alpha = 1.0f;
    else if ([_selectedCells containsObject: ip])
    {
        cell.iconImageView.alpha = 1.0f;
        [tableView selectRowAtIndexPath: indexPath animated: NO scrollPosition: UITableViewScrollPositionNone];
    }
    return cell;

}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger openedSection = [[_openedChoices anyObject] integerValue];
    indexPath = [NSIndexPath indexPathForRow: indexPath.row inSection: openedSection];
    Choice *choice = _dish.choices[indexPath.section];
    if (choice.type == ChoiceTypeNotes)
    {
        ChoiceCell *choiceCell = (ChoiceCell*)cell;
        choiceCell.textField.text = _dishNotes;
        double delayInSeconds = 0.2f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [choiceCell.textField becomeFirstResponder];
        });
    }
}
#pragma Selecting
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger openedSection = [[_openedChoices anyObject] integerValue];
    NSIndexPath *ip = [NSIndexPath indexPathForRow: indexPath.row inSection: openedSection];

    Choice *choice = _dish.choices[openedSection];
    SubChoice *subChoice = choice.subsChosen[ip.row];

    BOOL updateTable = NO;
    if (choice.type == ChoiceTypeNotes)
    {
        ip = [NSIndexPath indexPathForRow:0 inSection:0];
        ChoiceCell *cell = (ChoiceCell*)[tableView cellForRowAtIndexPath: ip];
        [tableView deselectRowAtIndexPath: ip animated: NO];
        [cell.textField becomeFirstResponder];
        return;
    }
    else if (choice.maxPicksAllowed == 1 || choice.type == ChoiceTypeComboBox)
    {
        // deselect cells
        NSMutableArray *ipsToRemove = [NSMutableArray arrayWithCapacity: _selectedCells.count];
        for (NSIndexPath *selectedIp in _selectedCells)
        {
            Choice *selectedChoice = _dish.choices[selectedIp.section];
            if (choice == selectedChoice)
            {
                NSIndexPath *deselectIndexPath = [NSIndexPath indexPathForRow: selectedIp.row inSection:0];
                [_tableView deselectRowAtIndexPath: deselectIndexPath animated: NO];
                [ipsToRemove addObject: selectedIp];
            }
        }
        [_selectedCells removeObjectsInArray: ipsToRemove];
        
        // subchoices
        [_selectedSubchoices removeObjectForKey: @(openedSection)];
        [_selectedSubchoices setObject: [NSMutableArray arrayWithObject: subChoice.name] forKey: @(openedSection)];
        if (choice.type == ChoiceTypeCheckBox)
            updateTable = YES;
    }
    else
    {
        NSMutableArray *subs = [NSMutableArray arrayWithArray: [_selectedSubchoices objectForKey: @(openedSection)]];
        if (subs == nil)
            subs = [NSMutableArray arrayWithCapacity: choice.subsChosen.count];

        if (subs.count + 1 == choice.maxPicksAllowed)
            updateTable = YES;
        else if (subs.count == choice.maxPicksAllowed)
        {
            [tableView deselectRowAtIndexPath: indexPath animated: NO];
            return;
        }

        [subs addObject: subChoice.name];
        [_selectedSubchoices setObject: subs forKey: @(openedSection)];
        

    }
    [_selectedCells addObject: ip];
    [self updatePriceLabel];

    if (updateTable)
        [tableView reloadData];
    else
        [self updateHeaderViewForSection: indexPath.section header: nil];

    dispatch_async(dispatch_get_main_queue(), ^{
        if (choice.maxPicksAllowed == 1 || choice.type == ChoiceTypeComboBox)
        {
            [self collapseSection: openedSection];
            MenuHeader *header = (MenuHeader*)[_tableView headerViewForSection: openedSection];
            header.isOpen = NO;
        }
    });
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger openedSection = [[_openedChoices anyObject] integerValue];
    NSIndexPath *ip = [NSIndexPath indexPathForRow: indexPath.row inSection: openedSection];

    Choice *choice = _dish.choices[openedSection];
    SubChoice *subChoice = choice.subsChosen[indexPath.row];
    
    BOOL updateTable = NO;
    if (choice.type == ChoiceTypeBeverages ||
        choice.type == ChoiceTypeComboBox)
    {
        [tableView selectRowAtIndexPath: indexPath animated: NO scrollPosition: UITableViewScrollPositionNone];
        return;
    }
    [_selectedCells removeObject: ip];
    NSMutableArray *subs = [NSMutableArray arrayWithArray: [_selectedSubchoices objectForKey: @(openedSection)]];
    if (choice.type == ChoiceTypeRadioButtons)
        [_selectedSubchoices removeObjectForKey: @(openedSection)];
    else
    {
        updateTable = subs.count == choice.maxPicksAllowed;
        [subs removeObject: subChoice.name];
        [_selectedSubchoices setObject:subs forKey:@(openedSection)];
    }
    
    [self updatePriceLabel];
    if (updateTable)
        [tableView reloadData];
    else
        [self updateHeaderViewForSection: indexPath.section header: nil];
}
#pragma mark - Collapsing
- (void)collapseSection:(NSInteger)section
{
    NSArray *dishes = [self subChoicesForSection: section];
    NSMutableArray *cellsToAnimate = [NSMutableArray arrayWithCapacity: dishes.count];
    NSMutableArray *cellsToSelect = [NSMutableArray arrayWithCapacity: _selectedCells.count];
    
    
    
    for (int i = 0; i < dishes.count; i++)
    {
        NSIndexPath *ip = [NSIndexPath indexPathForRow: i inSection: section];
        [cellsToAnimate addObject: ip];
        
        for (NSIndexPath *selectedIp in _selectedCells)
        {
            if (selectedIp.section == section && selectedIp.row == ip.row)
            {
                [cellsToSelect addObject: ip];
            }
        }
    }
    UITableViewRowAnimation animation = UITableViewRowAnimationTop;
    [_tableView beginUpdates];
    if ([_openedChoices containsObject: @(section)])
    {
        if ([_notesTF isFirstResponder])
            [_notesTF resignFirstResponder];
        
        [_openedChoices removeObject: @(section)];
        [_tableView deleteRowsAtIndexPaths:cellsToAnimate withRowAnimation: animation];
        //[_tableView insertSections: sectionsOnTop withRowAnimation: UITableViewRowAnimationTop];
        //[_tableView insertSections: sectionsOnBottom withRowAnimation: UITableViewRowAnimationBottom];
    }
    else
    {
        NSMutableArray *sectionsOnTop = [NSMutableArray array];
        NSMutableArray *sectionsOnBottom = [NSMutableArray array];
        for (NSNumber *num in _openedChoices) {
            NSArray *dishes = [self subChoicesForSection: num.integerValue];
            for(int i=0; i<dishes.count;i++)
            {
                if(i < section)
                {
                    [sectionsOnTop addObject:[NSIndexPath indexPathForRow:i inSection:num.integerValue]];
                }
                else
                {
                    [sectionsOnBottom addObject:[NSIndexPath indexPathForRow:i inSection:num.integerValue]];
                }
            }
            
        }
        [_openedChoices removeAllObjects];
        [_openedChoices addObject: @(section)];
        [_tableView deleteRowsAtIndexPaths:sectionsOnTop withRowAnimation:UITableViewRowAnimationTop];
        [_tableView deleteRowsAtIndexPaths: sectionsOnBottom withRowAnimation: UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths: cellsToAnimate withRowAnimation: animation];
        // footer
        
    }
    
    [_tableView endUpdates];
    
    if ([_openedChoices containsObject: @(section)])
    {
        for (NSIndexPath *selectedIp in cellsToSelect)
        {
            [_tableView selectRowAtIndexPath: selectedIp animated: NO scrollPosition: UITableViewScrollPositionNone];
        }
    }
    
}

- (IBAction)changeQuantity:(id)sender
{
    NSInteger val = [_quantityLabel.text integerValue];
    if(sender == _plusButton) val++;
    else if(sender == _minusButton && val > _dish.minimalQuantity) val--;
    _quantityLabel.text = [NSString stringWithFormat:@"%ld", (long)val];
    [self updatePriceLabel];

}


#pragma mark - Keyboard
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [aValue CGRectValue];
    CGRect convertedFrame = [self.view.window convertRect: keyboardFrame toView: self.view];
    CGRect intersect = CGRectIntersection(_tableView.frame, convertedFrame);
    if (CGRectIsNull(intersect) == NO)
    {
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height, 0);
        _tableView.scrollIndicatorInsets = _tableView.contentInset;
    }
}
- (void)keyboardHidden
{
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}
@end
