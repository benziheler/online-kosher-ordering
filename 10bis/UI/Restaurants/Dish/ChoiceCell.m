//
//  SubChoiceCell.m
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "ChoiceCell.h"
#import "Choice.h"
#import "SubChoice.h"
@implementation ChoiceCell
{
    __weak IBOutlet UILabel *_textLabel;
    CGRect _titleRect;
    __weak IBOutlet UIView *_grayView;
}
- (void)awakeFromNib
{
    [_textField setPlaceholder:@"Provide special instructions"];

    _titleRect = _textLabel.frame;
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundView = [UIView new];
    if ([Utils isIOS7])
        _textField.tintColor = RGB(50, 50, 50);
    else
        [[_textField valueForKey:@"textInputTraits"] setValue: RGB(50, 50, 50) forKey:@"insertionPointColor"];
}
- (void)setChoice:(Choice *)choice
{
    _choice = choice;
    UIImage *image = nil;
    UIImage *selectedImage = nil;
    _textLabel.frame = _titleRect;
    switch (choice.type) {
        case ChoiceTypeUnknown:
            _textField.hidden = YES;
            self.iconImageView.hidden = YES;
            _textLabel.hidden = NO;
            _textLabel.text = @"Unknown";
            break;
        case ChoiceTypeCheckBox:
            image = [UIImage imageNamed: @"checkbox"];
            selectedImage = [UIImage imageNamed: @"checkbox_filled"];
            _textLabel.hidden = self.iconImageView.hidden = NO;
            _textField.hidden = YES;
            break;
        case ChoiceTypeComboBox:
        case ChoiceTypeBeverages:
        {
            _textLabel.hidden = NO;
            _textField.hidden = self.iconImageView.hidden = YES;
            
            CGRect labelRect = _titleRect;
            labelRect.size.width += 33.0f;
            _textLabel.frame = labelRect;
        }
            break;
        case ChoiceTypeRadioButtons:
            image = [UIImage imageNamed: @"radio_btn-empty"];
            selectedImage = [UIImage imageNamed: @"radio_btn-full"];
            _textLabel.hidden = self.iconImageView.hidden = NO;
            _textField.hidden = YES;
            break;
        case ChoiceTypeNotes:
            _textLabel.hidden = self.iconImageView.hidden = YES;
            _textField.hidden = NO;
            break;
    }
    if (image)
        self.iconImageView.image = image;
    if (selectedImage)
        self.iconImageView.highlightedImage = selectedImage;
}
- (void)setSubChoice:(SubChoice *)subChoice
{
    float price = [subChoice.price floatValue];
    if (price != 0) {
        _textLabel.text = [NSString stringWithFormat:@"%@ ($%@)",subChoice.name, subChoice.price];
    }else{
        _textLabel.text = subChoice.name;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected: selected animated: animated];
    self.iconImageView.highlighted = selected;
    if (_choice.type == ChoiceTypeComboBox ||
        _choice.type == ChoiceTypeBeverages)
    {
        _grayView.hidden = !selected;
    }
    else
        _grayView.hidden = YES;
}
- (void)layoutSubviews
{
    [super layoutSubviews];

    CGRect labelRect = _textLabel.frame;
    labelRect.size.height = self.frame.size.height - 30.0f;
    _textLabel.frame = labelRect;

    if ([Utils isIOS7] == NO)
    {
        // ios6 group table inset fix
        CGRect r = self.contentView.frame;
        r.origin.x = 0.0f;
        r.size.width = DEVICE_WIDTH;
        r.size.height = self.frame.size.height;
        self.contentView.frame = r;
    }
}
+ (CGFloat)heightForSubChoice:(SubChoice*)subChoice
{
    CGSize textSize = [subChoice.name sizeWithFont: [UIFont fontWithName: @"ArialMT" size: 16] constrainedToSize: CGSizeMake(271, CGFLOAT_MAX) lineBreakMode: NSLineBreakByClipping];
    CGFloat cellHeight = ceilf(textSize.height + 30.0f);
    return MAX(cellHeight, 52.0f);

}

@end
