//
//  DishVC.h
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@class Restaurant, Dish, DishConfirmation;
@interface DishVC : BaseVC
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, copy) Dish *dish;
@property (nonatomic, strong) DishConfirmation *confirmation;
@property (nonatomic, copy) void (^didAddDish)();
@property (nonatomic, copy) void (^didUpdateDish)();
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@end
