//
//  GradientView.h
//  10bis
//
//  Created by Vadim Pavlov on 25.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradientView : UIView
@property (nonatomic, strong) NSArray *colors;
@end
