//
//  MenuHeader.m
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "MenuHeader.h"
#import <CoreText/CoreText.h>
#import "Utils.h"

@implementation MenuHeader

- (void)awakeFromNib
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(click:)];
    [self addGestureRecognizer: tap];
    _headerHeight = self.frame.size.height;
}
- (void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    UIImage *arrowImage = [UIImage imageNamed: isOpen ? @"icon_next_up" : @"icon_next_down"];
    _arrImageView.image = arrowImage;
    _headerLabel.x = 20;
    _headerLabel.width = DEVICE_WIDTH - 45;

}
- (void)click:(id)sender
{
    [UIView animateWithDuration: 0.2f
                     animations:^{
                         _arrImageView.transform = CGAffineTransformMakeRotation(M_PI);
                     } completion:^(BOOL finished) {
                         _arrImageView.transform = CGAffineTransformIdentity;
                         [self setIsOpen: !_isOpen];
                     }];
    
    _didClickOnHeader(self);
}

+(CGFloat)heightforStr:(NSString *)str
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 70;

    if([Utils isIOS7])
    {
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, MAXFLOAT)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{
                                                    NSFontAttributeName : [UIFont fontWithName:@"ArialMT" size:27.0f]
                                                    }
                                          context:nil].size;
        CGFloat cellHeight = ceilf(size.height +2);

        return MAX(cellHeight, 52.0f);

    }
    else
    {
    CGSize textSize = [str sizeWithFont: [UIFont fontWithName: @"ArialMT" size: 25.0f] constrainedToSize: CGSizeMake(width, CGFLOAT_MAX) lineBreakMode: NSLineBreakByTruncatingTail];
        CGFloat cellHeight = ceilf(textSize.height +2);

    return MAX(cellHeight, 52.0f);
    }
}


+ (CGFloat)heightForText:(NSAttributedString*)hederText
{
    CGFloat width = [UIScreen mainScreen].bounds.size.width - 70;

    CGSize textSize = [hederText boundingRectWithSize: CGSizeMake(width, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    CGFloat cellHeight = ceilf(textSize.height +2);
    return MAX(cellHeight, 52.0f);
}
- (void)setHeaderHeight:(CGFloat)headerHeight
{
    if (_headerHeight == headerHeight)
        return;
    _headerHeight = headerHeight;
    CGRect labelRect = _headerLabel.frame;
    labelRect.size.height = headerHeight-2;
    labelRect.origin.x = 20;
    labelRect.size.width = DEVICE_WIDTH - 45;
    _headerLabel.frame = labelRect;
    
    
    CGRect bgRect = _bgView.frame;
    bgRect.size.height = headerHeight;
    _bgView.frame = bgRect;
    _imgView.height = headerHeight-2;

    
    [UIView animateWithDuration: 0.2f animations:^{
        CGRect arrowRect = _arrImageView.frame;
        arrowRect.origin.y = roundf((headerHeight - arrowRect.size.height)/2);
        _arrImageView.frame = arrowRect;
    }];
}
@end
