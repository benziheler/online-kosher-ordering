//
//  RestaurantMenuVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantMenuVC.h"
#import "Restaurant.h"
#import "MenuHeader.h"
#import "MenuCell.h"
#import "DataManager.h"
#import "Menu.h"
#import "Dish.h"
#import "WebVC.h"
#import "DishVC.h"
#import "StarsView.h"
#import "GradientView.h"
#import "CartVC.h"
@class OrderConfirmation;
@interface RestaurantMenuVC () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation RestaurantMenuVC
{
    NSMutableArray *headerViews;
    __weak IBOutlet UILabel *_contactLabel;
    __weak IBOutlet UILabel *_workHoursLabel;
    __weak IBOutlet UILabel *_reviewsLabel;
    __weak IBOutlet UILabel *_distanceLabel;
    
    __weak IBOutlet NSLayoutConstraint *_cuisineWidthConstraint;
    __weak IBOutlet UILabel *orderLabel;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_phoneLabel;
    __weak IBOutlet UIImageView *_logoView;
    
    __weak IBOutlet UITableView *_tableView;
    
    __weak IBOutlet UILabel *_noMenuLabel;
    __weak IBOutlet UIButton *_kosherDetailsButton;
    __weak IBOutlet UILabel *_discountLabel;
    __weak IBOutlet UIButton *_discountButton;
    __weak IBOutlet UIButton *_categoryButton;
    
    __weak IBOutlet UILabel *currencySign;
    NSArray *_menu;
    NSMutableSet *_openedCategories;
    
    __weak Dish *_selectedDish;
    
    CGRect _initialCusineRect;
    __weak IBOutlet StarsView *_starsView;
    BOOL _isLoading;
    __weak IBOutlet UIImageView *_locationIcon;
    __weak IBOutlet UIImageView *_kosherIcon;
        __weak IBOutlet UIButton *_orderButton;
    __weak IBOutlet GradientView *_orderView;
    DishVC *_dishScreen;
    
    CartVC *_cartVC;
    OrderConfirmation *_order;
}
- (void)setRestaurant:(Restaurant *)restaurant
{
    _restaurant = restaurant;

       _isLoading = YES;
    __weak RestaurantMenuVC *weakSelf = self;
    [DataManager getRestaurantMenu: restaurant.restaurantId userId: nil completion:^(id result, NSString *error) {
        RestaurantMenuVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            strongSelf->_isLoading = NO;
            [strongSelf removeLoadingOverlay];
            if (error)
                [Utils showErrorMessage: error];
            else
            {
                strongSelf->_menu = result;
                strongSelf->_openedCategories = [NSMutableSet setWithCapacity: strongSelf->_menu.count];
                headerViews = [NSMutableArray array];
                for (int i=0;i<_menu.count;i++) {
                    [headerViews addObject:[self headerFromCategoryIndex:i]];
                }
                [strongSelf->_tableView reloadData];
                if ([DataManager deliveryMethod] != DeliveryMethodSitting)
                {
                    [strongSelf updateCart];
                }
                if(_menu.count == 0)
                {
                    _noMenuLabel.hidden = NO;
                }
                else
                {
                    _noMenuLabel.hidden = YES;
                }
                
            }
        }
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 [_discountLabel setValue: [NSValue valueWithCGAffineTransform: CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-30))] forKey: @"transform"];
    _noMenuLabel.textColor = [Utils appPrimaryColor];
      if([[DataManager appDomain] isEqualToString:@"kosherordering"])
    {
        _kosherDetailsButton.hidden = NO;
    }
    _discountLabel.textColor = [Utils appTextColor];

    currencySign.textColor = [Utils appSecondaryColor];
    orderLabel.textColor = [Utils appPrimaryColor];
    [_orderButton setTitleColor:[Utils appPrimaryColor] forState:UIControlStateNormal];
    [_orderButton setBackgroundColor:[Utils appTextColor]];
    _initialCusineRect = _categoryButton.frame;
    if (_isLoading)
        [self showLoadingOverlay];
    
    DeliveryMethod method = [DataManager deliveryMethod];
    
    NSString *screenTitle = [NSString stringWithFormat: @"%@", _restaurant.name];
    
    _orderButton.layer.cornerRadius = 3.0f;
	// Do any additional setup after loading the view.
    UILabel *titleLabel = [[UILabel alloc]initWithFrame: CGRectMake(0, 0, 290, 30)];
    NSDictionary *attrs = [[UINavigationBar appearance] titleTextAttributes];
    titleLabel.font = attrs[NSFontAttributeName];
    titleLabel.textColor = attrs[NSForegroundColorAttributeName];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = screenTitle; //[NSString stringWithFormat: @"Menu - %@", _restaurant.name];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *titleWrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 40)];
    [titleWrapView addSubview: titleLabel];
    titleWrapView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleWrapView;
    _contactLabel.text = _restaurant.street;
    if (_restaurant.phone)
    {
        NSString *text = [NSString stringWithFormat: @"Tel: %@", _restaurant.phone];
        NSMutableAttributedString *attrContactText = [[NSMutableAttributedString alloc] initWithString:text];
        [Utils addBoldAttributeToString: attrContactText forText:text font: 13];
        _phoneLabel.attributedText = attrContactText;
    }
    else
        _phoneLabel.text = @"";
    
    _titleLabel.text = screenTitle;
    // reviews
    if(_restaurant.numberOfReviews > 0)
    {
        _reviewsLabel.hidden = NO;
        _starsView.hidden = NO;
        
        NSString *reviewsText = [NSString stringWithFormat: @"%ld reviews", (long)_restaurant.numberOfReviews];
        NSMutableAttributedString *attrReviewsText = [[NSMutableAttributedString alloc] initWithString: reviewsText];
        
        [Utils addBoldAttributeToString: attrReviewsText forText: [NSString stringWithFormat: @"%ld", (long)_restaurant.numberOfReviews] font: 13];
        _reviewsLabel.attributedText = attrReviewsText;
        
        [_starsView setStars: _restaurant.reviewsRank];
        
        [_reviewsLabel sizeToFit];
        CGRect reviewRect = _reviewsLabel.frame;
        CGRect starsRect = _starsView.frame;
        
        starsRect.origin.x = _workHoursLabel.frame.origin.x;
        reviewRect.origin.x = CGRectGetMaxX(starsRect) + 10;
        reviewRect.size.height = starsRect.size.height;
        reviewRect.origin.y = starsRect.origin.y - 1.0f;
        _reviewsLabel.frame = reviewRect;
        _starsView.frame = starsRect;
    }
    else
    {
        _reviewsLabel.hidden = YES;
        _starsView.hidden = YES;
    }
    if (_curLocation.longitude != 0 & _curLocation.latitude != 0)
        [self updateLocation];
    else
    {
        _distanceLabel.text = _restaurant.distanceFromUser;
        if ([_restaurant.distanceFromUser hasPrefix: @"0"])
        {
            _distanceLabel.hidden = YES;
            _locationIcon.hidden = YES;
        }
    }
    
    _logoView.layer.cornerRadius = 2.0f;
    _logoView.layer.masksToBounds = YES;
    
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    switch (method) {
        case DeliveryMethodDelivery:
            couponHidden = _restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)_restaurant.discountCouponPercent];
            break;
        case DeliveryMethodPickup:
            couponHidden = _restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)_restaurant.discountCouponPercent];
            break;
        case DeliveryMethodSitting:
            if (_restaurant.isHappyHourActive == NO ||
                [_restaurant.happyHourDiscount isEqualToString: @"0%"])
            {
                couponHidden = YES;
            }
            couponText = _restaurant.happyHourDiscount;
            break;
        default:
            break;
    }
    // discount
    _discountButton.hidden = _discountLabel.hidden = couponHidden;
    if (method == DeliveryMethodSitting || _restaurant.couponHasRestrictions)
        couponText = [@"* " stringByAppendingString: couponText];
    
    _workHoursLabel.text = nil;
    // hours or delivery fee
    if (method == DeliveryMethodDelivery && _restaurant.minimumOrder && _restaurant.deliveryPrice)
    {
        NSString *minOrder = nil;
        NSString *boldMinOrder = nil;
        NSString *deliveryPrice = nil;
        NSString *boldDeliveryPrice = nil;
        
        if (_restaurant.minimumPriceForOrder == 0)
        {
            minOrder = @"No minimum order";
            boldMinOrder = minOrder;
        }
        else
        {
            minOrder = [NSString stringWithFormat: @"Minimum order: $%ld" , (long)_restaurant.minimumPriceForOrder];
            boldMinOrder = [NSString stringWithFormat: @"$%ld", (long)_restaurant.minimumPriceForOrder];
        }
        
        if (_restaurant.deliveryPriceForOrder == 0)
        {
            deliveryPrice = @"Free Delivery";
            boldDeliveryPrice = deliveryPrice;
        }
        else
        {
            deliveryPrice = [NSString stringWithFormat: @"Delivery: %@", _restaurant.deliveryPrice];
            boldDeliveryPrice = [NSString stringWithFormat: @"%@", _restaurant.deliveryPrice];
        }
        NSString *fullString = [NSString stringWithFormat: @"%@ | %@", minOrder, deliveryPrice];
        NSMutableAttributedString *attrOrder = [[NSMutableAttributedString alloc] initWithString: fullString];
        
        [Utils addBoldAttributeToString: attrOrder forText: boldMinOrder font: _workHoursLabel.font.pointSize];
        [Utils addBoldAttributeToString: attrOrder forText: boldDeliveryPrice font: _workHoursLabel.font.pointSize];
        _workHoursLabel.attributedText = attrOrder;
    }
    else if (method == DeliveryMethodPickup && _restaurant.pickupActivityHours)
    {
        NSString *pickupHours = _restaurant.pickupActivityHours;
        NSString *hoursText = [NSString stringWithFormat: @"Hours: %@", pickupHours];
        NSMutableAttributedString *attrHoursText = [[NSMutableAttributedString alloc] initWithString: hoursText];
        [Utils addBoldAttributeToString: attrHoursText forText: pickupHours font: _workHoursLabel.font.pointSize];
        _workHoursLabel.attributedText = attrHoursText;
    }
    else if (method == DeliveryMethodDelivery)
        _workHoursLabel.hidden = YES;
    
    
    _discountLabel.text = couponText;
    [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
    _categoryButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    _categoryButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
    if (_restaurant.cuisineList.length == 0)
    {
        _categoryButton.hidden = YES;
    }
    
    UIImage *bgImage = [_categoryButton backgroundImageForState: UIControlStateNormal];
    UIImage *stretchBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight: bgImage.size.height/2];
    [_categoryButton setBackgroundImage: stretchBgImage forState: UIControlStateNormal];
    
    [Utils loadImage: _restaurant.logoURL completion:^(UIImage *image) {
        _logoView.image = image;
    }];
    
    [_tableView registerNib: [UINib nibWithNibName: @"MenuCell" bundle: nil] forCellReuseIdentifier: @"MenuCell"];
    [_tableView registerNib: [UINib nibWithNibName: @"MenuHeader" bundle: nil] forHeaderFooterViewReuseIdentifier: @"MenuHeader"];
    
    if (method == DeliveryMethodSitting)
        [self hideToolbar];
    else
    {
        _orderView.colors = @[[[UIColor whiteColor] colorWithAlphaComponent:0.9f], [UIColor clearColor]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(cartClicked:)];
        [_orderView addGestureRecognizer: tap];
    }
    


}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if ([DataManager deliveryMethod] != DeliveryMethodSitting)
    {
        [self updateCart];
    }
}

- (MenuHeader *)headerFromCategoryIndex:(NSInteger)section
{
    MenuHeader *header = [_tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MenuHeader"];
    Menu *category = _menu[section];
    header.imgView.backgroundColor = [Utils appSecondaryColor];
    header.headerHeight = [MenuHeader heightforStr:category.categoryName];
    header.headerLabel.text = category.categoryName;
    header.isOpen = [_openedCategories containsObject: @(section)];
    header.section = section;
    header.contentView.backgroundColor = [UIColor clearColor];
    header.didClickOnHeader = ^(MenuHeader *clickedHeader){
        
        NSArray *dishes = [self dishesForCategory: clickedHeader.section];
        NSMutableArray *cellsToAnimate = [NSMutableArray arrayWithCapacity: dishes.count];
        for (int i = 0; i < dishes.count; i++)
        {
            [cellsToAnimate addObject: [NSIndexPath indexPathForRow: i inSection: section]];
        }
        UITableViewRowAnimation animation = UITableViewRowAnimationTop;
      
        
        [_tableView beginUpdates];
        if ([_openedCategories containsObject: @(section)])
        {
            [_openedCategories removeObject: @(section)];
            [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionNone animated:NO];

            [_tableView deleteRowsAtIndexPaths:cellsToAnimate withRowAnimation: animation];
         
          

        }
        else
        {
            [_openedCategories addObject: @(section)];
            [_tableView insertRowsAtIndexPaths:cellsToAnimate withRowAnimation: animation];

        }
        
        [_tableView endUpdates];
    };
    return header;

}

- (void)setCurLocation:(CLLocationCoordinate2D)curLocation
{
    _curLocation = curLocation;
    if (self.isViewLoaded)
    {
        [self updateLocation];
    }
}
- (void)updateLocation
{
    if (_curLocation.longitude != 0 & _curLocation.latitude != 0)
    {
        CLLocation *restaurantLocation = [[CLLocation alloc] initWithLatitude: _restaurant.latitude longitude: _restaurant.longitude];
        CLLocation *userLocation = [[CLLocation alloc] initWithLatitude: _curLocation.latitude longitude: _curLocation.longitude];
        
        CLLocationDistance distance = [userLocation distanceFromLocation: restaurantLocation] / 1000;
        _distanceLabel.text = [NSString stringWithFormat: @"%0.2f %@", distance,@"km from You"];
    }
}
- (void)hideToolbar
{
    CGRect tableRect = _tableView.frame;
    tableRect.size.height += 48.0f;
    _tableView.frame = tableRect;
    _orderView.hidden = YES;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"dishSegue"])
    {
        DishVC *dishVC = segue.destinationViewController;
        dishVC.restaurant = _restaurant;
        dishVC.dish = _selectedDish;
        dishVC.didAddDish = ^{
            [self didAddDish];
        };
    }
}
- (NSArray*)dishesForCategory:(NSInteger)categoryIndex
{
    Menu *menu = _menu[categoryIndex];
    return menu.dishList;
}
- (void)didAddDish
{
    [self updateCart];
    _dishScreen = [self.storyboard instantiateViewControllerWithIdentifier: @"DishVC"];
    _dishScreen.restaurant = _restaurant;
    [self addChildViewController:_dishScreen];
    [_dishScreen didMoveToParentViewController:self];
    _dishScreen.dish = _selectedDish;
    
    [self.view addSubview: _dishScreen.view];
    _dishScreen.view.frame = self.view.bounds;
    [self.navigationController popViewControllerAnimated:NO];
    UIView *view = _dishScreen.view;
    [UIView animateWithDuration: 0.6f animations:^{
        view.transform = CGAffineTransformScale(view.transform, 0.1f, 0.03f);
        CGRect buttonRect = [self.view convertRect: _orderButton.frame fromView: _orderView];
        view.x = buttonRect.origin.x;
        view.y = buttonRect.origin.y;
        
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
    [UIView animateWithDuration:0.2 delay:0.4 options:0 animations:^{
        view.alpha = 0.5;
    } completion:^(BOOL finished) {
        
    }];
}
#pragma mark - Actions
- (void)updateCart
{
    [DataManager shoppingCartTotalForRestaurant: _restaurant.restaurantId completion:^(id result, NSString *error) {
        if (error)
        {
            [Utils showErrorMessage: error];
        }
        else if ([result isKindOfClass: [NSString class]])
        {
            if ([result isEqualToString: @"-1"])
            {
                result = @"0";
            }
            [_orderButton setTitle: [NSString stringWithFormat: @"%@", result] forState: UIControlStateNormal];
        }
    }];
    
}
- (void)cartClicked:(id)sender
{
    [self showLoadingOverlay];
    __weak RestaurantMenuVC *weakSelf = self;
    
    [DataManager orderConfirmation:^(id result, NSString *error) {
        RestaurantMenuVC *strongSelf = weakSelf;
        
        if (strongSelf)
        {
            [strongSelf removeLoadingOverlay];
            if(strongSelf.navigationController.topViewController == strongSelf &&
               result)
            {
                strongSelf->_order = result;
                [strongSelf transitToCart];
            }
        }
        else if (error)
            [Utils showErrorMessage: error];
    }];
}
- (void)transitToCart
{
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:@"Shopping Cart"
                                                           value:nil] build]];
    
    _cartVC = [self.storyboard instantiateViewControllerWithIdentifier: @"CartVC"];
    _cartVC.order = _order;
    _cartVC.minOrder = _restaurant.minimumPriceForOrder;
    _cartVC.companyFlag = _restaurant.companyFlag;
    
    UIView *cartView = _cartVC.view;
    cartView.transform = CGAffineTransformScale(cartView.transform, 0.1f, 0.1f);
    cartView.alpha = 0.5f;
    CGRect buttonRect = [self.view convertRect: _orderButton.frame fromView: _orderView];
    cartView.frame = buttonRect;
    [self.view addSubview: cartView];
    [UIView animateWithDuration: 0.4f animations:^{
        cartView.transform = CGAffineTransformIdentity;
        cartView.alpha = 1.0f;
        cartView.frame = self.view.bounds;
    } completion:^(BOOL finished) {
        [cartView removeFromSuperview];
        [self.navigationController pushViewController: _cartVC animated: NO];
    }];
    
}
- (IBAction)showDiscountAlert:(id)sender
{
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [self showCustomAlertWithURL: url button: @"Close"];
}

- (IBAction)showCusieneList:(UIButton*)sender
{
    CGRect buttonRect = _categoryButton.frame;
    CGFloat width = self.view.frame.size.width - buttonRect.origin.x*2;
    if(_kosherDetailsButton.hidden == NO)
    {
        width -= _kosherDetailsButton.width;
    }
    

       
    [_categoryButton setTitle: @"  " forState: UIControlStateNormal];
    if (sender.selected)
    {
        width = _initialCusineRect.size.width;
    }
    _cuisineWidthConstraint.constant = width;
    
    
    [UIView animateWithDuration: 0.2f animations:^{
        [sender setNeedsUpdateConstraints];
        [sender layoutIfNeeded];
        if (sender.selected)
        {
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
        }
        else
        {
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
            
        }
        sender.selected = !sender.selected;
    }];
    __weak RestaurantMenuVC *weakSelf = self;
    
    if(sender.selected)
    {
        [Utils handleScreenTouch:^{
            [weakSelf showCusieneList:sender];
        }];
    }

}

#pragma mark - Table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _menu.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *dishes = [self dishesForCategory: section];
    BOOL isSectionOpened = [_openedCategories containsObject: @(section)];
    
    return isSectionOpened ? dishes.count : 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell = (MenuCell*)[tableView dequeueReusableCellWithIdentifier: @"MenuCell"];
    NSArray *dishes = [self dishesForCategory: indexPath.section];
    Dish *dish = dishes[indexPath.row];
    cell.dish = dish;
    
    DeliveryMethod method = [DataManager deliveryMethod];
    cell.arrowImageView.hidden = method == DeliveryMethodSitting;
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return headerViews[section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  //  BOOL isSectionOpened = [_openedCategories containsObject: @(section)];
    
    MenuHeader *header = headerViews[section];
   
    CGFloat height =  header.headerHeight;
    return height;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dishes = [self dishesForCategory: indexPath.section];
    Dish *dish = dishes[indexPath.row];
    CGFloat cellHeight = [MenuCell heightForDish: dish];
    return cellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodSitting)
        return;
    
    Menu *category = _menu[indexPath.section];
    Dish *dish = category.dishList[indexPath.row];
    
    __weak RestaurantMenuVC *weakSelf = self;
    [self showLoadingOverlay];
    [DataManager getDetailedDish: dish.dishId categoryId: dish.categoryId completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        RestaurantMenuVC *strongSelf = weakSelf;
        if (result && strongSelf)
        {
            strongSelf->_selectedDish = result;
            if (strongSelf.navigationController.topViewController == strongSelf)
            {
                [strongSelf performSegueWithIdentifier: @"dishSegue" sender: self];
            }
        }
        else
        {
            [Utils showErrorMessage: error];
        }
    }];
    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"empty" delegate: Nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
    //    [alert show];
    
}
- (IBAction)kosherDetailsPressed:(id)sender
{
        NSString *url = [NSString stringWithFormat:@"%@?resId=%@", [DataManager kosherDetailsUrl],_restaurant.restaurantId.stringValue];
        
    [self showCustomAlertWithURL: url button: @"Close"];


}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
