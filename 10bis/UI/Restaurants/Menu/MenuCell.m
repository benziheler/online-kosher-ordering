//
//  MenuCell.m
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "MenuCell.h"
#import "Dish.h"
@implementation MenuCell
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_subTitleLabel;
    __weak IBOutlet UILabel *_priceLabel;
}
- (void)setDish:(Dish *)dish
{
    _dish = dish;
    _titleLabel.attributedText = [MenuCell stringForDish:dish];
    _priceLabel.textColor = [Utils appTextColor];
    float price = floorf(dish.dishPrice.floatValue * 100 + 0.5) / 100;
    _priceLabel.text = [NSString stringWithFormat: @"$%.2f", price];
}

+ (NSAttributedString *)stringForDish:(Dish *)dish
{
    NSString *temp = [NSString stringWithFormat:@"%@\n%@", dish.dishName,dish.dishDesc];
    if(dish.dishDesc == nil || dish.dishDesc.length == 0) temp = dish.dishName;
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:temp];
    NSRange descRange = [temp rangeOfString: dish.dishDesc];
    NSRange nameRange = [temp rangeOfString: dish.dishName];

[str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Arial-BoldMT" size:16.0f] range:nameRange];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"ArialMT" size:16.0f] range:descRange];
    return str;
}
+ (CGFloat)heightForDish:(Dish*)dish
{

    CGFloat width = [UIScreen mainScreen].bounds.size.width - 105;
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:dish.dishName attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16], NSWritingDirectionAttributeName :@[@(NSWritingDirectionLeftToRight | NSTextWritingDirectionOverride)]}];
    NSAttributedString *descr = [[NSAttributedString alloc] initWithString:dish.dishDesc attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16],  NSWritingDirectionAttributeName :@[@(NSWritingDirectionLeftToRight | NSTextWritingDirectionOverride)]}];
    CGSize nameSize = [title boundingRectWithSize: CGSizeMake(width, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    CGSize descrSize = [descr boundingRectWithSize: CGSizeMake(width, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    CGFloat cellHeight = ceilf(nameSize.height + descrSize.height + 12);
    return MAX(cellHeight, 52.0f);
    
    return MAX(cellHeight, 52.0f);
}





@end
