//
//  RestaurantMenuVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@class Restaurant;
@interface RestaurantMenuVC : BaseVC <UIGestureRecognizerDelegate>
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, assign) CLLocationCoordinate2D curLocation;

@end
