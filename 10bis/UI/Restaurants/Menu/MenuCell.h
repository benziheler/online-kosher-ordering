//
//  MenuCell.h
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Dish;
@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (nonatomic, strong) Dish *dish;
+ (CGFloat)heightForDish:(Dish*)dish;
@end
