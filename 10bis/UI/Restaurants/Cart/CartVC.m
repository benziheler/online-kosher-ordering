//
//  CartVC.m
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "CartVC.h"
#import "OrderConfirmation.h"
#import "BillingLine.h"
#import "DishCell.h"
#import "BillingCell.h"
#import "DishConfirmation.h"
#import "DishVC.h"
#import "WebVC.h"
#import "Restaurant.h"
#import "Dish.h"
@interface CartVC () <UITableViewDataSource, UITableViewDelegate>


@end

@implementation CartVC
{
    __weak IBOutlet UITextField *_fakeTF;
    __weak IBOutlet UITableView *_tableView;
    NSMutableArray *_billingLinesWithoutTip;
    BillingLine *_totalCharge;
    __weak IBOutlet AppRedButton *_confirmButton;
    
    __weak BillingCell *_tipCell;
    NSDictionary *_selectedTip;
    
    float _subTotalAmount;
    Dish *_dishToEdit;
    DishConfirmation *_dishConfirmation;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_tableView registerNib: [UINib nibWithNibName: @"BillingCell" bundle: nil] forCellReuseIdentifier: @"BillingCell"];
    [_tableView registerNib: [UINib nibWithNibName: @"DishCell" bundle: nil] forCellReuseIdentifier: @"DishCell"];
    CGRect tableRect = _tableView.frame;
    tableRect.size.height = CGRectGetMinY(_confirmButton.frame) - 14.0f;
    _tableView.frame = tableRect;
}
- (IBAction)continueOrder:(id)sender
{
    CGFloat amountWithTax = _totalCharge.amount.floatValue - [_selectedTip[@"Price"] floatValue];
    if (_subTotalAmount == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Empty shopping cart" message: @"Please add a dish from the men]u" delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    } else if (amountWithTax < _minOrder && _companyFlag == NO  && [DataManager deliveryMethod] == DeliveryMethodDelivery) {
        NSString *msg = [NSString stringWithFormat: @"This restaurant has a minimum order requirement of \n%ld $", (long)_minOrder];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Minimum Order" message: msg delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    } else {
        if ([DataManager currentUser])
        {
            id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                                       action:@"button_press"
                                                                        label:@"Checkout"
                                                                        value:nil] build]];

            [DataManager setLoginForCheckout: NO];
            [self performSegueWithIdentifier: @"WebSegue" sender: self];
        }
        else
        {
            [DataManager setLoginForCheckout: YES];
            [self performSegueWithIdentifier: @"LoginSegue" sender: self];
        }
    }
}
- (void)updateOrder
{
    [self showLoadingOverlay];
    __weak CartVC *weakSelf = self;
    [DataManager orderConfirmation:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (result)
            weakSelf.order = result;
        else if (error)
            [Utils showErrorMessage: error];
    }];
}

- (void)done
{
    _tableView.userInteractionEnabled = YES;
    [_fakeTF resignFirstResponder];
    CGFloat tipPrice = [_tipCell.tip[@"Price"] floatValue];
    NSInteger oldPrice = [_selectedTip[@"Price"] floatValue];
    if (tipPrice != oldPrice)
    {
        _selectedTip = _tipCell.tip;
        __weak CartVC *weakSelf = self;
        [self showLoadingOverlay];
        
        [DataManager setTipToCart: _order.restaurantId tipAmount: [NSNumber numberWithFloat:tipPrice] completion:^(id result, NSString *error) {
            if (error)
            {
                [Utils showErrorMessage: error];
                CartVC *strongSelf = weakSelf;
                if (strongSelf)
                {
                    [strongSelf refreshBillingLines: strongSelf->_order.billingLines];
                }
            }
            else if (result)
                [weakSelf refreshBillingLines: result];
            [weakSelf removeLoadingOverlay];
        }];
    }
}


- (void)setOrder:(OrderConfirmation *)order
{
    _order = order;
    [self refreshBillingLines: order.billingLines];
}
- (void)refreshBillingLines:(NSArray*)lines
{
    // extract tips & total charge
    _totalCharge = nil;
    _selectedTip = nil;
    _subTotalAmount = 0;
    
    _billingLinesWithoutTip = [NSMutableArray arrayWithCapacity: lines.count];
    for (BillingLine *line in lines)
    {
        if (line.type != BillingLineTip) {
            [_billingLinesWithoutTip addObject: line];
        }
        if (line.type == BillingLineTotalToCharge)
        {
            _totalCharge = line;
        }
        else if (line.type == BillingLineTip)
        {
            _selectedTip = @{@"Price": line.amount};
        }
        else if (line.type == BillingLineSubTotal)
            _subTotalAmount = [line.amount floatValue];
                 
    }
    NSMutableArray *billingLines = [NSMutableArray arrayWithArray: lines];
    [billingLines removeObject: _totalCharge];
    _order.billingLines = billingLines;
    
    if (self.isViewLoaded)
    {
        [_tableView reloadData];
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"editDishSegue"])
    {
        DishVC *dishVC = segue.destinationViewController;
        dishVC.confirmation = _dishConfirmation;
        dishVC.dish = _dishToEdit;
        
        Restaurant *restaurant = [Restaurant new];
        restaurant.restaurantId = _order.restaurantId;
        dishVC.restaurant = restaurant;
        
        __weak CartVC *weakSelf = self;
        dishVC.didUpdateDish = ^{
            [weakSelf updateOrder];
        };
    }
    else if ([segue.identifier isEqualToString: @"WebSegue"])
    {
        WebVC *webVC = segue.destinationViewController;
        webVC.showNavigationBar = YES;
        webVC.urlToLoad = [DataManager checkoutURLForRestaurant: _order.restaurantId];
        float price = floorf(_totalCharge.amount.floatValue * 100 + 0.5) / 100;

        webVC.title = [NSString stringWithFormat: @"Total Charge: $%.2f", price];
    }

}
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 0;
    switch (section)
    {
        case 0:
            number = _order.dishes.count;
            break;
        case 1:
            number = _order.showTipSelection ? _order.billingLines.count : _billingLinesWithoutTip.count;
            break;
        case 2:
            number = 1;
            break;
    }
    return number;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == 0)
    {
        DishCell *dishCell = (DishCell*)[tableView dequeueReusableCellWithIdentifier: @"DishCell"];
        cell = dishCell;
        DishConfirmation *dish = _order.dishes[indexPath.row];
        dishCell.dish = dish;
    }
    else
    {
        BillingCell *billingCell = (BillingCell*)[tableView dequeueReusableCellWithIdentifier: @"BillingCell"];
        cell = billingCell;
        
        if (indexPath.section == 2)
        {
            billingCell.line = _totalCharge;
            [billingCell setTipSelection:nil];
        }
        else
        {
            [billingCell setTip:_selectedTip subtotalAmount:_subTotalAmount];
            [billingCell setTipSelection:^(BillingCell *cell, NSInteger prevIndex) {
                CGFloat percent = 10.0+ (cell.segmentedControl.selectedSegmentIndex * 5.0);
                CGFloat tip = _subTotalAmount / 100.0 * percent;
                NSDictionary *tipDict = @{@"Price" : [NSNumber numberWithFloat:tip]};
                if(cell.segmentedControl.selectedSegmentIndex == -1)
                {
                    tipDict = @{@"Price" : [NSNumber numberWithFloat:0]};
                    
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_tipCell setTip:tipDict subtotalAmount:_subTotalAmount];
                    [self done];
                });
            }];
            
            _tipCell = billingCell;
            [_tipCell setTipChanged:^(CGFloat newTip) {
                [_tipCell setTip: @{@"Price" : [NSNumber numberWithFloat:newTip]} subtotalAmount:_subTotalAmount];
                [self done];
            }];
            
            BillingLine *line = _order.showTipSelection ? _order.billingLines[indexPath.row] : _billingLinesWithoutTip[indexPath.row];
            billingCell.line = line;
        }
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        DishConfirmation *dish = _order.dishes[indexPath.row];

        return [DishCell heightForDish:dish];
    }
    return 52;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 1.0f;
    switch (section) {
        case 1:
            height = 2.0f;
            break;
        case 2:
            height = 9.0f;
            break;
    }
    return height;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 1.f)];
    footer.backgroundColor = section == 0 ? [Utils appPrimaryColor] : [UIColor clearColor];
    return footer;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    switch (section) {
        case 1:
        {
            view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 2.0f)];
            view.backgroundColor = [Utils appPrimaryColor];
        }
            break;
        case 2:
        {
            view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 9.0f)];
            view.backgroundColor = [UIColor clearColor];
            
            UIView *redline = [[UIView alloc] initWithFrame: CGRectMake(0, 8, DEVICE_WIDTH, 1)];
            redline.backgroundColor = [Utils appPrimaryColor];
            [view addSubview: redline];
        }
            break;
    }
    return view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        [self showLoadingOverlay];
        DishConfirmation *dish = _order.dishes[indexPath.row];
        
        _dishConfirmation = dish;
        __weak CartVC *weakSelf = self;
        [DataManager getDetailedDish: dish.dishId categoryId: dish.dishOwnerId completion:^(Dish *detailedDish, NSString *error) {
            CartVC *strongSelf = weakSelf;
            if (strongSelf)
            {
                [strongSelf removeLoadingOverlay];
                if (error)
                {
                    [Utils showErrorMessage: error];
                    return;
                }
                
                if (strongSelf.navigationController.topViewController == strongSelf)
                {
                    detailedDish.orderIndex = dish.orderIndex;
                    strongSelf->_dishToEdit = detailedDish;
                    [strongSelf performSegueWithIdentifier: @"editDishSegue" sender: strongSelf];
                }
            }
        }];
    }
}

@end
