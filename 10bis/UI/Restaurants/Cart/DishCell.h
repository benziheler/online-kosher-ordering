//
//  DishCell.h
//  10bis
//
//  Created by Vadim Pavlov on 14.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DishConfirmation;
@interface DishCell : UITableViewCell <UITextFieldDelegate>
@property (nonatomic, strong) DishConfirmation *dish;

+ (CGFloat)heightForDish:(DishConfirmation *)dish;

@end
