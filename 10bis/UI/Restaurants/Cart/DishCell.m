//
//  DishCell.m
//  10bis
//
//  Created by Vadim Pavlov on 14.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "DishCell.h"
#import "DishConfirmation.h"
@implementation DishCell
{
    __weak IBOutlet UILabel *_priceLabel;
    __weak IBOutlet UILabel *_userLabel;
    __weak IBOutlet UILabel *_nameLabel;
    __weak IBOutlet UILabel *_quantityLabel;
}
- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundView = [UIView new];
}

- (void)setDish:(DishConfirmation *)dish
{
    _priceLabel.textColor = [Utils appSecondaryColor];
    _dish = dish;
    float price = floorf(dish.totalDishPrice.floatValue * 100 + 0.5) / 100;

    _priceLabel.text = [NSString stringWithFormat: @"$%.2f", price];
    _nameLabel.text = dish.dishName;
    [_nameLabel sizeToFit];
    _userLabel.text = dish.dishUsername;
    _quantityLabel.text = [NSString stringWithFormat: @"%ld X", (long)dish.quantity];
    if(dish.dishUsername.length < 1)
    {
        _quantityLabel.centerY = [DishCell heightForDish:dish]/2;
        _nameLabel.centerY = [DishCell heightForDish:dish]/2;
    }
    else
    {
        _quantityLabel.y = 4;
        _nameLabel.y = 4;
    }
}

+ (CGFloat)heightForDish:(DishConfirmation *)dish
{
    
    return [DishCell heightforStr:dish.dishName] + 27;
}

+(CGFloat)heightforStr:(NSString *)str
{
    if([Utils isIOS7])
    {
        CGSize size = [str boundingRectWithSize:CGSizeMake(185, MAXFLOAT)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{
                                                  NSFontAttributeName : [UIFont fontWithName:@"ArialMT" size:16.0f]
                                                  }
                                        context:nil].size;
        CGFloat cellHeight = ceilf(size.height);
        return MAX(cellHeight, 21.0f);
    }
    else
    {
        CGSize textSize = [str sizeWithFont: [UIFont fontWithName: @"ArialMT" size: 16.0f] constrainedToSize: CGSizeMake(185, CGFLOAT_MAX) lineBreakMode: NSLineBreakByTruncatingTail];
        CGFloat cellHeight = ceilf(textSize.height +2);
        return MAX(cellHeight, 21.0f);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{

}

- (void)layoutSubviews
{
    [super layoutSubviews];    
    if ([Utils isIOS7] == NO)
    {
        // ios6 group table inset fix
        CGRect r = self.contentView.frame;
        r.origin.x = 0.0f;
        r.size.width = DEVICE_WIDTH;
        r.size.height = self.frame.size.height;
        self.contentView.frame = r;
    }
}
@end
