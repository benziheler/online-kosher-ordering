//
//  BillingCell.m
//  10bis
//
//  Created by Vadim Pavlov on 14.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "BillingCell.h"
#import "BillingLine.h"
@implementation BillingCell
{
    __weak IBOutlet UILabel *_nameLabel;
    __weak IBOutlet UIImageView *_priceHolderView;
    __weak IBOutlet UILabel *_signLabel;
    __weak IBOutlet UILabel *_priceLabel;
    __weak IBOutlet UIView *_priceView;
    __weak IBOutlet UITextField *_tipTextField;
    NSDictionary *_tempTip;
}
- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundView = [UIView new];
    __weak BillingCell *weakSelf = self;
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [numberToolbar setTintColor:[Utils appPrimaryColor]];
    numberToolbar.items = [NSArray arrayWithObjects:
                       flexibleSpace,
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    _tipTextField.inputAccessoryView = numberToolbar;
    PPiFlatSegmentedControl *segmented=[[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(106, 11, 130, 30)
                                                                                items:@[@{@"text":@"10%",@"icon":@""},
                                                                                        @{@"text":@"15%",@"icon":@"icon-linkedin"},
                                                                                        @{@"text":@"20%",@"icon":@"icon-twitter"}]
                                                                         iconPosition:IconPositionRight andSelectionBlock:^(NSUInteger segmentIndex) {
                                                                             if(weakSelf.segmentedControl.userInteractionEnabled)
                                                                             {
                                                                                 _tempTip = nil;
                                                                                 NSLog(@"segmentChanged");
                                                                                 if (_tipSelection)
                                                                                 {
                                                                                     _tipSelection(self, segmentIndex);
                                                                                 }
                                                                                 self.prevSelectedIndex = _segmentedControl.selectedSegmentIndex;
                                                                                 weakSelf.segmentedControl.userInteractionEnabled = NO;
                                                                                 
                                                                                 double delayInSeconds = 1.0f;
                                                                                 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                                                                                 dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                                                                     weakSelf.segmentedControl.userInteractionEnabled = YES;
                                                                                     
                                                                                 });
                                                                             }
                                                                             else
                                                                             {
                                                                                 [weakSelf.segmentedControl setEnabled:YES forSegmentAtIndex:weakSelf.prevSelectedIndex];
                                                                             }
                                                                             
                                                                             
                                                                             
                                                                         } iconSeparation:3];
    segmented.color = [UIColor whiteColor];
    segmented.borderWidth = 1;
    segmented.borderColor = [Utils appPrimaryColor];
    segmented.selectedColor = [Utils appPrimaryColor];
    segmented.textAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],
                               NSForegroundColorAttributeName:[Utils appPrimaryColor]};
    segmented.selectedTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],
                                       NSForegroundColorAttributeName:[Utils appTextColor]};
    self.segmentedControl = segmented;
    segmented.hidden = YES;
    [self.contentView addSubview:segmented];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundView = [UIView new];
    _priceHolderView.backgroundColor = [Utils appPrimaryColor];
    _priceView.backgroundColor = [Utils appPrimaryColor];
    _priceView.layer.cornerRadius = 2;
    
    _tipTextField.textColor = [Utils appPrimaryColor];
    _priceLabel.textColor = [Utils appTextColor];
}
- (void)setLine:(BillingLine *)line
{
    _line = line;
    _nameLabel.text = line.caption;
    _priceLabel.text = [NSString stringWithFormat: @"$ %.2f", line.amount.floatValue];
    CGRect frame = _priceView.frame;
    if(line.isImportantPaymentType)
    {
        frame.size.width = 55;
        frame.size.height = 33;
        frame.origin.y = 9;
    }
    else
    {
        frame.size.width = 55;
        frame.size.height = 28;
        frame.origin.y = 12;
    }
    _priceView.frame = frame;
    
    _nameLabel.font = [UIFont fontWithName: line.isImportantPaymentType ? @"Arial-BoldMT" : @"Arial" size: 16];
    if (line.type == BillingLineTip)
    {
        _tipTextField.delegate = self;
        _tipTextField.hidden = NO;
        _segmentedControl.hidden = NO;
        _priceView.hidden = YES;
    }
    else
    {
        _segmentedControl.hidden = YES;
        _tipTextField.delegate = nil;
        _tipTextField.hidden = YES;
        _priceView.hidden = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.location > 1)
    {
        [_segmentedControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
        NSString *str = textField.text;
        str = [str stringByReplacingCharactersInRange:range withString:string];
        CGFloat tipVal = 0;
        if(str.length > 2)
        {
            str = [str substringFromIndex:2];
            tipVal = [str floatValue];
        }
        
        _tempTip = @{@"Price": [NSNumber numberWithFloat:tipVal]};
        return YES;
    }
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(_tipChanged)
    {
        _tipChanged([_tempTip[@"Price"] floatValue]);
    }
    [textField resignFirstResponder];
    _tempTip = nil;
    return YES;
}


- (void)setTip:(NSDictionary *)tip subtotalAmount:(CGFloat)subTotal
{
    NSString *title = [NSString stringWithFormat: @"$ %.2f", [tip[@"Price"] floatValue]];
    [_tipTextField setText:title];
    CGFloat rounded = [[NSString stringWithFormat:@"%.2f", [tip[@"Price"] floatValue]] floatValue];
    NSString *str = [NSString stringWithFormat:@"%.2f",rounded / subTotal];
    NSInteger index = _segmentedControl.selectedSegmentIndex;
    if([str isEqualToString:@"0.10"])
    {
        index = 0;
    }
    else if([str isEqualToString:@"0.15"])
    {
        index = 1;
    }
    else if([str isEqualToString:@"0.20"])
    {
        index = 2;
    }
    else
    {
        index = -1;
    }
    
    [_segmentedControl setEnabled:YES forSegmentAtIndex:index];
    
    self.prevSelectedIndex = _segmentedControl.selectedSegmentIndex;
    _tip = tip;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
}
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([Utils isIOS7] == NO)
    {
        // ios6 group table inset fix
        CGRect r = self.contentView.frame;
        r.origin.x = 0.0f;
        r.size.width = DEVICE_WIDTH;
        r.size.height = self.frame.size.height;
        self.contentView.frame = r;
    }
}

- (void)doneWithNumberPad
{
    if(_tipChanged && _tempTip)
    {
        _tipChanged([_tempTip[@"Price"] floatValue]);
    }
    [_tipTextField resignFirstResponder];
}

@end
