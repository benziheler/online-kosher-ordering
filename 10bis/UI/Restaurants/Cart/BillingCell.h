//
//  BillingCell.h
//  10bis
//
//  Created by Vadim Pavlov on 14.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPiFlatSegmentedControl.h"

@class BillingLine;
@interface BillingCell : UITableViewCell <UITextFieldDelegate>
@property (nonatomic, strong) BillingLine *line;
@property (nonatomic, copy) void (^tipSelection)(BillingCell *cell, NSInteger prevIndex);
@property (nonatomic, copy) void (^tipChanged)(CGFloat newValue);
@property (nonatomic, strong) NSDictionary *tip;
@property (nonatomic, assign) NSInteger prevSelectedIndex;

@property (strong, nonatomic) PPiFlatSegmentedControl *segmentedControl;
- (void)setTip:(NSDictionary *)tip subtotalAmount:(CGFloat)subTotal;


@end
