//
//  WelcomeVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "WelcomeVC.h"
#import "DataManager.h"
#import "ECSlidingViewController.h"
#import "GigyaVC.h"
#import "SearchRestaurantVC.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC
{
    __weak IBOutlet NSLayoutConstraint *deliveryTopConstraint;
    __weak IBOutlet UIButton *_deliveryButton;
    __weak IBOutlet UIImageView *_bgImageView;
    __weak IBOutlet UILabel *bottomLabel;
    __weak IBOutlet UIButton *pickUpButton;
    BOOL _isTransiting;
    CLLocation *currentLocation;
}
- (void)viewDidLoad
{

    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authFinished) name:kUserAuthorizationCompleteNotificationKey object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOut) name:kLogOutKey object:nil];
    UIImage *navbarImage = [[UIImage imageNamed:@"bg_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5) ];
    if ([Utils isIOS7])
    {
        navbarImage = [UIImage imageNamed:@"bg_headerNew"];
    }
    self.navigationController.navigationBar.tintColor = [Utils appPrimaryColor];

    [self.navigationController.navigationBar setBackgroundImage:navbarImage forBarMetrics:UIBarMetricsDefault];
	// Do any additional setup after loading the view.
    _bgImageView.image = [UIImage imageNamed: @"bg"]; // will set 4inch image if needed
    
    UIButton *menuButton = [Utils menuButton];
    [menuButton addTarget: self action: @selector(menuButtonTapped:) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithCustomView: menuButton];
    self.navigationItem.leftBarButtonItem = menuItem;
    _deliveryButton.imageView.contentMode = UIViewContentModeCenter;
    pickUpButton.imageView.contentMode = UIViewContentModeCenter;
    
    if  ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) &&
         ([UIScreen mainScreen].bounds.size.height == 480.0f)) {
        _deliveryButton.imageView.layer.cornerRadius = 5.0f;
        pickUpButton.imageView.layer.cornerRadius = 5.0f;
        
        _deliveryButton.height = 165;
        deliveryTopConstraint.constant = 15;
        pickUpButton.height = 165;
        [pickUpButton setY:pickUpButton.y - 50];
        
    }
    //    _deliveryButton.imageView.layer.cornerRadius = 4.0f;
    //  [_deliveryButton setImage: [UIImage imageNamed: @"img_delivey"] forState: UIControlStateNormal]; // will update image for 4inch if needed
    bottomLabel.textColor = [Utils appPrimaryColor];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Already have an account? Click Here"];
    [str addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} range:NSMakeRange (0, 25)];
    [str addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} range:NSMakeRange (25, 10)];
    bottomLabel.attributedText = str;
    UIImageView *logo = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"logo_main"]];
    self.navigationItem.titleView = logo;
    if ([DataManager isAuthorizationComplete] == NO)
    {
        UIImageView *splash = [[UIImageView alloc] initWithFrame: self.view.bounds];
        splash.image = [UIImage imageNamed: @"Default"];
        splash.backgroundColor = [UIColor cyanColor];
        [self.navigationController.navigationBar addSubview: splash];
        
        [[NSNotificationCenter defaultCenter] addObserverForName: kUserAuthorizationCompleteNotificationKey object: nil queue: nil usingBlock:^(NSNotification *note) {
            [self showStatusBar];
            [UIView animateWithDuration: 0.2f animations:^{
                splash.alpha = 0.0f;
            } completion:^(BOOL finished) {
                [splash removeFromSuperview];
            }];
        }];
    }
    else
        [self showStatusBar];
}
- (void)showStatusBar
{
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarHidden: NO withAnimation: UIStatusBarAnimationSlide];
    
}
- (void)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [DataManager setDeliveryMethod: DeliveryMethodNone];
    [DataManager setQuickOrder: NO];
    [DataManager setLoginForCheckout: NO];
    [DataManager setSearchCityID: nil];
    [DataManager setSearchStreetID: nil];
    
    bottomLabel.hidden = ([DataManager currentUser] != nil);
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    _isTransiting = NO;
}

- (void)authFinished
{
    bottomLabel.hidden = ([DataManager currentUser] != nil);

}

#pragma mark - Actions
- (id <GAITracker>)tracker
{
    return [[GAI sharedInstance] defaultTracker];
}
- (IBAction)pickup:(id)sender
{
        NSLog(@"loc");
    if (_isTransiting)
        return;
    _isTransiting = YES;
    [DataManager setDeliveryMethod: DeliveryMethodPickup];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"Pickup"
                                                                value:nil] build]];
    
    if([[DataManager sharedManager] currentLocation])
    {
        [self performSegueWithIdentifier: @"searchRestaurantsSegue" sender: self];
    }
    else
    {
        [self performSegueWithIdentifier: @"addressSegue" sender: self];
        
    }
}
- (IBAction)visit:(id)sender
{
    if (_isTransiting)
        return;
    _isTransiting = YES;
    [DataManager setDeliveryMethod: DeliveryMethodSitting];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"Eat Out"
                                                                value:nil] build]];
    [self performSegueWithIdentifier: @"searchRestaurantsSegue" sender: self];
}
- (IBAction)delivery:(id)sender
{
    if (_isTransiting)
        return;
    _isTransiting = YES;
    
    [DataManager setDeliveryMethod: DeliveryMethodDelivery];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"Delivery"
                                                                value:nil] build]];
    
   /* if ([DataManager currentUser] == nil)
    {
        [self performSegueWithIdentifier: @"loginSegue" sender: self];
        return;
    }
    else */if([[DataManager sharedManager] currentLocation])
    {
        [self performSegueWithIdentifier: @"searchRestaurantsSegue" sender: self];
    }
    else
    {
        [self performSegueWithIdentifier: @"addressSegue" sender: self];

    }
}

- (IBAction)login:(id)sender {
    if ([DataManager currentUser] == nil)
    {
      //  [self performSegueWithIdentifier: @"loginSegue" sender: self];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"LoginSegue"])
    {
        GigyaVC *gigyaVC = segue.destinationViewController;
        gigyaVC.isNewCustomer = NO;
    }
}

- (void)logOut
{
    bottomLabel.hidden = ([DataManager currentUser] != nil);
}

@end
