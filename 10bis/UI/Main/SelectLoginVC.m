//
//  SelectLoginVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "SelectLoginVC.h"
#import "GigyaVC.h"

@interface SelectLoginVC ()

@end

@implementation SelectLoginVC
{
    BOOL _isNewCustomer;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)newCustomer:(id)sender
{
    _isNewCustomer = YES;
}
- (IBAction)existingCustomer:(id)sender
{
    _isNewCustomer = NO;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    GigyaVC *gigyaVC = segue.destinationViewController;
    gigyaVC.isNewCustomer = _isNewCustomer;
}
@end
