//
//  CustomWindow.m
//  10bis
//
//  Created by Anton Vilimets on 8/8/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "CustomWindow.h"

@implementation CustomWindow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)sendEvent:(UIEvent *)event
{
    //UITouch *touch = event.allTouches.anyObject;
    //if(touch.phase == UITouchPhaseBegan)
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kTapNotification object:nil];
//    }
    [super sendEvent:event];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
