//
//  AddressCell.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "AddressCell.h"
#import "Address.h"

@implementation AddressCell
{
    __weak IBOutlet UILabel *_addressLabel;

}
- (void)awakeFromNib
{
    _addressLabel.textColor = [Utils appPrimaryColor];
    self.backgroundColor = [UIColor clearColor];
}
+ (CGFloat)heightForAddress:(Address*)address
{
    CGSize textSize = [address.addressString sizeWithFont: [UIFont fontWithName: @"ArialMT" size: 19] constrainedToSize: CGSizeMake(264, CGFLOAT_MAX) lineBreakMode: NSLineBreakByTruncatingTail];
    CGFloat cellHeight = ceilf(textSize.height + 10 * 2); // offset for label 10 pixels
    return MAX(cellHeight, 60.0f);
}
- (void)setAddress:(Address *)address
{
    _address = address;
    _addressLabel.text = address.addressString;
}
@end
