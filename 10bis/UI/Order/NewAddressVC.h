//
//  NewAddressVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@interface NewAddressVC : BaseVC
@property (nonatomic, strong)     NSTimer *autocompleteTimer;

@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, assign) BOOL popToRoot;
@property (weak, nonatomic) IBOutlet UIView *cityLoadingView;
@property (weak, nonatomic) IBOutlet UIView *streetLoadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cityActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *streetActivityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *noResLabel;

@end


@interface NoAutoScrollUIScrollView : UIScrollView
@end