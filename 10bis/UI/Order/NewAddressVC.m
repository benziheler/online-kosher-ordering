//
//  NewAddressVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "NewAddressVC.h"
#import "DataManager.h"
#import "City.h"
#import "Street.h"
#import "Restaurant.h"
#import "RestaurantCell.h"
#import "SearchRestaurantVC.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "SPGooglePlacesPlaceDetailQuery.h"

@implementation NoAutoScrollUIScrollView
- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
	// Don'd do anything here to prevent autoscrolling.
	// Unless you plan on using this method in another fashion.
}
@end

@interface NewAddressVC () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
@implementation NewAddressVC
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UITextField *_cityTF;
    __weak IBOutlet UITextField *_streetTF;
    
    __weak IBOutlet UIScrollView *_scrollView;
    CLLocationCoordinate2D _selectedCoordinate;
    SPGooglePlacesAutocompletePlace *selectedPlace;
    NSString *tfText;
    CGFloat _keyboardBottomInset;
    CGFloat _topInset;
    
    NSString *selectedAddressString;
    
    UIView *_searchView;
    UISearchBar *_searchBar;
    UITextField *_searchTF;
    
    CGRect _initialSearchRect;
    CGRect _initialTableRect;
    CGPoint _contentOffset;
    NSArray *_searchResults;
    NSArray *_searchResultsRestaurants;

    __weak UITextField *_activeTF;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        self.title = [Utils screenTitleForDelivery: [DataManager deliveryMethod]];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)back
{
    if (_popToRoot)
        [self.navigationController popToRootViewControllerAnimated: YES];
    else
        [self.navigationController popViewControllerAnimated: NO];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    [self removeLoadingOverlay];

    [_searchView removeFromSuperview];
    _contentOffset = _scrollView.contentOffset;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    _scrollView.contentOffset = _contentOffset;
    if (CGRectEqualToRect(_initialTableRect, CGRectZero))
    {
        _initialTableRect = _tableView.frame;
    }
}
- (void)configureNavigationBar
{
    _initialSearchRect = CGRectMake(0, -64, DEVICE_WIDTH, 44);
    _searchView = [[UIView alloc] initWithFrame: _initialSearchRect];
    _searchView.backgroundColor = [UIColor clearColor];
    _searchBar = [[UISearchBar alloc] initWithFrame: _searchView.bounds];
    _searchBar.showsCancelButton = YES;
    _searchBar.backgroundImage = [UIImage alloc];
    _searchBar.tintColor = [Utils appPrimaryColor];
    _searchBar.delegate = self;
    [_searchView addSubview: _searchBar];
    
    if ([Utils isIOS7] == NO)
    {
        _searchBar.backgroundColor = [Utils appPrimaryColor];
    }
    
    
    UIButton *cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
    cancelButton.backgroundColor = [Utils appPrimaryColor];
    [cancelButton setTitle: @"Cancel" forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor lightGrayColor] forState: UIControlStateHighlighted];
    
    [cancelButton addTarget: self action: @selector(cancelButtonClicked) forControlEvents: UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(250, 5, 65, 34);
    cancelButton.titleLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
    [_searchBar addSubview: cancelButton];
    _searchBar.tintColor = [Utils appPrimaryColor];
    [self.navigationController.navigationBar addSubview: _searchView];
    
    for (UIView *subView in _searchBar.subviews){
        if ([subView isKindOfClass: [UITextField class]])
            _searchTF =  (UITextField *)subView;
        else
            for (UIView *secondLevelSubView in subView.subviews)
            {
                if ([secondLevelSubView isKindOfClass:[UITextField class]])
                {
                    _searchTF = (UITextField *)secondLevelSubView;
                    break;
                }
            }
        if (_searchTF)
        {
            _searchTF.enablesReturnKeyAutomatically = NO;
            _searchTF.textColor = [Utils appPrimaryColor];
            _searchTF.background = [UIImage alloc];
            _searchTF.borderStyle = UITextBorderStyleNone;
            _searchTF.backgroundColor = RGB(228, 227, 223);
            _searchTF.layer.cornerRadius = 2.0f;
            
            if ([Utils isIOS7])
                _searchTF.tintColor = [Utils appPrimaryColor];
            else
                [[_searchTF valueForKey:@"textInputTraits"] setValue: [Utils appPrimaryColor] forKey:@"insertionPointColor"];
            break;
        }
    }
    
    [UIView animateWithDuration: 0.3f animations:^{
        CGRect searchRect = _searchView.frame;
        searchRect.origin.y = 0;
        _searchView.frame = searchRect;
    } completion:^(BOOL finished) {
        [_searchTF becomeFirstResponder];
    }];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _noResLabel.textColor = [Utils appPrimaryColor];
    _cityTF.leftView = _cityLoadingView;
    _cityTF.leftViewMode = UITextFieldViewModeAlways;
    
    _cityTF.rightView = _streetLoadingView;
    _cityTF.rightViewMode = UITextFieldViewModeAlways;
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];
    [_tableView registerNib: [UINib nibWithNibName: @"RestaurantCell" bundle: nil] forCellReuseIdentifier: @"RestaurantCell"];
    _scrollView.contentSize = _scrollView.frame.size;
    _tableView.backgroundColor = [UIColor clearColor];
    [_cityTF becomeFirstResponder];

    DeliveryMethod method = [DataManager deliveryMethod];
    if(method == DeliveryMethodDelivery)
    {
        self.navigationItem.title = @"Delivery";
        _cityTF.placeholder = @"Enter Your Address";
    }
    else
    {
        self.navigationItem.title = @"Pickup";
    }

//    if (_showSearchBar)
//    {
//        _tableView.frame = self.view.bounds;
//        _cityTF.hidden = YES;
//        [self configureNavigationBar];
//    }
}
- (void)updateTableInsets
{
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, _keyboardBottomInset, 0);
    _tableView.scrollIndicatorInsets = _tableView.contentInset;
    CGRect tableRect = _initialTableRect;
    tableRect.origin.y += _topInset;
    tableRect.size.height -= _topInset;
    [UIView animateWithDuration: 0.3f animations:^{
        _tableView.frame = tableRect;
    }];
}
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    _keyboardBottomInset = keyboardSize.height;
    [self updateTableInsets];
}
- (void)keyboardHidden
{
    _keyboardBottomInset = 0.0f;
    [self updateTableInsets];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"displayRestaurants"])
    {
        SearchRestaurantVC *searchVC = segue.destinationViewController;
        searchVC.restaurants = _searchResultsRestaurants;
        searchVC.addressString = selectedAddressString;
        searchVC.selectedLocation = _selectedCoordinate;
    }
}
#pragma mark - Search Bar
- (void)cancelButtonClicked
{
    [_searchBar resignFirstResponder];
    [UIView animateWithDuration: 0.3f animations:^{
        _searchView.frame = _initialSearchRect;
    }completion:^(BOOL finished) {
        [self back];
    }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
#pragma mark - Text Field
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if (result.length >= 2)
    {
        if (textField == _cityTF)
        {
            _streetTF.text = nil;
            [_autocompleteTimer invalidate];
            self.autocompleteTimer = nil;
            self.autocompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(showAutocomplete) userInfo:nil repeats:NO];
        }
    }
    else
    {
        if (textField == _cityTF)
        {
            _streetTF.text = nil;
        }
        _searchResults = nil;
        [_tableView reloadData];
    }
    
    return YES;
}

- (void)showAutocomplete
{
    [_cityActivityIndicator startAnimating];
    [DataManager googleAutocompleteWithText: _cityTF.text completion:^(id result, NSString *error) {
            [_cityActivityIndicator stopAnimating];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            _searchResults = result;
            [_tableView reloadData];
            _noResLabel.hidden = _searchResults.count > 0;
        }
    }];

}
- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    _activeTF = textField;
    _topInset = 0.0f;
    [self updateTableInsets];
    textField.filled = NO;
    
    if (textField == _cityTF)
    {
        [_scrollView setContentOffset: CGPointZero animated: YES];
        [_tableView reloadData];
    }
    [UIView animateWithDuration: 0.2f animations:^{
        _streetTF.alpha = textField == _cityTF ? 0.0f : 1.0f;
    }];
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    [self updateFilledState: textField];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)updateFilledState:(AppTextField*)tf
{
    tf.filled = tf.text.length > 0;
    
}
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _searchResults.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id searchItem = _searchResults[indexPath.section];
    if ([searchItem isKindOfClass: [Restaurant class]])
        return 90;
    return 50;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SimpleCell";
    UITableViewCell *cell = nil;
    id searchItem = _searchResults[indexPath.section];
    
    if ([searchItem isKindOfClass: [Restaurant class]])
    {
        RestaurantCell *restaurantCell = (RestaurantCell*)[tableView dequeueReusableCellWithIdentifier: @"RestaurantCell"];
        restaurantCell.restaurant = searchItem;
        cell = restaurantCell;
    }
    else
        cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.textColor = [Utils appPrimaryColor];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName: @"ArialMT" size: 17];
        UIImageView *background = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"address_list_bg"]];
        background.frame = cell.bounds;
        background.width = [UIScreen mainScreen].bounds.size.width;
        background.backgroundColor = [Utils appSecondaryColor];
        [cell.contentView insertSubview: background atIndex: 0];
       // [background sizeToFit];
        UIImageView *accessoryView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"icon_next"]];
        [accessoryView sizeToFit];
        cell.accessoryView = accessoryView;
    }
    
    if ([searchItem isKindOfClass: [SPGooglePlacesAutocompletePlace class]])
    {
        SPGooglePlacesAutocompletePlace *place = searchItem;
        NSRange range = [[place.name lowercaseString] rangeOfString:[_cityTF.text lowercaseString]];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:place.name];
        [str addAttributes:@{NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: 17]} range:range];
        cell.textLabel.attributedText = str;
    }


    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
   
    selectedPlace = _searchResults[indexPath.section];
    [self searchRestaurants];
}

- (void)searchRestaurants
{

    __weak NewAddressVC *weakSelf = self;
    [self showLoadingOverlay];
    
    SPGooglePlacesPlaceDetailQuery *query = [[SPGooglePlacesPlaceDetailQuery alloc] initWithApiKey:selectedPlace.key];
    query.reference = selectedPlace.reference;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
        if (error) {
        } else {
            NSDictionary *locDic = placeDictionary[@"geometry"][@"location"];
            _selectedCoordinate = CLLocationCoordinate2DMake([locDic[@"lat"] floatValue], [locDic[@"lng"] floatValue]);
            CompletionBlock completionBlock = ^(NSArray* result, NSString *error) {
                [weakSelf removeLoadingOverlay];
                NewAddressVC *strongSelf = weakSelf;
                if (strongSelf)
                {
                    if (error)
                        [Utils showErrorMessage: error];
                    else
                    {
                        if (result.count == 0)
                        {
                            [Utils showErrorMessage: @"There are no restaurants"];
                            return;
                        }
                        strongSelf->_searchResultsRestaurants = result;
                        [strongSelf->_tableView reloadData];
                        if (strongSelf.navigationController.topViewController == strongSelf)
                        {
                            [strongSelf removeLoadingOverlay];
                            selectedAddressString = selectedPlace.name;
                            [strongSelf performSegueWithIdentifier: @"displayRestaurants" sender: strongSelf];
                        }
                    }
                }
            };
            if ([DataManager deliveryMethod] == DeliveryMethodDelivery) {
                [DataManager searchRestaurantsByLocation:_selectedCoordinate completion:completionBlock];
            }
            else
            {
                [DataManager searchRestaurantsByMap:_selectedCoordinate completion:completionBlock];
            }
            

        }
    }];


   
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2.0f;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame: CGRectZero];
}
@end
