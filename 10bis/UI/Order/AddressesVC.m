//
//  AddressesVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "AddressesVC.h"
#import "DataManager.h"
#import "Address.h"
#import "AddressCell.h"
#import "NewAddressVC.h"
#import "SearchRestaurantVC.h"
#import "ECSlidingViewController.h"
@interface AddressesVC () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end

@implementation AddressesVC
{
    NSArray *_addresses;
    __weak IBOutlet UITableView *_tableView;
    IBOutlet UIView *_footerView;
    
    __weak IBOutlet AppTextField *_newAddressTF;
    __weak IBOutlet AppTextField *_fakeTF;
    NSArray *_restaurants;
    __weak IBOutlet UILabel *_addressLabel;
    __weak Address *_selectedAddress;
    CGRect _tfPosition;
    __weak IBOutlet UIView *_orView;
    __weak IBOutlet UIView *_or2View;
    __weak IBOutlet AppRedButton *_currentLocationButton;
    BOOL _isSearchingAddresses;
    
    BOOL _isAlertShowed;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder: aDecoder])
    {
        self.title = [Utils screenTitleForDelivery: [DataManager deliveryMethod]];
        if ([DataManager currentUser])
        {
            _isSearchingAddresses = YES;
            __weak AddressesVC *weakSelf = self;
            [DataManager getAddressesByUser: [DataManager currentUser] completion:^(id result, NSString *error) {
                AddressesVC *strongSelf = weakSelf;
                if (strongSelf)
                {
                    strongSelf->_isSearchingAddresses = NO;
                    [strongSelf removeLoadingOverlay];
                    strongSelf->_tableView.hidden = NO;
                    strongSelf->_footerView.hidden = NO;
                    strongSelf->_addressLabel.hidden = NO;
                    if (error)
                    {
                        [Utils showErrorMessage: error];
                        [strongSelf noAddressesDisplay];
                    }
                    else
                    {
                        strongSelf->_addresses = result;
                        if (strongSelf->_addresses.count == 0)
                        {
                            [strongSelf noAddressesDisplay];
                        }
                        [strongSelf->_tableView reloadData];
                    }
                }
            }];
        }
    }
    
    return self;
}
- (void)noAddressesDisplay
{
    _newAddressTF.placeholder = @"Enter Your Address";
    _fakeTF.placeholder = @"Enter Your Address";

//    CGRect footerRect = _footerView.frame;
//    footerRect.size.height -= 22.0f;
//    _footerView.frame = footerRect;

    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodDelivery)
    {
       // _orView.hidden = YES;
       /* double delayInSeconds = 0.3f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if (_isAlertShowed == NO)
                [_newAddressTF becomeFirstResponder];
        });*/
    }
    else
    {
        _or2View.hidden = YES;
    }
    _tableView.scrollEnabled = NO;
    _addressLabel.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _addressLabel.textColor = [Utils appPrimaryColor];
    if (_isSearchingAddresses)
        [self showLoadingOverlay];
    
    [_tableView registerNib: [UINib nibWithNibName: @"AddressCell" bundle: nil] forCellReuseIdentifier: @"AddressCell"];
  //  _tableView.tableFooterView = _footerView;
    _or2View.hidden = !_addresses;

    if (_addresses)
    {
        _tableView.hidden = NO;
        _addressLabel.hidden = NO;
    }
    else if ([DataManager currentUser] == nil)
    {
        [self noAddressesDisplay];
        _footerView.hidden = NO;
        _tableView.hidden = NO;
    }
    
    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodDelivery)
    {
       // _or2View.hidden = _currentLocationButton.hidden = YES;
//        CGRect footerRect = _footerView.frame;
//        footerRect.size.height -= 99.0f;
//        _footerView.frame = footerRect;
        self.navigationItem.title = @"Delivery";

    }
    else
    {
        self.navigationItem.title = @"Pickup";

    }
    
  //  static NSString * const kQuickOrderPopupShowed = @"kQuickOrderPopupShowed";
    //BOOL popupShowed = [[NSUserDefaults standardUserDefaults] boolForKey: kQuickOrderPopupShowed];
   /* if (_isNewUser && popupShowed == NO)
    {
        _isAlertShowed = YES;
        [[NSUserDefaults standardUserDefaults] setBool: YES forKey: kQuickOrderPopupShowed];
        [self showCustomAlert: @"New Here?" msg: @"empty" button: @"Cool, Got it!" alignment: NSTextAlignmentCenter completion:^{
                  }];
    }*/
    if (_orView.hidden) // no addresses
    {
      //  [_newAddressTF becomeFirstResponder];
    }

    
    if(![[DataManager sharedManager] currentLocation])
    {
        [Utils showErrorMessage: @"No user location"];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"restaurantsSegue"])
    {
        SearchRestaurantVC *searchVC = segue.destinationViewController;
        searchVC.restaurants = _restaurants;
        searchVC.address = _selectedAddress;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"currentLocationSegue"] && ![[DataManager sharedManager] currentLocation])
    {
        [Utils showErrorMessage: @"No user location"];

        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _tfPosition = [textField.superview convertRect: textField.frame toView: self.view];
    _fakeTF.frame = _tfPosition;
    _fakeTF.hidden = NO;
    textField.hidden = YES;
    [UIView animateWithDuration: 0.3f animations:^{
        _tableView.alpha = 0.0f;
        _addressLabel.alpha = 0.0f;
        CGRect fakeRect = _fakeTF.frame;
        fakeRect.origin.y = 42.0f;
        _fakeTF.frame = fakeRect;
    } completion:^(BOOL finished) {
        NewAddressVC *newAddressVC = [self.storyboard instantiateViewControllerWithIdentifier: @"NewAddressVC"];
        newAddressVC.popToRoot = _orView.hidden;
        [self.navigationController pushViewController: newAddressVC animated: NO];
    }];
    return NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if (_tableView.alpha == 0.0f)
    {
        _fakeTF.frame = _tfPosition;
        _fakeTF.hidden = YES;
        _newAddressTF.hidden = NO;
        [UIView animateWithDuration: 0.5f animations:^{
            _tableView.alpha = 1.0f;
            _addressLabel.alpha = 1.0f;
            _addressLabel.hidden = NO;
        } completion:^(BOOL finished) {
            _fakeTF.hidden = YES;
            _newAddressTF.hidden = NO;
        }];
    }
}
#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _addresses.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Address *address = _addresses[indexPath.row];
    return [AddressCell heightForAddress: address];
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier: @"AddressCell"];
    Address *address = _addresses[indexPath.row];
    cell.address = address;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    Address *address = _addresses[indexPath.row];
    [self showLoadingOverlay];
    __weak AddressesVC *weakSelf = self;
    CompletionBlock block = ^(NSArray *result, NSString *error) {
        AddressesVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            [strongSelf removeLoadingOverlay];
            
            if (error)
                [Utils showErrorMessage: error];
            else
            {
                if (result.count == 0)
                {
                    [Utils showErrorMessage: @"There are no restaurants"];
                    return;
                }
                
                strongSelf->_restaurants = result;
                strongSelf->_selectedAddress = address;
                if (strongSelf.navigationController && strongSelf.navigationController.topViewController == strongSelf)
                {
                    [strongSelf performSegueWithIdentifier: @"restaurantsSegue" sender: strongSelf];
                }
                
            }
        }
    };
    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodDelivery)
    {
        [DataManager searchRestaurantsWithAddress: address completion:block];
    }
    else
    {
        [DataManager searchRestaurantsByMap:CLLocationCoordinate2DMake(address.latitude, address.longitude) completion:block];
    }
    
}
@end
