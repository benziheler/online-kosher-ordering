//
//  StarsView.m
//  10bis
//
//  Created by Vadim Pavlov on 28.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "StarsView.h"

@implementation StarsView
{
    NSMutableArray *_starsViews;
}
- (void)awakeFromNib
{
    _starsViews = [NSMutableArray arrayWithCapacity: 5];
    for (int i = 0; i < 5; i++)
    {
        UIImageView *starView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"rate_star_grey"]];
        starView.highlightedImage = [UIImage imageNamed: @"rate_star_blue"];
        [starView sizeToFit];
        [self addSubview: starView];
        [_starsViews addObject:starView];
        CGRect starRect = starView.frame;
        CGFloat offset = 4.0f;
        starRect.origin.x = (CGRectGetWidth(starRect) + offset) * i;
        starView.frame = starRect;
    }
    
    UIImageView *halfStarView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"rate_star_half_blue"]];
    [halfStarView sizeToFit];
    [self addSubview: halfStarView];
    [_starsViews addObject: halfStarView];
    
}
- (void)setStars:(NSInteger)stars
{
    UIImageView *halfStarView = [_starsViews lastObject];
    halfStarView.hidden = YES;
    for (int i = 1; i < 6; i++)
    {
        UIImageView *starView = _starsViews[i-1];
        if (i * 2 <= stars)
        {
            starView.highlighted = YES;
        }
        else if (i * 2 - 1 == stars)
        {
            starView.highlighted = NO;
            halfStarView.hidden = NO;
            halfStarView.frame = starView.frame;
        }
        else
            starView.highlighted = NO;
    }
}
@end
