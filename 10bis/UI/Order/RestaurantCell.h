//
//  RestaurantCell.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Restaurant;
@interface RestaurantCell : UITableViewCell
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, copy) void (^showKosherDetails)(Restaurant *restaurant);
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIImageView *distIcon;
@property (weak, nonatomic) IBOutlet UILabel *distLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (weak, nonatomic) IBOutlet UIView *phoneOnlyView;
@property (weak, nonatomic) IBOutlet UILabel *phoneOnlyLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButon;
- (IBAction)callButtonPressed:(id)sender;
@end
