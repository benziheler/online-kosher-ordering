//
//  RestaurantCell.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantCell.h"
#import "Restaurant.h"
#import "DataManager.h"
#import "CustomAlert.h"
#import "StarsView.h"

@implementation RestaurantCell
{
    __weak IBOutlet UILabel *_nameLabel;
    __weak IBOutlet UILabel *_addressLabel;
    __weak IBOutlet UILabel *_reviewsLabel;
    __weak IBOutlet UIImageView *_dlvUnavailableImgView;
    __weak IBOutlet UILabel *_deliveryUnavailableLabel;
    __weak IBOutlet UIImageView *_logoImageView;
    __weak IBOutlet UILabel *_discountLabel;
    __weak IBOutlet UIButton *_discountRibbonButton;
    __weak IBOutlet StarsView *_starsView;
    __weak IBOutlet UIButton *_categoryButton;
    CGRect _initialCouponRect;
    CGRect _initialCusineRect;
    __weak IBOutlet UILabel *deliveryLabel;
    __weak IBOutlet UIButton *kosherDetailsButton;
    
}
- (void)awakeFromNib
{
    if([[DataManager appDomain] isEqualToString:@"kosherordering"])
    {
        kosherDetailsButton.hidden = NO;
        _phoneOnlyLabel.textColor = [Utils appPrimaryColor];
        [_callButon setTitleColor:[Utils appPrimaryColor] forState:UIControlStateNormal];
        deliveryLabel.textColor = [Utils appPrimaryColor];
        _discountLabel.textColor = [Utils appTextColor];
        _leftLabel.textColor = [Utils appPrimaryColor];
        _rightLabel.textColor = [Utils appPrimaryColor];
        _distLabel.textColor = [Utils appPrimaryColor];

    }
    else
    {
        kosherDetailsButton.hidden = YES;
    }
    
    _discountLabel.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-30));
    _logoImageView.layer.cornerRadius = 2.0f;
    _logoImageView.layer.masksToBounds = YES;
    _initialCusineRect = _categoryButton.frame;
    self.bgImageView.backgroundColor = [Utils appSecondaryColor];

    UIImage *bgImage = [_categoryButton backgroundImageForState: UIControlStateNormal];
    UIImage *strBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight:bgImage.size.height/2];
    [_categoryButton setBackgroundImage: strBgImage forState: UIControlStateNormal];
}
- (void)setRestaurant:(Restaurant *)restaurant
{
    deliveryLabel.text = @"";
    _addressLabel.hidden = NO;
   
    _distLabel.text = restaurant.distanceString;

    _arrowImageView.hidden = restaurant.phoneOrdersOnlyOnPortals;
    NSString *oldLogoURL = _restaurant.logoURL;
    _restaurant = restaurant;
    _nameLabel.text = restaurant.name;
    //orr
      NSMutableArray *contactArray = [NSMutableArray arrayWithCapacity: 2];
//    if (_restaurant.cityName)
//        [contactArray addObject: _restaurant.cityName];
    if (_restaurant.street)
        [contactArray addObject: _restaurant.street];
    _addressLabel.text = [contactArray componentsJoinedByString: @", "];
    
    // logo
    _logoImageView.image = [UIImage imageNamed: @"place_holder_140"];
    [Utils loadImage: restaurant.logoURL completion:^(UIImage *image) {
        if (_restaurant == restaurant && image)
        {
            _logoImageView.image = image;
            if ([oldLogoURL isEqualToString: restaurant.logoURL] == NO)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    _logoImageView.image = image;
                });
            }
        }
    }];

    [_starsView setStars: _restaurant.reviewsRank];
    
    _deliveryUnavailableLabel.hidden = YES;
    _dlvUnavailableImgView.hidden = YES;
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    DeliveryMethod method = [DataManager deliveryMethod];
    switch (method) {
        case DeliveryMethodDelivery:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            BOOL isDeliveryActive = restaurant.isActiveForDelivery;
            _deliveryUnavailableLabel.text = @"Delivery Unavailable";
            _deliveryUnavailableLabel.hidden = isDeliveryActive;
            _dlvUnavailableImgView.hidden = isDeliveryActive;

            break;
        case DeliveryMethodPickup:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            BOOL isPickupActive = restaurant.isActiveForPickup;
            _deliveryUnavailableLabel.hidden = isPickupActive;
            _dlvUnavailableImgView.hidden = isPickupActive;
            _deliveryUnavailableLabel.text = @"Pickup Unavailable";

            break;
        case DeliveryMethodSitting:
            if (restaurant.isHappyHourActive == NO ||
                [restaurant.happyHourDiscount isEqualToString: @"0%"])
            {
                couponHidden = YES;
            }
            couponText = restaurant.happyHourDiscount;
            break;
        default:
            break;
    }
    
    // discount
    _discountLabel.hidden = _discountRibbonButton.hidden = couponHidden;
    if ((method == DeliveryMethodSitting || restaurant.couponHasRestrictions) && couponText)
        couponText = [@"*" stringByAppendingString: couponText];
    
    _discountLabel.text = couponText;

    // hours or delivery fee
    if (method == DeliveryMethodDelivery)
    {
        _leftLabel.hidden = NO;
        _leftIcon.hidden = NO;
        _leftLabel.text = restaurant.phone;
        _leftIcon.image = [UIImage imageNamed:@"icn_phone"];
        _leftIcon.hidden = NO;
        _rightLabel.hidden = YES;
        _rightIcon.hidden = YES;
        _distLabel.hidden = YES;
        _distIcon.hidden = YES;
        NSString *minOrder = nil;
        NSString *boldMinOrder = nil;
        NSString *deliveryPrice = nil;
        NSString *boldDeliveryPrice = nil;
        if (restaurant.minimumPriceForOrder == 0)
        {
            minOrder = @"No minimum order";
            boldMinOrder = minOrder;
        }
        else
        {
            minOrder = [NSString stringWithFormat: @"Minimum order: $%ld" , (long)restaurant.minimumPriceForOrder];
            boldMinOrder = [NSString stringWithFormat: @"$%ld", (long)restaurant.minimumPriceForOrder];
        }
        
        if (restaurant.deliveryPriceForOrder == 0)
        {
            deliveryPrice = @"Free Delivery";
            boldDeliveryPrice = deliveryPrice;
        }
        else
        {
            deliveryPrice = [NSString stringWithFormat: @"Delivery %@", restaurant.deliveryPrice];
            boldDeliveryPrice = [NSString stringWithFormat: @"%@", restaurant.deliveryPrice];
        }
        NSString *fullString = [NSString stringWithFormat: @"%@ | %@", minOrder, deliveryPrice];
        NSMutableAttributedString *attrOrder = [[NSMutableAttributedString alloc] initWithString: fullString];
        
        [Utils addBoldAttributeToString: attrOrder forText: boldMinOrder font: deliveryLabel.font.pointSize];
        [Utils addBoldAttributeToString: attrOrder forText: boldDeliveryPrice font: deliveryLabel.font.pointSize];
        deliveryLabel.attributedText = attrOrder;
        deliveryLabel.hidden = NO;
    }
    else if (method == DeliveryMethodPickup)
    {
        _rightLabel.text = restaurant.phone;
        _leftLabel.text = restaurant.pickupActivityHours;
        _leftIcon.image = [UIImage imageNamed:@"icn_hours"];
        _rightIcon.image = [UIImage imageNamed:@"icn_phone"];
        
        _leftLabel.hidden = NO;
        _leftIcon.hidden = NO;
        _rightLabel.hidden = NO;
        _rightIcon.hidden = NO;
        _distLabel.hidden = NO;
        _distIcon.hidden = NO;
    }
    
    // reviews
    if(_restaurant.numberOfReviews > 0)
    {
        _reviewsLabel.hidden = NO;
        _starsView.hidden = NO;
    NSString *reviewsText = [NSString stringWithFormat: @"%ld Reviews", (long)_restaurant.numberOfReviews];
    NSMutableAttributedString *attrReviewsText = [[NSMutableAttributedString alloc] initWithString: reviewsText];
    
    [Utils addBoldAttributeToString: attrReviewsText forText: [NSString stringWithFormat: @"%ld", (long)_restaurant.numberOfReviews] font: _reviewsLabel.font.pointSize];
    _reviewsLabel.attributedText = attrReviewsText;
    
    [_reviewsLabel sizeToFit];
    CGRect reviewRect = _reviewsLabel.frame;
    CGRect starsRect = _starsView.frame;
    
    starsRect.origin.x = _addressLabel.frame.origin.x;
    _starsView.frame = starsRect;
    reviewRect.origin.x = CGRectGetMaxX(starsRect) + 10;
    reviewRect.size.height = starsRect.size.height;
    reviewRect.origin.y = starsRect.origin.y - 1.0f;
    _reviewsLabel.frame = reviewRect;
    }
    else
    {
        _reviewsLabel.hidden = YES;
        _starsView.hidden = YES;
    }
  

    _categoryButton.hidden = _restaurant.cuisineList.length == 0;
    [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
    _categoryButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    _categoryButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
    _phoneOnlyView.hidden = YES;
    _phoneOnlyLabel.hidden = YES;
    if(_restaurant.phoneOrdersOnlyOnPortals )
    {
        _phoneOnlyLabel.hidden = NO;
        _phoneOnlyView.hidden = NO;
        deliveryLabel.hidden = YES;
        _addressLabel.hidden = YES;
        _rightLabel.hidden = YES;
        _rightIcon.hidden = YES;
        _distLabel.hidden = YES;
        _distIcon.hidden = YES;
        _leftIcon.hidden = YES;
        _leftLabel.hidden = YES;
    }

    
}
- (IBAction)kosherDetailsPressed:(id)sender
{
    if(_showKosherDetails)
    {
        self.showKosherDetails(_restaurant);
    }
}

- (IBAction)showAlert:(id)sender
{
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [CustomAlert showCustomAlertWithURL: url button: @"Close"];
}
- (IBAction)showCusieneList:(UIButton*)sender
{
    CGRect buttonRect = _categoryButton.frame;
    CGFloat width = CGRectGetWidth(self.frame) - buttonRect.origin.x*2;
    if(kosherDetailsButton.hidden == NO)
    {
        width -= kosherDetailsButton.width +2;
    }

     buttonRect.size.width = width;
    [_categoryButton setTitle: @"  " forState: UIControlStateNormal];
    [UIView animateWithDuration: 0.2f animations:^{
        if (sender.selected)
        {
            _categoryButton.frame = _initialCusineRect;
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
        }
        else
        {
            _categoryButton.frame = buttonRect;
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];

        }
        sender.selected = !sender.selected;
    }];
    
    if(sender.selected)
    {
        [Utils handleScreenTouch:^{
            [self showCusieneList:sender];
        }];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
}
- (IBAction)callButtonPressed:(id)sender {
    [Utils callToNumber:_restaurant.phone];

}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
