//
//  StarsView.h
//  10bis
//
//  Created by Vadim Pavlov on 28.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarsView : UIView
- (void)setStars:(NSInteger)stars;
@end
