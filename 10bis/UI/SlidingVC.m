//
//  SlidingVC.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "SlidingVC.h"
#import "MenuVC.h"
@interface SlidingVC ()

@end

@implementation SlidingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIStoryboard* storyboard = self.storyboard;
    UINavigationController *navigationVC = [storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
    [navigationVC.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];

    self.topViewController = navigationVC;
    
    if ([Utils isIOS7])
    {
        UIView *stripView = [[UIView alloc] initWithFrame: CGRectMake(0, -20, DEVICE_WIDTH, 20)];
        stripView.backgroundColor = [Utils appPrimaryColor];
        [navigationVC.navigationBar addSubview: stripView];
    }
    
    MenuVC *menuVC = [storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    self.underLeftViewController = menuVC;
    
    [self setAnchorRightRevealAmount:DEVICE_WIDTH - 50];
}
@end
