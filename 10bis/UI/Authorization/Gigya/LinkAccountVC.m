//
//  LinkAccountVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "LinkAccountVC.h"
#import "DataManager.h"
#import <GigyaSDK/GSUser.h>
#import "ForgotPasswordVC.h"
#import "CartVC.h"
#import "CreateAccountVC.h"

@interface LinkAccountVC () <UITextFieldDelegate>

@end

@implementation LinkAccountVC
{
    __weak IBOutlet AppTextField *_emailTF;
    __weak IBOutlet AppTextField *_passwordTF;
    
    __weak IBOutlet UILabel *_headerLabel;
    __weak IBOutlet UIView *_socialHeader;
    __weak IBOutlet UIImageView *_socialIcon;
    __weak IBOutlet UILabel *_loginLabel;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[DataManager appDomain] isEqualToString:@"kosherordering"])
    {
        _headerLabel.text = @"Enter the details of your kosher ordering to connect your accounts";
        
    }
    else
    {
        _headerLabel.text = @"Enter the details of your online ordering to connect your accounts";
    }
    
    if (_isNewCustomer)
    {
        NSString *newTitle = [NSString stringWithFormat:@"New to %@?",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
        self.title = newTitle;
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Already have an account? Click Here"];
        [str addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} range:NSMakeRange (0, 25)];
        [str addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} range:NSMakeRange (25, 10)];
        _loginLabel.attributedText = str;
        
    }
    else
    {
        NSString *newTitle = [NSString stringWithFormat:@"New to %@? Click Here",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
        self.title = @"Already a Member?";
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:newTitle];
        [str addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} range:NSMakeRange (0, 24)];
        [str addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} range:NSMakeRange (24, 10)];
        _loginLabel.attributedText = str;
        
    }
    _loginLabel.textColor = [Utils appPrimaryColor];
    
    _socialHeader.backgroundColor = [Utils colorForSocialNetwork: _socialNetwork];
    _socialIcon.image = [Utils iconForSocialNetwork: _socialNetwork];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"ForgotPasswordSegue"])
    {
        ForgotPasswordVC *vc = segue.destinationViewController;
        vc.isNewCustomer = _isNewCustomer;
    }
    else if ([segue.identifier isEqualToString:@"CreateAccountSegue"])
    {
        CreateAccountVC *accountVC = segue.destinationViewController;
        accountVC.socialUser = _socialUser;
        accountVC.socialNetwork = _socialNetwork;
        accountVC.isNewCustomer = YES;
    }
    
}
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}
- (IBAction)enter:(id)sender
{
    NSString *email = _emailTF.text;
    NSString *password = _passwordTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    else if (password.length == 0)
    {
        [self showValidationAlertFor: _passwordTF];
        return;
    }
    [self showLoadingOverlay];
    __weak LinkAccountVC *weakSelf = self;
    [DataManager loginUser: email withPassword: password socialUID: _socialUser.UID encryptedId:nil completion:^(id result, NSString *error) {
        [self removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            UIViewController *rootVC = weakSelf.navigationController.viewControllers[0];
            DeliveryMethod method = [DataManager deliveryMethod];
            if (method == DeliveryMethodNone)
            {
                [weakSelf.navigationController popToRootViewControllerAnimated: YES];
            }
            else
            {
                if ([DataManager isLoginForCheckout])
                {
                    for (UIViewController *vc in weakSelf.navigationController.viewControllers)
                    {
                        if ([vc isKindOfClass: [CartVC class]])
                        {
                            [weakSelf.navigationController popToViewController: vc animated: NO];
                            [(CartVC*)vc continueOrder: nil];
                            return;
                        }
                    }
                }
                
                [weakSelf.navigationController popToRootViewControllerAnimated: NO];
                [rootVC performSegueWithIdentifier: @"addressSegue" sender: weakSelf];
            }
        }
    }];
}

#pragma mark - Text Fields
- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    textField.filled = textField.text.length > 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTF)
    {
        [_passwordTF becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}
@end
