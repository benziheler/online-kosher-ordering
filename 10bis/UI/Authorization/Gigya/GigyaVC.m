//
//  GigyaVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "GigyaVC.h"
#import <GigyaSDK/Gigya.h>
#import "NSArray+Sort.h"
#import "DataManager.h"
#import "CreateAccountVC.h"
#import "LinkAccountVC.h"
#import "AddressesVC.h"
#import "CartVC.h"
@interface GigyaVC ()

@end

@implementation GigyaVC
{
    NSArray *_socialNetworks;
    IBOutletCollection(UIButton) NSArray *_networkButtons;
    NSArray *_sortedButtons;
    GSUser *_loadedUser;
    __weak IBOutlet UILabel *_textLabel;
    __weak IBOutlet UIButton *_fbButton;
    
    __weak IBOutlet UIButton *_googleButton;
    __weak IBOutlet UIButton *_quickOrderButton;
    __weak IBOutlet UIButton *_loginButton;
    __weak NSString *_selectedNetwork;
    IBOutletCollection(UIImageView) NSArray *_loginIcons;
    BOOL _isLogining;
    __weak IBOutlet UIView *_orView;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    _sortedButtons = [_networkButtons sortByUIViewOriginY];
  
    _socialNetworks = @[@"facebook", @"twitter", @"linkedin", @"google"];
    
    /*
     NSArray *loggedNetworks = [DataManager loginedSocialNetworks];
     for (NSString *network in loggedNetworks)
     {
     NSInteger buttonIndex = [_socialNetworks indexOfObject: network];
     UIButton *button = _sortedButtons[buttonIndex];
     button.enabled = NO;
     }
     */
    _textLabel.text = @"Sign in using your social account";
    _loginButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _loginButton.titleLabel.minimumScaleFactor = 0.1f;
     NSString *newTitle = [NSString stringWithFormat:@"New to %@?",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
    if (_isNewCustomer)
    {
        _loginButton.hidden = YES;
        self.title = newTitle;
        //[_quickOrderButton setTitle:@"Register later" forState:UIControlStateNormal];
        _quickOrderButton.hidden = YES;
            _orView.hidden = YES;
    }
    else
    {
        _orView.hidden = NO;
        _quickOrderButton.hidden = YES;
        _loginButton.hidden = NO;

        self.title = @"Already a Member?";
        UIImage *image = [UIImage imageNamed: @"icon_login2"];
        [_loginIcons setValue: image  forKey: @"image"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserverForName: UIApplicationWillEnterForegroundNotification object: Nil queue: nil usingBlock:^(NSNotification *note) {
        if (_isLogining)
        {
            _isLogining = NO;
            [self removeLoadingOverlay];
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if (_loadedUser)
    {
        [DataManager userLogout];
        [Gigya logout];
        _loadedUser = nil;
    }
}

#pragma mark - Actions
- (IBAction)quickOrder
{
    [DataManager setDeliveryMethod: DeliveryMethodDelivery]; // coz can achive this screen from menu
    [DataManager setQuickOrder: YES];
    
    UINavigationController *naviVC = self.navigationController;
    [naviVC popToRootViewControllerAnimated: NO];
    AddressesVC *addressesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"AddressesVC"];
    addressesVC.isNewUser = YES;
    [naviVC pushViewController: addressesVC animated: YES];
}
- (IBAction)loginSocial:(UIButton*)sender
{
    if (_isLogining)
        return;
    
    _isLogining = YES;
    [self showLoadingOverlay];
    NSInteger socialIndex = [_sortedButtons indexOfObject: sender];
    _selectedNetwork = _socialNetworks[socialIndex];
    GSSession *session =  [Gigya session];
    
    __weak GigyaVC *weakSelf = self;
    if ([session isValid]) // situation when user login to social network but not created account in 10bis
    {
        [DataManager userLogout];
        [Gigya logoutWithCompletionHandler:^(GSResponse *response, NSError *error) {
            GigyaVC *strongSelf = weakSelf;
            if (strongSelf)
            {
                [weakSelf removeLoadingOverlay];
                strongSelf->_isLogining = NO;
                if (error)
                {
                    [Utils showErrorAlert: error];
                }
                else
                {
                    [strongSelf loginSocial: sender];
                }
            }
            
        }];
        return;
    }
    {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[NSNumber numberWithInt:FBSessionLoginBehaviorUseSystemAccountIfPresent] forKey:@"facebookLoginBehavior"];
        
        [Gigya loginToProvider:_selectedNetwork parameters:params over:self completionHandler:^(GSUser *user, NSError *error) {
            
            GigyaVC *strongSelf = weakSelf;
            if (strongSelf)
            {
                if (error)
                {
                    strongSelf->_isLogining = NO;
                    [strongSelf removeLoadingOverlay];
                    if (error.code != 200001)
                        [Utils showErrorAlert: error];
                }
                else
                {
                    [DataManager userLoginedToNetwork: strongSelf->_selectedNetwork];
                    [strongSelf socialLoginForUser: user];
                }
                
            }
        }];
        
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"createAccountSegue"])
    {
        CreateAccountVC *accountVC = segue.destinationViewController;
        accountVC.socialUser = _loadedUser;
        accountVC.socialNetwork = _selectedNetwork;
        accountVC.isNewCustomer = _isNewCustomer;
    }
    else if ([segue.identifier isEqualToString: @"linkAccountSegue"])
    {
        LinkAccountVC *linkAccountVC = segue.destinationViewController;
        linkAccountVC.socialUser = _loadedUser;
        linkAccountVC.socialNetwork = _selectedNetwork;
        linkAccountVC.isNewCustomer = _isNewCustomer;
    }
    
}
#pragma mark - Gigya
- (void)socialLoginForUser:(GSUser*)gUser
{
    __weak GigyaVC *weakSelf = self;
    if (_isLogining == NO)
    {
        _isLogining = YES;
        [self showLoadingOverlay];
    }
    [DataManager socialLoginWithUser: gUser completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
              GigyaVC *strongSelf = weakSelf;
             if (strongSelf)
             strongSelf->_isLogining = NO;
             
            if ([result isEqualToString: @"SignatureValid"]) // should register user
            {
                if(weakSelf.navigationController)
                {
                    [weakSelf setLoadedUser:gUser];
                    if (_isNewCustomer)
                        [weakSelf performSegueWithIdentifier: @"createAccountSegue" sender: weakSelf];
                    else
                        [weakSelf performSegueWithIdentifier: @"linkAccountSegue" sender: weakSelf];
                }
            }
            else if (([result isEqualToString: @"LoginSuccess"] ||
                      [result isEqualToString: @"ConnectionSuccess"]) && weakSelf.navigationController) // user logined
            {
                UIViewController *rootVC = weakSelf.navigationController.viewControllers[0];
                DeliveryMethod method = [DataManager deliveryMethod];
                if (method == DeliveryMethodNone)
                {
                    [weakSelf.navigationController popToRootViewControllerAnimated: YES];
                }
                else
                {
                    if ([DataManager isLoginForCheckout])
                    {
                        for (UIViewController *vc in weakSelf.navigationController.viewControllers)
                        {
                            if ([vc isKindOfClass: [CartVC class]])
                            {
                                [weakSelf.navigationController popToViewController: vc animated: NO];
                                [(CartVC*)vc continueOrder: nil];
                                return ;
                            }
                        }
                    }
                    
                    [weakSelf.navigationController popToRootViewControllerAnimated: NO];
                    [rootVC performSegueWithIdentifier: @"addressSegue" sender: self];
                }
            }
            else
            {
                [DataManager userLogout];
                [Gigya logout];
                [Utils showErrorMessage: @"Connection error"];
            }
            
            /*
             else if ([result isEqualToString: @"\"ConnectionSuccess\""])
             {
             // Happens if isSiteUser=false but the user is signed in to 10bis (logged in previously). This scenario is when the user desides to connect his existing 10bis account with a social account, probably from interface 8.
             
             }
             else if ([result isEqualToString: @"\"SignatureInvalid\""])
             {
             // Data sent was inconsistent- error.
             }
             */
        }
    }];
    
}

- (void)setLoadedUser:(GSUser *)user
{
    _loadedUser = user;
}

@end
