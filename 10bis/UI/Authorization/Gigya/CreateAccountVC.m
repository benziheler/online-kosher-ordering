//
//  CreateAccountVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "CreateAccountVC.h"
#import <GigyaSDK/GSUser.h>
#import "NSArray+Sort.h"
#import "DataManager.h"
#import "LinkAccountVC.h"
#import "User.h"
#import "CheckButton.h"
#import "AddressesVC.h"
#import "CartVC.h"
#import "GigyaVC.h"

@interface CreateAccountVC () <UITextFieldDelegate>

@end

@implementation CreateAccountVC
{
    __weak IBOutlet AppTextField *_emailTF;
    __weak IBOutlet AppTextField *_firstNameTF;
    __weak IBOutlet AppTextField *_lastNameTF;
    __weak IBOutlet AppTextField *_phoneTF;
    __weak IBOutlet UILabel *agreeLabel;
    
    IBOutletCollection(UITextField) NSArray *_allTF;
    NSArray *_sortedTF;
    
    __weak IBOutlet CheckButton *_checkButton;
    __weak IBOutlet CheckButton *_checkButton2;
    
    __weak IBOutlet UIButton *_registerButton;
    
    __weak IBOutlet UILabel *_loginLabel;
    __weak IBOutlet UIView *_socialHeader;
    __weak IBOutlet UIImageView *_socialIcon;
    
    __weak IBOutlet UIScrollView *_scrollView;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     NSString *newTitle = [NSString stringWithFormat:@"New to %@?",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];
    if (_isNewCustomer)
    {
        self.title = newTitle;
    }
    else
    {
        self.title = @"Already a Member?";
    }
    _loginLabel.textColor = [Utils appPrimaryColor];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Already have an account? Click Here"];
    [str addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} range:NSMakeRange (0, 25)];
    [str addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14]} range:NSMakeRange (25, 10)];
    NSMutableAttributedString *agreeStr = [[NSMutableAttributedString alloc] initWithString:@"I read and agree to the Terms of Use and the Privacy Policy"];
     [agreeStr addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:13]} range:NSMakeRange (24, 12)];
     [agreeStr addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:13]} range:NSMakeRange (45, 14)];
    agreeLabel.attributedText = agreeStr;
    
    _loginLabel.attributedText = str;

    _sortedTF = [_allTF sortByUIViewOriginY];
    
    _checkButton.emptyImage = [UIImage imageNamed: @"checkbox"];
    _checkButton.filledImage = [UIImage imageNamed: @"checkbox_filled"];
    
    _checkButton2.emptyImage = [UIImage imageNamed: @"checkbox"];
    _checkButton2.filledImage = [UIImage imageNamed: @"checkbox_filled"];

    _checkButton.checked = YES;
    _checkButton2.checked = YES;
    
    _emailTF.text = _socialUser.email;
    _firstNameTF.text = _socialUser.firstName;
    _lastNameTF.text = _socialUser.lastName;
    
    if (_emailTF.text.length == 0)
        _emailTF.enabled = YES;
    if (_firstNameTF.text.length == 0)
        _firstNameTF.enabled = YES;
    if (_lastNameTF.text.length == 0)
        _lastNameTF.enabled = YES;
    
    [self updateFilledState: _emailTF];
    [self updateFilledState: _firstNameTF];
    [self updateFilledState: _lastNameTF];
    
    _socialHeader.backgroundColor = [Utils colorForSocialNetwork: _socialNetwork];
    _socialIcon.image = [Utils iconForSocialNetwork: _socialNetwork];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    _scrollView.contentSize = _scrollView.frame.size;
}
#pragma mark - Actions
- (IBAction)agree:(CheckButton*)sender
{
    if (_checkButton.checked)
        _registerButton.enabled = YES;
    else
        _registerButton.enabled = NO;

}
- (IBAction)agree2:(CheckButton*)sender
{
}

- (IBAction)registerAction:(id)sender
{
    NSString *email = _emailTF.text;
    NSString *firstName = _firstNameTF.text;
    NSString *lastName = _lastNameTF.text;
    NSString *phone = _phoneTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    else if (firstName.length == 0)
    {
        [self showValidationAlertFor: _firstNameTF];
        return;
    }
    else if (lastName.length == 0)
    {
        [self showValidationAlertFor: _lastNameTF];
        return;
    }
    
    User *user = [User new];
    user.email = email;
    user.firstName = firstName;
    user.lastName = lastName;
    user.phone = phone;
    
    [self showLoadingOverlay];
    __weak CreateAccountVC *weakSelf =  self;
    [DataManager registerUser: user socialUID: _socialUser.UID promotion: _checkButton2.checked password: nil completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            DeliveryMethod method = [DataManager deliveryMethod];
            if (method == DeliveryMethodNone)
            {
                [weakSelf.navigationController popToRootViewControllerAnimated: YES];
            }
            else
            {
                if ([DataManager isLoginForCheckout])
                {
                    for (UIViewController *vc in weakSelf.navigationController.viewControllers)
                    {
                        if ([vc isKindOfClass: [CartVC class]])
                        {
                            [weakSelf.navigationController popToViewController: vc animated: NO];
                            [(CartVC*)vc continueOrder: nil];
                            return ;
                        }
                    }
                }

                UINavigationController *naviVC = weakSelf.navigationController;
                [naviVC popToRootViewControllerAnimated: NO];
                AddressesVC *addressesVC = [weakSelf.storyboard instantiateViewControllerWithIdentifier: @"AddressesVC"];
                addressesVC.isNewUser = YES;
                [naviVC pushViewController: addressesVC animated: YES];
            }
        }
    }];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"linkAccountSegue"])
    {
        LinkAccountVC *linkAccountVC = segue.destinationViewController;
        linkAccountVC.socialUser = self.socialUser;
        linkAccountVC.socialNetwork = self.socialNetwork;
    }
    else if ([segue.identifier isEqualToString:@"showLoginScreen"])
    {
        GigyaVC *gigyaVC = segue.destinationViewController;
        gigyaVC.isNewCustomer = NO;
    }
    else if ([segue.identifier isEqualToString:@"AlreadyRegisteredSegue"])
    {
        LinkAccountVC *linkAccountVC = segue.destinationViewController;
        linkAccountVC.socialUser = self.socialUser;
        linkAccountVC.socialNetwork = self.socialNetwork;
    }
}

#pragma mark - Scroll View
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [aValue CGRectValue];
    CGRect convertedFrame = [self.view.window convertRect: keyboardFrame toView: self.view];
    CGRect intersect = CGRectIntersection(_scrollView.frame, convertedFrame);
    if (CGRectIsNull(intersect) == NO)
    {
        _scrollView.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height, 0);
        _scrollView.scrollIndicatorInsets = _scrollView.contentInset;
    }
}
- (void)keyboardHidden
{
    _scrollView.contentInset = UIEdgeInsetsZero;
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

#pragma mark - Text Field
- (void)updateFilledState:(AppTextField*)tf
{
    tf.filled = tf.text.length > 0;
}
- (BOOL)textField:(AppTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _phoneTF && string.length)
    {
        NSCharacterSet *numbers = [NSCharacterSet decimalDigitCharacterSet];
        BOOL member = [numbers characterIsMember: [string characterAtIndex: 0]];
        return member;
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    [self updateFilledState: textField];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType == UIReturnKeyNext)
    {
        NSInteger index = [_sortedTF indexOfObject: textField];
        UITextField *nextTF = _sortedTF[index+1];
        [nextTF becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}
@end
