//
//  LoginVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "LoginVC.h"
#import "DataManager.h"
#import "CartVC.h"
@interface LoginVC () <UITextFieldDelegate>

@end

@implementation LoginVC
{
    
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *_emailTF;
    __weak IBOutlet UITextField *_passwordTF;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    scrollView.contentSize = CGSizeMake(width, 500);
    
	// Do any additional setup after loading the view.
}

- (IBAction)login:(id)sender
{
    NSString *email = _emailTF.text;
    NSString *password = _passwordTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    else if (password.length == 0)
    {
        [self showValidationAlertFor: _passwordTF];
        return;
    }
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    
    [self showLoadingOverlay];
    __weak LoginVC *weakSelf = self;
    [DataManager loginUser: email withPassword: password socialUID: @"0" encryptedId:nil completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (error)
        {
            [Utils showErrorMessage: error];
        }
        else
        {
            UIViewController *rootVC = weakSelf.navigationController.viewControllers[0];
            DeliveryMethod method = [DataManager deliveryMethod];
            if (method == DeliveryMethodNone)
            {
                [weakSelf.navigationController popToRootViewControllerAnimated: YES];
            }
            else
            {
                if ([DataManager isLoginForCheckout])
                {
                    for (UIViewController *vc in weakSelf.navigationController.viewControllers)
                    {
                        if ([vc isKindOfClass: [CartVC class]])
                        {
                            [weakSelf.navigationController popToViewController: vc animated: NO];
                            [(CartVC*)vc continueOrder: nil];
                            return ;
                        }
                    }
                }
                [weakSelf.navigationController popToRootViewControllerAnimated: NO];
                [rootVC performSegueWithIdentifier: @"addressSegue" sender: weakSelf];
            }
        }
    }];
}

- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    textField.filled = textField.text.length > 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTF)
    {
        [_passwordTF becomeFirstResponder];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}

@end
