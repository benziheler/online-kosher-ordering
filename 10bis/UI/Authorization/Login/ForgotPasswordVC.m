//
//  ForgotPasswordVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "DataManager.h"
@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC
{
    __weak IBOutlet UITextField *_emailTF;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 NSString *newTitle = [NSString stringWithFormat:@"New to %@?",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]];    if (_isNewCustomer)
    {
        self.title = newTitle;
    }
    else
    {
        self.title = @"Already a Member?";
    }
}
- (IBAction)retrieve:(id)sender
{
    NSString *email = _emailTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    [self showLoadingOverlay];
    __weak ForgotPasswordVC *weakSelf = self;
    [DataManager resetPassword: email completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            [weakSelf.navigationController popToRootViewControllerAnimated: YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"Your request was received, an email with a new password will be sent to you as soon as possible" delegate: nil cancelButtonTitle: @"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }];
}

- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    textField.filled = textField.text.length > 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
