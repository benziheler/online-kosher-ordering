//
//  FilterView.h
//  10bis
//
//  Created by Anton Vilimets on 5/22/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kHasDiscountKey @"hasDiscount"


@interface FilterView : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger openedHeader;
}
@property (assign, nonatomic) BOOL isSortingEnabled;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryHeaderImageView;
@property (weak, nonatomic) IBOutlet UIImageView *pickupHeaderImageView;
@property (weak, nonatomic) IBOutlet UIButton *cuisineHaderButton;
@property (weak, nonatomic) IBOutlet UIImageView *deliveryHeaderArrow;
@property (weak, nonatomic) IBOutlet UIImageView *topHeaderArrow;
@property (weak, nonatomic) IBOutlet UIImageView *pickupHeaderArrow;
@property (weak, nonatomic) IBOutlet AppRedButton *doneButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *pickupHeaderButton;
@property (weak, nonatomic) IBOutlet UIView *topHeaderView;
@property (strong, nonatomic) IBOutlet UIView *deliverySortHeaderView;
@property (strong, nonatomic) IBOutlet UIView *pickupSortHeaderView;
@property (copy, nonatomic) void (^block)(NSDictionary *);
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *deliveryHeaderButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (void)showWithCompletion:(void (^)(NSDictionary *type))block;
- (void)setupWithDict:(NSDictionary *)dict;
+ (id)filterView;

- (IBAction)headerPerssed:(UIButton *)sender;
- (IBAction)donePressed:(id)sender;
- (IBAction)closePressed:(id)sender;

@end
