//
//  FilterCell.h
//  10bis
//
//  Created by Anton Vilimets on 5/22/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, FilterCellType)
{

    CellTypeRadioButton,
    CellTypeSwitch,
    CellTypeIcon
};

@interface FilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UISwitch *switchView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setupWithType:(FilterCellType)type title:(NSString *)title icon:(UIImage *)icon selected:(BOOL)selected;
- (BOOL)isSelected;

@end
