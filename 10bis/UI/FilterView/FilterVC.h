//
//  FilterViewController.h
//  10bis
//
//  Created by Anton Vilimets on 7/11/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterView.h"
#import "BaseVC.h"

@interface FilterVC : BaseVC

@property (strong, nonatomic) FilterView *filterView;

@end
