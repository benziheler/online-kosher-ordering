//
//  FilterCell.m
//  10bis
//
//  Created by Anton Vilimets on 5/22/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell
{
    FilterCellType _type;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setupWithType:(FilterCellType)type title:(NSString *)title icon:(UIImage *)icon selected:(BOOL)selected
{
    if([[DataManager appDomain] isEqualToString:@"kosherordering"])
    {
        self.titleLabel.textColor = [Utils appPrimaryColor];
        _switchView.onTintColor = [Utils appSecondaryColor];

    }
    else
    {
        self.titleLabel.textColor = [UIColor blackColor];
        _switchView.onTintColor = [Utils appPrimaryColor];

    }
    _type = type;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _iconImageView.image = icon;

    if(type == CellTypeRadioButton)
    {
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _switchView.hidden = YES;
        _radioButton.hidden = NO;
        _radioButton.selected = selected;
        _iconImageView.hidden = YES;


    }
    else if(type == CellTypeSwitch)
    {
        _iconImageView.hidden = NO;
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _switchView.hidden = NO;
        _radioButton.hidden = YES;
        [_switchView setOn:selected];

    }
    else if (type == CellTypeIcon)
    {
        _switchView.hidden = YES;
        _radioButton.hidden = YES;
        _iconImageView.hidden = NO;
        self.selectionStyle = UITableViewCellSelectionStyleGray;


    }
    _titleLabel.text = title;
    [self setSelected:selected];
}

- (BOOL)isSelected
{
    if(_type == CellTypeRadioButton)
    {
        return _radioButton.isSelected;
    }
    else if (_type == CellTypeSwitch)
    {
        return _switchView.isOn;
    }
    return NO;
}

- (void)setSelected:(BOOL)selected
{

 //   [super setSelected:selected animated:animated];
 //   [_switchView setOn:selected];
}

@end
