//
//  FilterViewController.m
//  10bis
//
//  Created by Anton Vilimets on 7/11/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "FilterVC.h"

@interface FilterVC ()

@end

@implementation FilterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.filterView = [FilterView filterView];
    [self.view addSubview:_filterView];
    _filterView.frame = self.view.bounds;
    _filterView.closeButton.hidden = YES;
    [_filterView.doneButton setTitle:@"Save" forState:UIControlStateNormal];
    _filterView.contentView.frame = _filterView.bounds;
    __weak FilterVC *weakSelf = self;
    [_filterView setBlock:^(NSDictionary *resultDict) {
        [[NSUserDefaults standardUserDefaults] setObject:resultDict forKey:[DataManager filterSettingKey]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [weakSelf.navigationController popViewControllerAnimated:YES];

    }];
    
    NSDictionary *savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    if(savedDict)
    {
        [_filterView setupWithDict:savedDict];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
