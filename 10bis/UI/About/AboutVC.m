//
//  AboutVC.m
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "AboutVC.h"
#import "ECSlidingViewController.h"
@interface AboutVC () <UIWebViewDelegate>

@end

@implementation AboutVC
{
    __weak IBOutlet UIWebView *_webView;
    __weak IBOutlet UIActivityIndicatorView *_indicator;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *url = [NSString stringWithFormat:@"%@/Info/AboutUs?isMobileUser=true",[DataManager baseUrl]];
    [_webView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_indicator stopAnimating];
    // Disable user selection
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    // Disable callout
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
}
@end
