//
//  AppDelegate.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomWindow.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@end
