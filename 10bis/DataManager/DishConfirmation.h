//
//  DishConfirmation.h
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DishConfirmation : NSObject
@property (nonatomic, strong) NSNumber *dishId;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *dishDesc;
@property (nonatomic, strong) NSString *dishNotes;

@property (nonatomic, strong) NSNumber *singleDishPrice;
@property (nonatomic, strong) NSNumber *totalDishPrice;
@property (nonatomic, strong) NSString *totalDishPriceString;

@property (nonatomic, assign) NSInteger orderIndex;
@property (nonatomic, assign) NSInteger shoppingCartIndex;

@property (nonatomic, strong) NSNumber *dishOwnerId;
@property (nonatomic, strong) NSNumber *dishUserId;
@property (nonatomic, strong) NSString *dishUsername;
@property (nonatomic, assign) NSInteger quantity;

@property (nonatomic, strong) NSArray *choices;

+ (instancetype)dishConfirmation:(NSDictionary*)data;
@end
