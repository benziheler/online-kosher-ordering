//
//  Address.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Address.h"

@implementation Address
+ (id)addressWithData:(NSDictionary*)addressData
{
    Address *address = [Address new];
    address.addressId = ValueOrNil(addressData, @"AddressId");
    address.addressNumber = ValueOrNil(addressData, @"AddressNumber");
    address.floor = ValueOrNil(addressData, @"Floor");
    address.enterance = ValueOrNil(addressData, @"Enterance");
    address.addressString = ValueOrNil(addressData, @"AddressString");
    address.addressStringWithBreak = ValueOrNil(addressData, @"AddressStringWithBreak");
    address.cityId = ValueOrNil(addressData, @"CityID");
    address.streetId = ValueOrNil(addressData, @"StreetID");
    address.companyId = ValueOrNil(addressData, @"AddressCompanyId");
    address.isCompanyAddress = [ValueOrNil(addressData, @"IsCompanyAddress") boolValue];
    address.restaurantListURL = ValueOrNil(addressData, @"RestaurantListByAddressURL");
    address.restaurantDeliversToAddress = [ValueOrNil(addressData, @"ResDeliversToAddress") boolValue];
    address.phone1 = ValueOrNil(addressData, @"Phone_1");
    address.phone2 = ValueOrNil(addressData, @"Phone_2");
    address.latitude = [ValueOrNil(addressData, @"AddressLat") doubleValue];
    address.longitude = [ValueOrNil(addressData, @"AddressLong") doubleValue];
    
    return address;
}
@end
