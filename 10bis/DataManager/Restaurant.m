//
//  Restaurant.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Restaurant.h"

@implementation Restaurant
+ (id)restaurantWithData:(NSDictionary*)restaurantData
{
    Restaurant *restaurant = [Restaurant new];
    restaurant.restaurantId = ValueOrNil(restaurantData, @"RestaurantId");
    restaurant.name = ValueOrNil(restaurantData, @"RestaurantName");
    restaurant.street = ValueOrNil(restaurantData, @"RestaurantAddress");
    restaurant.cityName = ValueOrNil(restaurantData, @"RestaurantCityName");
    restaurant.logoURL = ValueOrNil(restaurantData, @"RestaurantLogoUrl");
    restaurant.phone = ValueOrNil(restaurantData, @"RestaurantPhone");
    restaurant.cuisineList = ValueOrNil(restaurantData, @"RestaurantCuisineList");
    restaurant.numberOfReviews = [ValueOrNil(restaurantData, @"NumOfReviews") integerValue];
    restaurant.reviewsRank = [ValueOrNil(restaurantData, @"ReviewsRank") integerValue];
    restaurant.distanceFromUser = ValueOrNil(restaurantData, @"distanceFromUser");
    restaurant.distanceFromUserMeters = ValueOrNil(restaurantData, @"distanceFromUserInMeters");
    restaurant.isOpenForDelivery = [ValueOrNil(restaurantData, @"IsOpenForDelivery") boolValue];
    restaurant.isOpenForPickup = [ValueOrNil(restaurantData, @"IsOpenForPickup") boolValue];
    restaurant.minimumOrder = ValueOrNil(restaurantData, @"MinimumOrder");
    restaurant.minimumPriceForOrder = [ValueOrNil(restaurantData, @"MinimumPriceForOrder") integerValue];
    restaurant.deliveryPrice = ValueOrNil(restaurantData, @"DeliveryPrice");
    restaurant.deliveryPriceForOrder = [ValueOrNil(restaurantData, @"DeliveryPriceForOrder") integerValue];
    restaurant.isKosher = ValueOrNil(restaurantData, @"IsKosher");
    restaurant.kosher = ValueOrNil(restaurantData, @"RestaurantKosher");
    restaurant.deliveryRemarks = ValueOrNil(restaurantData, @"DeliveryRemarks");
    restaurant.latitude = [ValueOrNil(restaurantData, @"ResGeoLocation_lat") doubleValue];
    restaurant.longitude = [ValueOrNil(restaurantData, @"ResGeoLocation_lon") doubleValue];
    restaurant.isHappyHourActive = [ValueOrNil(restaurantData, @"IsHappyHourActive") boolValue];
    restaurant.phoneOrdersOnlyOnPortals = [ValueOrNil(restaurantData, @"PhoneOrdersOnlyOnPortals") boolValue];

//    restaurant.companyFlag = [ValueOrNil(restaurantData, @"CompanyFlag") boolValue];

    restaurant.happyHourDiscount = ValueOrNil(restaurantData, @"HappyHourDiscount");
    restaurant.happyHourDiscountValidity = ValueOrNil(restaurantData, @"HappyHourDiscountValidityString");
    restaurant.startOrderURL = ValueOrNil(restaurantData, @"StartOrderURL");
    restaurant.activityHours = ValueOrNil(restaurantData, @"ActivityHours");
    restaurant.pickupActivityHours = ValueOrNil(restaurantData, @"PickupActivityHours");
    restaurant.deliveryTime = ValueOrNil(restaurantData, @"DeliveryTime");
    restaurant.isPromotionActive = [ValueOrNil(restaurantData, @"IsPromotionActive") boolValue];
    restaurant.companyFlag = [ValueOrNil(restaurantData, @"CompanyFlag") boolValue];
    restaurant.isOverPoolMin = [ValueOrNil(restaurantData, @"IsOverPoolMin") boolValue];
    restaurant.poolSum = ValueOrNil(restaurantData, @"PoolSum");
    restaurant.deliveryEndTime = ValueOrNil(restaurantData, @"DeliveryEndTime");
    restaurant.isTerminalActive = [ValueOrNil(restaurantData, @"IsTerminalActive") boolValue];
    restaurant.isActiveForDelivery = [ValueOrNil(restaurantData, @"IsActiveForDelivery") boolValue];
    restaurant.isActiveForPickup = [ValueOrNil(restaurantData, @"IsActiveForPickup") boolValue];
    restaurant.bookmarked = [ValueOrNil(restaurantData, @"Bookmarked") boolValue];
    restaurant.discountCouponPercent = [ValueOrNil(restaurantData, @"DiscountCouponPercent") integerValue];
    restaurant.couponHasRestrictions = [ValueOrNil(restaurantData, @"CouponHasRestrictions") boolValue];
    restaurant.happyHourResRulesDescription = ValueOrNil(restaurantData, @"HappyHourResRulesDescription");
    [restaurant updateDistance];
    return restaurant;
}

- (void)updateDistance
{
    CLLocationCoordinate2D curLocation = [[DataManager sharedManager] currentLocation].coordinate;
    
    CLLocation *restaurantLocation = [[CLLocation alloc] initWithLatitude: self.latitude longitude: self.longitude];
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude: curLocation.latitude longitude: curLocation.longitude];
    
    CLLocationDistance distance = [userLocation distanceFromLocation: restaurantLocation];
    self.distanceFromUserMeters = [NSNumber numberWithDouble:distance];
}

- (NSString *)reversedPickupHours
{
    NSString *old = self.pickupActivityHours;
    NSArray *arr = [old componentsSeparatedByString:@"-"];
    if(arr.count == 2)
    {
        NSString *result = [NSString stringWithFormat:@"%@ - %@",arr[1], arr[0]];
        return result;
    }
    return @"";
}

- (NSString *)distanceString
{
    [self updateDistance];
    if(self.distanceFromUserMeters.doubleValue < 1000)
    {
        return [NSString stringWithFormat:@"%.2f meters away", self.distanceFromUserMeters.doubleValue];
    }
    else
    {
        return [NSString stringWithFormat:@"%.2f km away", self.distanceFromUserMeters.doubleValue/1000];
    }
    
}

@end
