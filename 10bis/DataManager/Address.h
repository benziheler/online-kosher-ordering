//
//  Address.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Address : NSObject
@property (nonatomic, strong) NSNumber *addressId;
@property (nonatomic, strong) NSNumber *addressNumber;
@property (nonatomic, strong) NSString *floor;
@property (nonatomic, strong) NSString *enterance;
@property (nonatomic, strong) NSString *addressString;
@property (nonatomic, strong) NSString *addressStringWithBreak;

@property (nonatomic, strong) NSNumber *cityId;
@property (nonatomic, strong) NSNumber *streetId;

@property (nonatomic, strong) NSNumber *companyId;
@property (nonatomic, assign) BOOL isCompanyAddress;

@property (nonatomic, strong) NSString *restaurantListURL;
@property (nonatomic, assign) BOOL restaurantDeliversToAddress;

@property (nonatomic, strong) NSString *phone1;
@property (nonatomic, strong) NSString *phone2;

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;

+ (id)addressWithData:(NSDictionary*)addressData;
@end
