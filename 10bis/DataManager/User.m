//
//  User.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "User.h"

@implementation User
- (NSDictionary*)userData
{
    NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithCapacity: 3];
    if (_userId)
        userData[@"userId"] = _userId;
    if (_firstName)
        userData[@"firstName"] = _firstName;
    if (_lastName)
        userData[@"lastName"] = _lastName;
    if (_email)
        userData[@"email"] = _email;
    if (_phone)
        userData[@"phone"] = _phone;
    if (_photoURL)
        userData[@"photoURL"] = _photoURL;
    return userData;
}
+ (id)userWithData:(NSDictionary*)userData
{
    User *user = [User new];
    user.userId = userData[@"userId"];
    user.firstName = userData[@"firstName"];
    user.lastName = userData[@"lastName"];
    user.email = userData[@"email"];
    user.phone = userData[@"phone"];
    user.photoURL = userData[@"photoURL"];
    return user;
}
@end
