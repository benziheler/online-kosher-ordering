//
//  McUser.m
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "McUser.h"

@implementation McUser
+ (instancetype)userFromData:(NSDictionary*)userData
{
    McUser *user = [McUser new];
    user.userId = ValueOrNil(userData, @"ID");
    user.selected = [ValueOrNil(userData, @"IsSelected") boolValue];
    user.name = ValueOrNil(userData, @"Name");
    return user;
}
@end
