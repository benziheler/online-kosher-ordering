//
//  DataManager.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "DataManager.h"
#import "CommManager.h"
#import <GigyaSDK/Gigya.h>

#import "User.h"
#import "Address.h"
#import "City.h"
#import "Street.h"
#import "Restaurant.h"
#import "Menu.h"
#import "URLDictionary.h"
#import "Dish.h"
#import "Order.h"
#import "Choice.h"
#import "McUser.h"
#import "NSObject+JSONDictionary.h"
#import "OrderConfirmation.h"
#import "BillingLine.h"
#import "SPGooglePlacesAutocomplete.h"



NSString * kGigyaKey ;//@"3_FwZaI36GQCWRSLjee42SNTr8v74DZdGknGPDkk5HQbn7rCXYO65jONGrUYHO2Fab";

static NSString * const kDebugModeKey = @"kDebugModeKey";
static NSString * kBaseURL;
static NSString * kBaseURLAPI;
static SPGooglePlacesAutocompleteQuery *searchQuery;


static NSString * const kHomePrefix = @"home/WebsiteGateway?EncodedUrl=~";

//static NSString * const kBaseURL = @"http://10.10.200.177/tenbiswebapplication";
//static NSString * const kBaseURLAPI = @"http://10.10.200.177/tenbiswebapplication/api/";


static NSInteger const kNumberOfTries =2;
static NSInteger const kTimeout = 30;

#define DEFAULTS [NSUserDefaults standardUserDefaults]
NSString * const kUserAuthorizationCompleteNotificationKey = @"kUserAuthorizationCompleteNotificationKey";
NSString * const kLogOutKey = @"kUserLogout";

static BOOL isAuthorizationComplete;

static NSString * const kLoginedNetworksKey = @"kLoginedNetworksKey";

static NSString * const kUserEncryptedKey = @"kUserEncryptedKey";
static NSString * const kShoppingCartKey = @"kShoppingCartKey";


static User *_currentUser;
static NSString * _shoppingCart;
static DeliveryMethod _deliveryMethod;

static BOOL _quickOrder;
static BOOL _loginForCheckout;

static NSNumber *_searchCityID;
static NSNumber *_searchStreetID;

@implementation DataManager
{
    CLLocation *_currentLocation;
}
static DataManager *dataManager = nil;

+ (id) sharedManager
{
    @synchronized (self)
    {
        if (dataManager == nil)
        {
            dataManager = [[self alloc] init];
        }
    }
    return dataManager;
}


- (CLLocationManager *)locationManager
{
    if(!_locationManager)
    {
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
    }
    return _locationManager;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = [locations lastObject];
    [manager stopUpdatingLocation];
    _currentLocation = loc;
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [manager stopUpdatingLocation];
}

+ (void)setDebugMode:(BOOL)isDebug
{
    [DEFAULTS setBool: isDebug forKey: kDebugModeKey];
    [DEFAULTS synchronize];
    if (isDebug)
    {
        kBaseURL = @"http://10.10.200.177/tenbiswebapplication";
        kBaseURLAPI = @"http://10.10.200.177/tenbiswebapplication/api";
    }
    else
    {
        kBaseURL = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BaseUrl"];
        kBaseURLAPI = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BaseAPIUrl"];    }
}
+ (BOOL)debugMode
{
    return [DEFAULTS boolForKey: kDebugModeKey];
}
+ (void)initialize
{
    // base urls
    [self setDebugMode: [self debugMode]];
    
    
    kGigyaKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GigyaAPIKey"];

    [Gigya initWithAPIKey: kGigyaKey];
    
    _shoppingCart = [DEFAULTS objectForKey: kShoppingCartKey];
    [DataManager loginOnInit];
}
+ (void)loginOnInit {
    // user login
    NSString *encryptedID = [DEFAULTS objectForKey: kUserEncryptedKey];
    if (encryptedID)
    {
        // try to login
        [self loginUser: nil withPassword: nil socialUID: nil encryptedId: encryptedID completion:^(id result, NSString *error) {
            if (error && [Gigya session].isValid)
            {
                // if fails try to refresh session via Gigya
                GSRequest *request = [GSRequest requestForMethod:@"socialize.getUserInfo"];
                [request sendWithResponseHandler:^(GSResponse *response, NSError *error) {
                    if (!error) {
                        [self socialLoginWithUser: (GSUser*)response completion:^(id result, NSString *error) {
                            [self authorizationComplete];
                        }];
                    }
                    else
                        [self authorizationComplete];
                }];
            }
            else
                [self authorizationComplete];
        }];
    }
    else
        [self authorizationComplete];
}
+ (void)authorizationComplete
{
    isAuthorizationComplete = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName: kUserAuthorizationCompleteNotificationKey object: nil];
}
+ (BOOL)isAuthorizationComplete
{
    return isAuthorizationComplete;
}
+ (void)setDeliveryMethod:(DeliveryMethod)method
{
    _deliveryMethod = method;
}
+ (DeliveryMethod)deliveryMethod
{
    return _deliveryMethod;
}
+ (void)setQuickOrder:(BOOL)quickOrder;
{
    _quickOrder = quickOrder;
}
+ (BOOL)quickOrder
{
    return _quickOrder;
}
+ (void)setLoginForCheckout:(BOOL)loginForCheckout
{
    _loginForCheckout = loginForCheckout;
}
+ (BOOL)isLoginForCheckout
{
    return _loginForCheckout;
}

+ (void)setSearchCityID:(NSNumber*)cityId
{
    _searchCityID = cityId;
}
+ (void)setSearchStreetID:(NSNumber*)streetId
{
    _searchStreetID = streetId;
}

+ (void)userLoginedWithData:(NSDictionary*)serverData
{
    NSDictionary *userData = serverData[@"UserData"];
    
    User *user = [User new];
    user.userId = ValueOrNil(userData, @"UserId");
    user.encryptedId = ValueOrNil(userData, @"EncryptedUserId");
    user.firstName = ValueOrNil(userData, @"UserFirstName");
    user.lastName = ValueOrNil(userData, @"UserLastName");
    user.email = ValueOrNil(userData, @"UserEmail");
    user.companyId = ValueOrNil(userData, @"CompanyID");
    user.photoURL = ValueOrNil(userData, @"UserThumbnail");
    
    [self saveShoppingCart: ValueOrNil(serverData, @"ShoppingCartGuid")];
    
    
    [DEFAULTS setObject: user.encryptedId forKey: kUserEncryptedKey];
    [DEFAULTS synchronize];
    _currentUser = user;

}
+ (void)saveShoppingCart:(NSString*)cart
{
    _shoppingCart = cart;
    [DEFAULTS setObject: _shoppingCart forKey: kShoppingCartKey];
    [DEFAULTS synchronize];
}
- (void)updateCurrentLocation
{
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [[self locationManager] startUpdatingLocation];
}


- (CLLocation*)currentLocation
{
    return _currentLocation;
}


+ (NSString *)filterSettingKey
{
    User *user = [DataManager currentUser];
    return [NSString stringWithFormat:@"%@_%@",kFilterSettingsKey, user.encryptedId];
}

#pragma mark - Gigya
+ (User*)currentUser
{
    return _currentUser;
}
+ (void)userLoginedToNetwork:(NSString*)network
{
    NSMutableArray *networks = [NSMutableArray arrayWithArray: [DEFAULTS objectForKey: kLoginedNetworksKey]];
    [networks addObject: network];
    [DEFAULTS setObject: networks forKey: kLoginedNetworksKey];
}
+ (NSArray*)loginedSocialNetworks
{
    return [DEFAULTS objectForKey: kLoginedNetworksKey];
}
+ (void)userLogout
{
    if ([self isLoginForCheckout]) // don't clear internal data
        return;
    
    _currentUser = nil;
    _shoppingCart = nil;
    
    [DEFAULTS removeObjectForKey: kLoginedNetworksKey];
    [DEFAULTS removeObjectForKey: kUserEncryptedKey];
    [DEFAULTS removeObjectForKey: kShoppingCartKey];
    [DEFAULTS synchronize];
}
+ (NSString*)deliveryFromType:(DeliveryMethod)method
{
    NSString *deliveryString = nil;
    switch (method)
    {
        case DeliveryMethodNone:
            break;
        case DeliveryMethodDelivery:
            deliveryString = @"delivery";
            break;
        case DeliveryMethodPickup:
            deliveryString = @"pickup";
            break;
        case DeliveryMethodSitting:
            deliveryString = @"sitting";
            break;
    }
    return deliveryString;
}

+ (NSString *)appDomain
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"domainId"];
}

#pragma mark - 10Bis Server
+ (URLDictionary*)baseParams
{
    URLDictionary *params = [URLDictionary dictionaryWithCapacity: 10];
    if (_currentUser)
        params[@"encryptedUserId"] = _currentUser.encryptedId;
    
    params[@"websiteid"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"websiteId"];
    params[@"domainID"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"domainId"];
    return params;
}
+ (NSString*)errorFromResponse:(NSDictionary*)response
{
    if(response && [response isKindOfClass:[NSDictionary class]])
    {
        NSString *error = response[@"ErrorText"];
        if ([error isKindOfClass: [NSString class]] == NO || error.length == 0)
        {
            error = @"An error occurred while connecting to the server\nPlease try again.";
            if ([response isKindOfClass: [NSDictionary class]])
            {
                if ([response[@"Error"] isKindOfClass: [NSDictionary class]])
                {
                    NSString *errDesc = response[@"Error"][@"ErrorDesc"];
                    if ([errDesc isKindOfClass: [NSString class]] && errDesc.length)
                        error = errDesc;
                }
            }
        }
        return error;
    }
    return nil;
}

+ (void)loginUser:(NSString*)email withPassword:(NSString*)password socialUID:(NSString*)uid encryptedId:(NSString*)encryptedId completion:(CompletionBlock)handler
{
    
    //#ifdef DEBUG
    //    email = @"fefer@10bis.co.il";
    //    password = @"11235813";
    //#endif
    URLDictionary *params = [self baseParams];
    params[@"encryptedUserId"] = encryptedId;
    params[@"SocialLoginUID"] = uid;
    
    if (email && email.length && password && password.length) {
        params[@"UserName"] = email;
        params[@"Password"] = password;
        params[@"shoppingCartGuid"] = _shoppingCart;
    }
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"Login",  [params getParametrs]];
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(NSDictionary* response) {
        BOOL responseSuccess = [response[@"Success"] boolValue];
        if (responseSuccess)
        {
            [self userLoginedWithData: response];
            handler(_currentUser, nil);
        }
        else
        {
            handler(nil, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (void)socialLoginWithUser:(GSUser*)user completion:(CompletionBlock)handler
{
    NSString *encodedUID = [URLDictionary encodeToPercentEscapeString: user[@"UID"]];
    URLDictionary *params = [self baseParams];
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"SocialLogin", [params getParametrs]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary: @{@"encodedUID": encodedUID, @"signatureTimestamp" : user[@"signatureTimestamp"], @"UIDSignature" : user[@"UIDSignature"], @"isSiteUID" : user[@"isSiteUID"]}];
    if (_shoppingCart)
    {
        postParams[@"ShoppingCartGuid"] = _shoppingCart;
    }
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: postParams finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: NO requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail:
     kNumberOfTries completionBlock:^(id response) {
         NSString *responseString = response;
         NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding: NSUTF8StringEncoding] options: kNilOptions error: nil];
         NSString *loginResponse = jsonResponse[@"LoginResponse"];
         if ([loginResponse isEqualToString: @"LoginSuccess"] || [loginResponse isEqualToString: @"ConnectionSuccess"])
         {
             [self userLoginedWithData: jsonResponse];
         }
         handler(loginResponse, nil);
     } failBlock:^(ASIHTTPRequest *result) {
         handler(nil, result.error.localizedDescription);
     } timeoutInSec: kTimeout];
}
+ (void)registerUser:(User*)user socialUID:(NSString*)uid promotion:(BOOL)promotion password:(NSString*)password completion:(CompletionBlock)handler
{
    NSString *URL = [NSString stringWithFormat: @"%@/%@", kBaseURLAPI, @"Register"];
    URLDictionary *params = [self baseParams];
    params[@"FirstName"] = user.firstName;
    params[@"LastName"] = user.lastName;
    params[@"CellPhone"] = user.phone;
    params[@"email"] = user.email;
    params[@"WantPromotion"] = @(promotion);
    params[@"shoppingCartGuid"] = _shoppingCart;
    if (uid)
        params[@"SocialLoginUID"] = uid;
    else if (password)
        params[@"Password"] = password;
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: params.dictionary finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail:
     kNumberOfTries completionBlock:^(id response) {
         BOOL responseSuccess = [response[@"Success"] boolValue];
         if (responseSuccess)
         {
             [self userLoginedWithData: response];
             handler(_currentUser, nil);
         }
         else
         {
             handler(nil, [self errorFromResponse: response]);
         }
     } failBlock:^(ASIHTTPRequest *result) {
         handler(nil, result.error.localizedDescription);
     } timeoutInSec: kTimeout];
}

+(void)loadCuisinesFilterList:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"cuisines", [params getParametrs]];
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        if ([response isKindOfClass: [NSArray class]])
        {
            NSArray *cuisines = response;
            handler(cuisines, nil);
        }
        else
            handler (nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}


+ (void)resetPassword:(NSString*)email completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?emailaddress=%@&%@", kBaseURLAPI, @"ResetPassword",  email, [params getParametrs]];

    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        handler(response, nil);
        
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
    
}

+ (void)getAddressesByUser:(User*)user completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"GetAddressList", [params getParametrs]];
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        if ([response isKindOfClass: [NSArray class]])
        {
            NSArray *addressesData = response;
            NSMutableArray *addresses = [NSMutableArray arrayWithCapacity: addressesData.count];
            for (NSDictionary *addressData in addressesData)
            {
                Address *address = [Address addressWithData: addressData];
                [addresses addObject: address];
            }
            handler(addresses, nil);
        }
        else
        {
            handler(nil, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}

+ (void)googleAutocompleteWithText:(NSString *)text completion:(CompletionBlock)handler
{
    if(!searchQuery)
    {
        searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] initWithApiKey:@"AIzaSyCFg6OZoYLmOEJl6ayavZc3PhFu88XndKc"];
        searchQuery.language = @"en";
    }
    
   // searchQuery.location = self.mapView.userLocation.coordinate;
    searchQuery.input = text;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            handler(nil, error.localizedDescription);
        } else {
            handler(places,nil);
        }
    }];

}

+ (void)autocompleteCityName:(NSString*)cityName completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"q"] = cityName;
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"CityNameAutoComplete", [params getParametrs]];
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        if ([response isKindOfClass: [NSArray class]])
        {
            NSArray *citiesData = response;
            NSMutableArray *cities = [NSMutableArray arrayWithCapacity: citiesData.count];
            for (NSDictionary *cityData in citiesData)
            {
                City *city = [City cityWithData: cityData];
                [cities addObject: city];
            }
            handler(cities, nil);
        }
        else
            handler (nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (void)autocompleteStreetName:(NSString*)streetName parent:(NSNumber*)parentId completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"q"] = streetName;
    params[@"parentId"] = parentId;
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"StreetNameAutoComplete", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        if ([response isKindOfClass: [NSArray class]])
        {
            NSArray *streetsData = response;
            NSMutableArray *streets = [NSMutableArray arrayWithCapacity: streetsData.count];
            for (NSDictionary *streetData in streetsData)
            {
                Street *street = [Street streetWithData: streetData];
                [streets addObject: street];
            }
            handler(streets, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}

+ (void)searchRestaurantsWithAddress:(Address*)address completion:(CompletionBlock)handler
{
    [self searchRestaurantsWithAddress:address.addressId city:address.cityId andStreet:address.streetId filter:0 completion:handler];
    
}


+ (void)searchRestaurantsWithAddress:(NSNumber*)addressId city:(NSNumber *)cityId andStreet:(NSNumber *)streetId filter:(SearchFilter)filter completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"id"] = addressId;
    params[@"cityId"] = cityId;
    params[@"streetId"] = streetId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    
    if (_deliveryMethod != DeliveryMethodNone)
        params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    
    //    params[@"FilterByKosher"] = @((filter & SearchFilterByKosher) == SearchFilterByKosher);
    //    params[@"FilterByBookmark"] = @((filter & SearchFilterByBookmark) == SearchFilterByBookmark);
    //    params[@"FilterByCoupon"] = @((filter & SearchFilterByCoupon) == SearchFilterByCoupon);
    
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"SearchRestaurants", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL success = [ValueOrNil(response, @"Success") boolValue];
        if (success)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            
            NSArray *restaurantsData = ValueOrNil(response, @"Data");
            NSMutableArray *restaurants = [NSMutableArray arrayWithCapacity: restaurantsData.count];
            for (NSDictionary *restaurantData in restaurantsData)
            {
                Restaurant *restaurant = [Restaurant restaurantWithData: restaurantData];
                [restaurants addObject: restaurant];
            }
            handler(restaurants, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}

+ (void)searchRestaurantsByLocation:(CLLocationCoordinate2D)coordinates completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"Latitude"] = @(coordinates.latitude);
    params[@"Longitude"] = @(coordinates.longitude);

    params[@"shoppingCartGuid"] = _shoppingCart;


    if (_deliveryMethod != DeliveryMethodNone)
        params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    
    //    params[@"FilterByKosher"] = @((filter & SearchFilterByKosher) == SearchFilterByKosher);
    //    params[@"FilterByBookmark"] = @((filter & SearchFilterByBookmark) == SearchFilterByBookmark);
    //    params[@"FilterByCoupon"] = @((filter & SearchFilterByCoupon) == SearchFilterByCoupon);
    
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@/?%@", kBaseURLAPI, @"SearchRestaurants", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL success = [ValueOrNil(response, @"Success") boolValue];
        if (success)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            
            NSArray *restaurantsData = ValueOrNil(response, @"Data");
            NSMutableArray *restaurants = [NSMutableArray arrayWithCapacity: restaurantsData.count];
            for (NSDictionary *restaurantData in restaurantsData)
            {
                Restaurant *restaurant = [Restaurant restaurantWithData: restaurantData];
                [restaurants addObject: restaurant];
            }
            handler(restaurants, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];

}

+ (void)searchRestaurantsByMap:(CLLocationCoordinate2D)coordinates completion:(CompletionBlock)handler
{
    NSArray *requestBoundary = [Utils requestBoundariesForLocation: coordinates];
    CLLocationDegrees northPointLat = [requestBoundary[0] doubleValue];
    CLLocationDegrees southPointLat = [requestBoundary[1] doubleValue];
    CLLocationDegrees eastPointLng = [requestBoundary[2] doubleValue];
    CLLocationDegrees westPointLng = [requestBoundary[3] doubleValue];
    
    //    CLLocationDegrees northPointLat = 35.570297;
    //    CLLocationDegrees southPointLat = 29.470688;
    //    CLLocationDegrees westPointLng = 34.167480;
    //    CLLocationDegrees eastPointLng = 35.936279;
    
    URLDictionary *params = [self baseParams];
    params[@"destinationLng"] = @(coordinates.longitude);
    params[@"destinationLat"] = @(coordinates.latitude);
    params[@"notrhBoundary"] = @(northPointLat);
    params[@"southBoundary"] = @(southPointLat);
    params[@"westBoundary"] = @(westPointLng);
    params[@"eastBoundary"] = @(eastPointLng);
    
    params[@"isKosher"] = @"false";
    params[@"FilterByCoupon"] = @"false";
    params[@"shoppingCartGuid"] = _shoppingCart;
    
    if (_deliveryMethod != DeliveryMethodNone)
        params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"SearchRestaurantsListByMapBoundaries", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL success = [ValueOrNil(response, @"Success") boolValue];
        if (success)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            
            NSArray *restaurantsData = ValueOrNil(response, @"Data");
            NSMutableArray *restaurants = [NSMutableArray arrayWithCapacity: restaurantsData.count];
            for (NSDictionary *restaurantData in restaurantsData)
            {
                Restaurant *restaurant = [Restaurant restaurantWithData: restaurantData];
                [restaurants addObject: restaurant];
            }
            handler(restaurants, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
        
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
    
    
}
+ (void)getRestaurantMenu:(NSNumber*)restaurantId userId:(NSString*)userId completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"ResId"] = restaurantId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    if (_deliveryMethod != DeliveryMethodNone)
        params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"GetMenu", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL success = [ValueOrNil(response, @"Success") boolValue];
        if (success)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            
            NSArray *menusData = ValueOrNil(response, @"Data");
            NSMutableArray *menus = [NSMutableArray arrayWithCapacity: menusData.count];
            for (NSDictionary *menuData in menusData)
            {
                Menu *menu = [Menu menuWithData:menuData];
                [menus addObject: menu];
            }
            handler(menus, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (NSString*)discountURLForRestaurant:(NSNumber*)restaurantId
{
    URLDictionary *params = [self baseParams];
    params[@"ResId"] = restaurantId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    
    if (_deliveryMethod != DeliveryMethodNone)
        params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURL, @"Restaurants/Discounts", [params getParametrs]];
    return URL;
}

+ (void)getDetailedDish:(NSNumber*)dishId categoryId:(NSNumber*)categoryId completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"dishId"] = dishId;
    params[@"categoryID"] = categoryId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"GetDish", [params getParametrs]];
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL success = [ValueOrNil(response, @"Success") boolValue];
        if (success)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            
            NSDictionary *dishData = ValueOrNil(response, @"Data");
            Dish *dish = [Dish dishWithData: dishData];
            handler(dish, nil);
        }
        else
            handler(nil, [self errorFromResponse: response]);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}

+ (void)shoppingCartTotalForRestaurant:(NSNumber*)restaurantId completion:(CompletionBlock)handler
{
    if (_shoppingCart == nil)
    {
        handler(@"0", nil);
        return;
    }
    
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"RestId"] = restaurantId;
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"ShoppingCartTotal", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: NO requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        handler(response, nil);
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (void)addDishToCart:(Order*)order restaurant:(NSNumber*)restaurantId completion:(BoolCompletionBlock)handler;
{
    NSString *URL = [NSString stringWithFormat: @"%@/%@", kBaseURLAPI, @"AddDish"];
    NSMutableDictionary *post = [NSMutableDictionary dictionaryWithDictionary: @{@"ResId": restaurantId, @"Domainid" : @"10bis", @"Websiteid" : @"10bis",  @"dishToSubmit": [order jsonDictionary]}];
    if (_currentUser && _currentUser.encryptedId)
        post[@"encryptedUserId"] = _currentUser.encryptedId;
    if (_shoppingCart)
        post[@"shoppingCartGuid"] = _shoppingCart;
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: post finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        
        BOOL responseSuccess = [response[@"Success"] boolValue];
        if (responseSuccess)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            handler(YES, nil);
        }
        else
        {
            handler(NO, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(NO, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}

+ (void)orderConfirmation:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"OrderConfirmation", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        
        if ([response isKindOfClass: [NSDictionary class]] && [response count])
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            OrderConfirmation *order = [OrderConfirmation orderConfirmationWithData: response];
            handler(order, nil);
        }
        else
        {
            handler(nil, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
    
}
+ (void)setTipToCart:(NSNumber*)restaurantId tipAmount:(NSNumber *)amount completion:(CompletionBlock)handler
{
    //    URLDictionary *params = [self baseParams];
    //    params[@"ResId"] = restaurantId;
    //    params[@"shoppingCartGuid"] = _shoppingCart;
    //    params[@"TipAmount"] = @(amount);
    //    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"SetTip", [params getParametrs]];
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@", kBaseURLAPI, @"SetTip"];
    NSMutableDictionary *post = [NSMutableDictionary dictionaryWithDictionary: @{@"ResId": restaurantId,  @"TipAmount": amount}];
    post[@"websiteid"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"websiteId"];
    post[@"domainID"] = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"domainId"];

    if (_currentUser)
        post[@"encryptedUserId"] = _currentUser.encryptedId;
    if (_shoppingCart)
        post[@"shoppingCartGuid"] = _shoppingCart;
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: post finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL responseSuccess = [response[@"Success"] boolValue];
        if (responseSuccess)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            if (cart)
                [self saveShoppingCart: cart];
            
            NSDictionary *data = ValueOrNil(response, @"Data");
            NSArray *billingLinesData = ValueOrNil(data, @"BillingLines");
            NSMutableArray *billingLines = [NSMutableArray arrayWithCapacity: billingLinesData.count];
            for (NSDictionary *billingLineData in billingLinesData)
            {
                BillingLine *line = [BillingLine billingTypeFromData: billingLineData];
                [billingLines addObject: line];
            }
            
            handler(billingLines, nil);
        }
        else
        {
            handler(nil, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(NO, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
    
}
+ (void)getDishToEdit:(NSNumber*)restaurantId index:(NSInteger)index completion:(CompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"ResId"] = restaurantId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"dishIndex"] = @(index);
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"GetDishToEdit", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kGET useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        if ([response isKindOfClass:[NSDictionary class]] && [response[@"Success"] boolValue])
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            NSDictionary *dishData = ValueOrNil(response, @"Data");
            Dish *dish = [Dish dishWithData: dishData];
            handler(dish, nil);
        }
        else
        {
            handler(nil, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(nil, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (void)removeDishFromCart:(NSNumber*)restaurantId index:(NSInteger)index completion:(BoolCompletionBlock)handler
{
    URLDictionary *params = [self baseParams];
    params[@"ResId"] = restaurantId;
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"dishIndex"] = @(index);
    
    NSString *URL = [NSString stringWithFormat: @"%@/%@?%@", kBaseURLAPI, @"removedish", [params getParametrs]];
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: nil finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL responseSuccess = [response[@"Success"] boolValue];
        if (responseSuccess)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            handler(YES, nil);
        }
        else
        {
            handler(NO, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(NO, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
}
+ (void)updateDish:(Order*)order restaurant:(NSNumber*)restaurantId completion:(BoolCompletionBlock)handler;
{
    NSString *URL = [NSString stringWithFormat: @"%@/%@", kBaseURLAPI, @"updatedish"];
    NSMutableDictionary *post = [NSMutableDictionary dictionaryWithDictionary: @{@"ResId": restaurantId, @"Domainid" : @"10bis", @"Websiteid" : @"10bis", @"dishToSubmit": [order jsonDictionary]}];
    if (_currentUser)
        post[@"encryptedUserId"] = _currentUser.encryptedId;
    if (_shoppingCart)
        post[@"shoppingCartGuid"] = _shoppingCart;
    
    [[CommManager sharedManager] grabURLInBackground: URL withDelegate: nil useQueue: NO withData: post finishSelector: nil failSelector: nil queueFinishSel: nil userInfo: nil useJSON: YES isResponseIsInJSON: YES requestMethod: kPOST useHTTPS: YES continueInBackground: YES headers: nil numberOfRetriesOnFail: kNumberOfTries completionBlock:^(id response) {
        BOOL responseSuccess = [response[@"Success"] boolValue];
        if (responseSuccess)
        {
            NSString *cart = ValueOrNil(response, @"ShoppingCartGuid");
            [self saveShoppingCart: cart];
            handler(YES, nil);
        }
        else
        {
            handler(NO, [self errorFromResponse: response]);
        }
    } failBlock:^(ASIHTTPRequest *result) {
        handler(NO, result.error.localizedDescription);
    } timeoutInSec: kTimeout];
    
}

+ (NSString*)checkoutURLForRestaurant:(NSNumber*)restaurantId
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    params[@"SearchCityId"] = _searchCityID;
    params[@"SearchStreetId"] = _searchStreetID;
    params[@"ResId"] = restaurantId;
    params[@"deliveryMethod"] = [self deliveryFromType: _deliveryMethod];
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL, kHomePrefix, @"checkout", [params getParametrs]];
    return URL;
}
+ (NSString*)creditCardsURL
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL,kHomePrefix, @"account/usercclist", [params getParametrs]];
    return URL;
}
+ (NSString*)tenbisCardsURL
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL, kHomePrefix, @"account/UserAccountManagement", [params getParametrs]];
    return URL;
}

+ (NSString*)profileURL
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL, kHomePrefix, @"account/UserProfileDetails", [params getParametrs]];
    return URL;
}
+ (NSString*)tableForTwoURL
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL, kHomePrefix, @"TableForTwo", [params getParametrs]];
    return URL;
}

+ (NSString *)ownRestaurantUrl
{
    NSString *result = @"https://onlineordering.wufoo.com/forms/w12u27m01mnoye6/";
    return result;
}

+ (NSString*)reportURL
{
    URLDictionary *params = [self baseParams];
    params[@"shoppingCartGuid"] = _shoppingCart;
    params[@"ismobile"] = @"true";
    params[@"currentContext"] = @"MobileApp";
    NSString *URL = [NSString stringWithFormat: @"%@/%@/%@&%@", kBaseURL, kHomePrefix, @"account/userreport", [params getParametrs]];
    return URL;
}
+ (NSString*)contactURL
{
    return @"https://onlineordering.wufoo.com/forms/p10j0koq1lwf4we";
}

+ (NSString *)policyURL
{
    NSString *url = [NSString stringWithFormat:@"%@/Info/PrivacyPolicies?isMobileUser=true",kBaseURL];
    return url;
}

+ (NSString *)touURL
{
    NSString *url = [NSString stringWithFormat:@"%@/Info/TermOfUse?isMobileUser=true",kBaseURL];
    return url;
}

+ (NSString *)kosherDetailsUrl
{
    NSString *url = [NSString stringWithFormat:@"%@/Restaurants/kosherdetails",kBaseURL];
    return url;
}

+ (NSString *)baseUrl
{
    return kBaseURL;
}

+ (NSString *)baseAPIUrl
{
    return kBaseURLAPI;
}

@end
