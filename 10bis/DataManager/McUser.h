//
//  McUser.h
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface McUser : NSObject
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) BOOL selected;

+ (instancetype)userFromData:(NSDictionary*)userData;
@end
