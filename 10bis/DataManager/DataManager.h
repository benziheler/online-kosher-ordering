//
//  DataManager.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Address.h"

#define kCuisinesFilterList @"cuisinesFilterList"
#define kFilterSettingsKey @"filterSettingsKey"


extern NSString * const kUserAuthorizationCompleteNotificationKey;
extern NSString * const kLogOutKey;

typedef void (^BoolCompletionBlock) (BOOL result, NSString *error);
typedef void (^CompletionBlock)(id result, NSString *error);

typedef NS_ENUM(NSUInteger, DeliveryMethod) {
    DeliveryMethodNone,
    DeliveryMethodPickup,
    DeliveryMethodDelivery,
    DeliveryMethodSitting,
};

typedef NS_OPTIONS(NSUInteger, SearchFilter)
{
    SearchFilterNone = 0,
    SearchFilterByKosher = 1 << 0,
    SearchFilterByBookmark = 1 << 1,
    SearchFilterByCoupon = 2 << 1,
};

@class User, GSUser, Dish, Order;
@interface DataManager : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

+ (id) sharedManager;
+ (void)setDebugMode:(BOOL)isDebug;
+ (BOOL)debugMode;

// user
+ (BOOL)isAuthorizationComplete;
+ (void)userLoginedToNetwork:(NSString*)network;
+ (NSArray*)loginedSocialNetworks;

- (void)updateCurrentLocation;
- (CLLocation*)currentLocation;

+ (NSString *)baseUrl;
+ (NSString *)baseAPIUrl;
+ (NSString *)appDomain;


// login
+ (User*)currentUser;
+ (void)loginOnInit;
+ (void)loginUser:(NSString*)email withPassword:(NSString*)password socialUID:(NSString*)uid encryptedId:(NSString*)encryptedId completion:(CompletionBlock)handler;
+ (void)socialLoginWithUser:(GSUser*)user completion:(CompletionBlock)handler;
+ (void)registerUser:(User*)user socialUID:(NSString*)uid promotion:(BOOL)promotion password:(NSString*)password completion:(CompletionBlock)handler;

+ (void)resetPassword:(NSString*)email completion:(CompletionBlock)handler;
+ (void)userLogout;

// addresses
+ (void)getAddressesByUser:(User*)user completion:(CompletionBlock)handler;

// autocomplete
+ (void)googleAutocompleteWithText:(NSString *)text completion:(CompletionBlock)handler;
+ (void)autocompleteCityName:(NSString*)cityName completion:(CompletionBlock)handler;
+ (void)autocompleteStreetName:(NSString*)streetName parent:(NSNumber*)parentId completion:(CompletionBlock)handler;

// restaurants
+ (void)searchRestaurantsWithAddress:(Address*)address completion:(CompletionBlock)handler;
+ (void)searchRestaurantsByMap:(CLLocationCoordinate2D)coordinates completion:(CompletionBlock)handler;

+ (void)getRestaurantMenu:(NSNumber*)restaurantId userId:(NSString*)userId completion:(CompletionBlock)handler;
+(void)loadCuisinesFilterList:(CompletionBlock)handler;

// dish
+ (void)getDetailedDish:(NSNumber*)dishId categoryId:(NSNumber*)categoryId completion:(CompletionBlock)handler;
+ (void)shoppingCartTotalForRestaurant:(NSNumber*)restaurantId completion:(CompletionBlock)handler;
+ (void)addDishToCart:(Order*)order restaurant:(NSNumber*)restaurantId completion:(BoolCompletionBlock)handler;

+ (void)orderConfirmation:(CompletionBlock)handler;
+ (void)setTipToCart:(NSNumber*)restaurantId tipAmount:(NSNumber *)amount completion:(CompletionBlock)handler;
+ (void)getDishToEdit:(NSNumber*)restaurantId index:(NSInteger)index completion:(CompletionBlock)handler;
+ (void)removeDishFromCart:(NSNumber*)restaurantId index:(NSInteger)index completion:(BoolCompletionBlock)handler;
+ (void)updateDish:(Order*)order restaurant:(NSNumber*)restaurantId completion:(BoolCompletionBlock)handler;
+ (void)searchRestaurantsByLocation:(CLLocationCoordinate2D)coordinates completion:(CompletionBlock)handler;
+ (void)setSearchCityID:(NSNumber*)cityId;
+ (void)setSearchStreetID:(NSNumber*)streetId;

// URLs
+ (NSString*)checkoutURLForRestaurant:(NSNumber*)restaurantId;
+ (NSString*)discountURLForRestaurant:(NSNumber*)restaurantId;

+ (NSString*)creditCardsURL;
+ (NSString*)tenbisCardsURL;
+ (NSString*)profileURL;
+ (NSString*)tableForTwoURL;
+ (NSString*)reportURL;
+ (NSString*)contactURL;
+ (NSString *)touURL;
+ (NSString *)policyURL;
+ (NSString *)ownRestaurantUrl;
+ (NSString *)kosherDetailsUrl;

// Flags
+ (void)setDeliveryMethod:(DeliveryMethod)method;
+ (DeliveryMethod)deliveryMethod;

+ (void)setQuickOrder:(BOOL)quickOrder;
+ (BOOL)quickOrder;

+ (void)setLoginForCheckout:(BOOL)loginForCheckout;
+ (BOOL)isLoginForCheckout;
+ (NSString *)filterSettingKey;
@end
