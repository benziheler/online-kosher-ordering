//
//  City.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "City.h"

@implementation City
+ (id)cityWithData:(NSDictionary*)cityData
{
    City *city = [City new];
    city.cityId = ValueOrNil(cityData, @"Id");
    city.isBigCity = [ValueOrNil(cityData, @"IsBigCity") boolValue];
    city.cityName = ValueOrNil(cityData, @"Name");
    return city;
}
@end
