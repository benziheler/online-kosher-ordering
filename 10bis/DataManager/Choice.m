//
//  Choice.m
//  10bis
//
//  Created by Vadim Pavlov on 12.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Choice.h"
#import "SubChoice.h"
@implementation Choice
+ (instancetype)choiceWithData:(NSDictionary*)choiceData
{
    Choice *choice = [Choice new];
    choice.ID = ValueOrNil(choiceData, @"ID");
    choice.choicePosition = [ValueOrNil(choiceData, @"ChoicePosition") integerValue];
    choice.maxPicksAllowed = [ValueOrNil(choiceData, @"MaxPicksAllowed") integerValue];
    choice.textBeforeChoosing = ValueOrNil(choiceData, @"TextBeforeChoosing");
    choice.type = [ValueOrNil(choiceData, @"UIType") integerValue];
    NSArray *subsData = ValueOrNil(choiceData, @"Subs");
    if (subsData == nil)
    {
        subsData = ValueOrNil(choiceData, @"SubsChosen");
    }
    if ([subsData isKindOfClass: [NSArray class]])
    {
        NSMutableArray *subs = [NSMutableArray arrayWithCapacity: subsData.count];
        for (NSDictionary *subData in subsData)
        {
            SubChoice *sub = [SubChoice subChoiceWithData: subData];
            [subs addObject: sub];
        }
        choice.subsChosen = subs;
    }
    return choice;
}
+ (BOOL)isSingleValueForType:(ChoiceType)type
{
    switch (type) {
        case ChoiceTypeRadioButtons:
        case ChoiceTypeBeverages:
        case ChoiceTypeComboBox:
            return YES;
            break;
        default:
            break;
    }
    return NO;
}
- (id)copyWithZone:(NSZone *)zone
{
    Choice *choice = [Choice new];
    choice.ID = self.ID;
    choice.choicePosition = self.choicePosition;
    choice.maxPicksAllowed = self.maxPicksAllowed;
    choice.textBeforeChoosing = self.textBeforeChoosing;
    choice.type = self.type;
    choice.subsChosen = [[NSArray alloc] initWithArray: self.subsChosen copyItems: YES];
    return choice;
}
@end
