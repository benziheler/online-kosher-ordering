//
//  Restaurant.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Restaurant : NSObject
@property (nonatomic, strong) NSNumber *restaurantId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *logoURL;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *cuisineList;
@property (nonatomic, assign) NSInteger numberOfReviews;
@property (nonatomic, assign) NSInteger reviewsRank;
@property (nonatomic, strong) NSString *distanceFromUser;
@property (nonatomic, strong) NSNumber *distanceFromUserMeters;
@property (nonatomic, assign) BOOL isOpenForDelivery;
@property (nonatomic, assign) BOOL isOpenForPickup;
@property (nonatomic, strong) NSString *minimumOrder;
@property (nonatomic, assign) NSInteger minimumPriceForOrder;
@property (nonatomic, strong) NSString *deliveryPrice;
@property (nonatomic, assign) NSInteger deliveryPriceForOrder;
@property (nonatomic, strong) NSString *isKosher;
@property (nonatomic, strong) NSString *kosher;
@property (nonatomic, strong) NSString *deliveryRemarks;
@property (nonatomic, assign) CLLocationDegrees latitude;
@property (nonatomic, assign) CLLocationDegrees longitude;

@property (nonatomic, assign) BOOL isHappyHourActive;

@property (nonatomic, assign) BOOL phoneOrdersOnlyOnPortals;
//@property (nonatomic, assign) BOOL сompanyFlag;

@property (nonatomic, strong) NSString *happyHourDiscount;
@property (nonatomic, strong) NSString *happyHourDiscountValidity;

@property (nonatomic, strong) NSString *startOrderURL;
@property (nonatomic, strong) NSString *activityHours;
@property (nonatomic, strong) NSString *pickupActivityHours;
@property (nonatomic, strong) NSString *deliveryTime;

@property (nonatomic, assign) BOOL isPromotionActive;
@property (nonatomic, assign) BOOL companyFlag;
@property (nonatomic, assign) BOOL isOverPoolMin;
@property (nonatomic, strong) NSString *poolSum;
@property (nonatomic, strong) NSString *deliveryEndTime;

@property (nonatomic, assign) BOOL isTerminalActive;
@property (nonatomic, assign) BOOL isActiveForDelivery;
@property (nonatomic, assign) BOOL isActiveForPickup;
@property (nonatomic, assign) BOOL bookmarked;

@property (nonatomic, assign) NSInteger discountCouponPercent;
@property (nonatomic, assign) BOOL couponHasRestrictions;
@property (nonatomic, strong) NSString *happyHourResRulesDescription;

+ (id)restaurantWithData:(NSDictionary*)restaurantData;
- (NSString *)distanceString;
- (NSString *)reversedPickupHours;

@end
