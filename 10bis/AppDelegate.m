//
//  AppDelegate.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "AppDelegate.h"
#import "DataManager.h"
#import <Crashlytics/Crashlytics.h>
#import <GigyaSDK/Gigya.h>
#import "SlidingVC.h"

static NSString * googleTrackId;

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GAI sharedInstance].dispatchInterval = 20;
    [[UILabel appearanceWhenContainedIn:[UITextField class], nil] setTextColor:[Utils appPrimaryColor]];
    

    // Initialize tracker.
    googleTrackId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GoogleAnalyticsTrackId"];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:googleTrackId];

    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"app_event"     // Event category (required)
                                                          action:@"launch"        // Event action (required)
                                                           label:@"application launched"          // Event label
                                                           value:nil] build]];
    
    if ([Utils isIOS7] == NO)
    {
        [application setStatusBarHidden: NO withAnimation: UIStatusBarAnimationSlide];
    }

    [Crashlytics startWithAPIKey:@"f0068115da4c06bf11b509483e559ac09d4a5e25"];
    
    [application setStatusBarStyle: UIStatusBarStyleLightContent];
    [DataManager class];
    
    NSString *languageCode = @"en";
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* languages = [[userDefaults objectForKey:@"AppleLanguages"] mutableCopy];
    [languages removeObject: languageCode];
    [languages insertObject: languageCode atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     
    UIColor *tintColor = [Utils appPrimaryColor];
    if ([Utils isIOS7])
    {
        [[UINavigationBar appearance] setTintColor: [Utils appTextColor]];
        [[UINavigationBar appearance] setBarTintColor: tintColor];
    }
    else
    {
        [[UINavigationBar appearance] setTintColor: tintColor];
    }
    // setting empty image will broke Gigya's screens, check SlidingVC for customizing app navigation bar
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundColor: [Utils appPrimaryColor]];

  
    [DataManager loadCuisinesFilterList:^(id result, NSString *error) {
        if(result && !error)
        {
            [[NSUserDefaults standardUserDefaults] setObject:result forKey:kCuisinesFilterList];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [Utils appTextColor], NSFontAttributeName : [UIFont fontWithName: @"Arial" size: 20]}];
    
    return YES;
}

- (CustomWindow *)window
{
    static CustomWindow *customWindow = nil;
    if (!customWindow) customWindow = [[CustomWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    return customWindow;
}

- (void)applicationWillResignActive:(UIApplication *)application
{

}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"enterBgDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSDate *enterBgDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"enterBgDate"];
    if(enterBgDate)
    {
        NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:enterBgDate];
        NSInteger minutesBetweenDates = distanceBetweenDates / 60;
        if(minutesBetweenDates >= 40)
        {
            SlidingVC *rootVC = (SlidingVC *)self.window.rootViewController;
            [rootVC resetTopView];
            UINavigationController *topNavVC = (UINavigationController *)rootVC.topViewController;
            [topNavVC popToRootViewControllerAnimated:NO];
            //go to main
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[DataManager sharedManager] updateCurrentLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [Gigya handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
}

@end
